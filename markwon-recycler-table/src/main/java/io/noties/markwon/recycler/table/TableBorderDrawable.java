package io.noties.markwon.recycler.table;

import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.annotation.Px;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.media.image.common.PixelFormat;

class TableBorderDrawable extends Drawable {

    private final Paint paint = new Paint();

    TableBorderDrawable() {
        paint.setStyle(Paint.Style.STROKE_STYLE);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        if (paint.getStrokeWidth() > 0) {
            canvas.drawRect(new RectFloat(getBounds()), paint);
        }
    }

    @Override
    public void setAlpha(int alpha) {
        // no op
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        // no op
    }

    @Override
    public int getOpacity() {
        return PixelFormat.ARGB_8888.getValue();
    }

    @Override
    public void setState(int[] state) {

    }

    void update(@Px int borderWidth, @ColorInt int color) {
        paint.setStrokeWidth(borderWidth);
        paint.setColor(new Color(color));
        invalidateSelf();
    }
}
