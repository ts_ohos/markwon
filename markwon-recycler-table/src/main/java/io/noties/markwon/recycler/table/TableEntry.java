package io.noties.markwon.recycler.table;

import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.IdRes;
import com.noties.markwon.annotation.LayoutRes;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Px;
import com.noties.markwon.annotation.SuppressLint;
import com.noties.markwon.annotation.VisibleForTesting;
import com.noties.markwon.wrapper.view.TableRow;

import io.noties.markwon.Markwon;
import io.noties.markwon.ext.tables.Table;
import io.noties.markwon.recycler.MarkwonAdapter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.TableLayout;
import ohos.agp.components.Text;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.utils.PacMap;

import org.commonmark.ext.gfm.tables.TableBlock;


/**
 * @since 3.0.0
 */
public class TableEntry extends MarkwonAdapter.Entry<TableBlock, TableEntry.Holder> {

    public interface Builder {

        /**
         * @param tableLayoutResId layout with TableLayout
         * @param tableIdRes       id of the TableLayout inside specified layout
         * @see #tableLayoutIsRoot(int)
         */
        @NonNull
        Builder tableLayout(@LayoutRes int tableLayoutResId, @IdRes int tableIdRes);

        /**
         * @param tableLayoutResId layout with TableLayout as the root view
         * @see #tableLayout(int, int)
         */
        @NonNull
        Builder tableLayoutIsRoot(@LayoutRes int tableLayoutResId);

        /**
         * @param textLayoutResId layout with TextView
         * @param textIdRes       id of the TextView inside specified layout
         * @see #textLayoutIsRoot(int)
         */
        @NonNull
        Builder textLayout(@LayoutRes int textLayoutResId, @IdRes int textIdRes);

        /**
         * @param textLayoutResId layout with TextView as the root view
         * @see #textLayout(int, int)
         */
        @NonNull
        Builder textLayoutIsRoot(@LayoutRes int textLayoutResId);

        /**
         * @param cellTextCenterVertical if text inside a table cell should centered
         *                               vertically (by default `true`)
         */
        @NonNull
        Builder cellTextCenterVertical(boolean cellTextCenterVertical);

        /**
         * @param isRecyclable flag to set on RecyclerView.ViewHolder (by default `true`)
         */
        @NonNull
        Builder isRecyclable(boolean isRecyclable);

        @NonNull
        TableEntry build();
    }

    public interface BuilderConfigure {
        void configure(@NonNull Builder builder);
    }

    @NonNull
    private static Builder builder() {
        return new BuilderImpl();
    }

    @NonNull
    public static TableEntry create(@NonNull BuilderConfigure configure) {
        final Builder builder = builder();
        configure.configure(builder);
        return builder.build();
    }

    private final int tableLayoutResId;
    private final int tableIdRes;

    private final int textLayoutResId;
    private final int textIdRes;

    private final boolean isRecyclable;
    private final boolean cellTextCenterVertical;

    private LayoutScatter inflater;

    private final Map<TableBlock, Table> map = new HashMap<>(3);

    private TableEntry(
        @LayoutRes int tableLayoutResId,
        @IdRes int tableIdRes,
        @LayoutRes int textLayoutResId,
        @IdRes int textIdRes,
        boolean isRecyclable,
        boolean cellTextCenterVertical) {
        this.tableLayoutResId = tableLayoutResId;
        this.tableIdRes = tableIdRes;
        this.textLayoutResId = textLayoutResId;
        this.textIdRes = textIdRes;
        this.isRecyclable = isRecyclable;
        this.cellTextCenterVertical = cellTextCenterVertical;
    }

    @NonNull
    @Override
    public Holder createHolder(@NonNull LayoutScatter inflater, @NonNull ComponentContainer parent) {
        return new Holder(
                isRecyclable,
                tableIdRes,
                inflater.parse(tableLayoutResId, parent, false));
    }

    @Override
    public void bindHolder(@NonNull Markwon markwon, @NonNull Holder holder, @NonNull TableBlock node) {

        Table table = map.get(node);
        if (table == null) {
            table = Table.parse(markwon, node);
            map.put(node, table);
        }

        // check if this exact TableBlock was already applied
        // set tag of tableLayoutResId as it's 100% to be present (we still allow 0 as
        // tableIdRes if tableLayoutResId has TableLayout as root view)
        final TableLayout layout = holder.tableLayout;
        PacMap tablePacMap = ((PacMap)layout.getTag());
        if (table == null
                || table == layout.getTag()) {
            return;
        }

        // set this flag to indicate what table instance we current display
        layout.setTag(table);

        final TableEntryPlugin plugin = markwon.getPlugin(TableEntryPlugin.class);
        if (plugin == null) {
            throw new IllegalStateException("No TableEntryPlugin is found. Make sure that it " +
                    "is _used_ whilst configuring Markwon instance");
        }

        // we must remove unwanted ones (rows and columns)

        final TableEntryTheme theme = plugin.theme();
        final int borderWidth;
        final int borderColor;
        final int cellPadding;
        {
            final Text textView = ensureTextView(layout, 0, 0);
            borderWidth = theme.tableBorderWidth(new Paint());
            borderColor = theme.tableBorderColor(new Paint());
            cellPadding = theme.tableCellPadding();
        }

        ensureTableBorderBackground(layout, borderWidth, borderColor);

        //noinspection SuspiciousNameCombination
//        layout.setPadding(borderWidth, borderWidth, borderWidth, borderWidth);
//        layout.setClipToPadding(borderWidth == 0);

        final List<Table.Row> rows = table.rows();

        final int rowsSize = rows.size();

        // all rows should have equal number of columns
        final int columnsSize = rowsSize > 0
                ? rows.get(0).columns().size()
                : 0;

        Table.Row row;
        Table.Column column;

        TableRow tableRow;

        for (int y = 0; y < rowsSize; y++) {

            row = rows.get(y);
            tableRow = ensureRow(layout, y);

            for (int x = 0; x < columnsSize; x++) {

                column = row.columns().get(x);

                final Text textView = ensureTextView(layout, y, x);
                Font font =  new Font.Builder("normal").setWeight(textGravity(column.alignment(), cellTextCenterVertical)).build();
                textView.setFont(font);
                //textView.getPaint().setFakeBoldText(row.header());

                // apply padding only if not specified in theme (otherwise just use the value from layout)
                if (cellPadding > 0) {
                    textView.setPadding(cellPadding, cellPadding, cellPadding, cellPadding);
                }

                ensureTableBorderBackground(textView, borderWidth, borderColor);
                markwon.setParsedMarkdown(textView, column.content());
            }

            // row appearance
            if (row.header()) {
               // tableRow.setBackgroundColor(theme.tableHeaderRowBackgroundColor());
            } else {
                // as we currently have no support for tables without head
                // we shift even/odd calculation a bit (head should not be included in even/odd calculation)
                final boolean isEven = (y % 2) == 1;
                if (isEven) {
                  //  tableRow.setBackgroundColor(theme.tableEvenRowBackgroundColor());
                } else {
                    // just take first
                    final Text textView = ensureTextView(layout, y, 0);
//                    tableRow.setBackgroundColor(
//                            theme.tableOddRowBackgroundColor(textView.getPaint()));
                }
            }
        }

        // clean up here of un-used rows and columns
        removeUnused(layout, rowsSize, columnsSize);
    }

    @NonNull
    private TableRow ensureRow(@NonNull TableLayout layout, int row) {

        final int count = layout.getChildCount();

        // fill the requested views until we have added the `row` one
        if (row >= count) {

            final Context context = layout.getContext();

            int diff = row - count + 1;
            while (diff > 0) {
                layout.addComponent(new TableRow(context));
                diff -= 1;
            }
        }

        // return requested child (here it always should be the last one)
        return (TableRow) layout.getComponentAt(row);
    }

    @NonNull
    private Text ensureTextView(@NonNull TableLayout layout, int row, int column) {

        final TableRow tableRow = ensureRow(layout, row);
        final int count = tableRow.getChildCount();

        if (column >= count) {

            final LayoutScatter inflater = ensureInflater(layout.getContext());

            boolean textViewChecked = false;

            Component view;
            Text textView;
            ComponentContainer.LayoutConfig layoutParams;

            int diff = column - count + 1;

            while (diff > 0) {

                view = inflater.parse(textLayoutResId, tableRow, false);

                // we should have `match_parent` as height (important for borders and text-vertical-align)
                layoutParams = view.getLayoutConfig();
                if (layoutParams.height != ComponentContainer.LayoutConfig.MATCH_PARENT) {
                    layoutParams.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
                }

                // it will be enough to check only once
                if (!textViewChecked) {

                    if (textIdRes == 0) {
                        if (!(view instanceof Text)) {
                            try {
                                final Resource name = layout.getContext().getResourceManager().getResource(textLayoutResId);
                                throw new IllegalStateException(String.format("textLayoutResId(R.layout.%s) " +
                                        "has other than TextView root view. Specify TextView ID explicitly", name));
                            } catch (Exception ignored) {

                            }
                        }
                        textView = (Text) view;
                    } else {
                        textView = (Text) view.findComponentById(textIdRes);
                        if (textView == null) {
                            try {
                            final Resource r = layout.getContext().getResourceManager().getResource(textIdRes);
                            final Resource layoutName = layout.getContext().getResourceManager().getResource((textLayoutResId));
                            final Resource idName = layout.getContext().getResourceManager().getResource((textIdRes));
                            throw new NullPointerException(String.format("textLayoutResId(R.layout.%s) " +
                                    "has no TextView found by id(R.id.%s): %s", layoutName, idName, view));
                            } catch (Exception ignored) {

                            }
                        }
                    }
                    // mark as checked
                    textViewChecked = true;
                } else {
                    if (textIdRes == 0) {
                        textView = (Text) view;
                    } else {
                        textView = (Text) view.findComponentById(textIdRes);
                    }
                }

                // we should set SpannableFactory during creation (to avoid another setText method)
                //textView.setSpannableFactory(NoCopySpannableFactory.getInstance());
                tableRow.addComponent(textView);

                diff -= 1;
            }
        }

        // we can skip all the validation here as we have validated our views whilst inflating them
        final Component last = tableRow.getComponentAt(column);
        if (textIdRes == 0) {
            return (Text) last;
        } else {
            return (Text) last.findComponentById(textIdRes);
        }
    }

    private void ensureTableBorderBackground(@NonNull Component view, @Px int borderWidth, @ColorInt int borderColor) {
        if (borderWidth == 0) {
            view.setBackground(null);
        } else {
            /*
            final Drawable drawable = (Drawable) view.getBackgroundElement();
            if (!(drawable instanceof TableBorderDrawable)) {
                final TableBorderDrawable borderDrawable = new TableBorderDrawable();
                borderDrawable.update(borderWidth, borderColor);
                view.setBackground(borderDrawable);
            } else {
                ((TableBorderDrawable) drawable).update(borderWidth, borderColor);
            }
            */
        }
    }

    @NonNull
    private LayoutScatter ensureInflater(@NonNull Context context) {
        if (inflater == null) {
            inflater = LayoutScatter.getInstance(context);
        }
        return inflater;
    }

    @SuppressWarnings("WeakerAccess")
    @VisibleForTesting
    static void removeUnused(@NonNull TableLayout layout, int usedRows, int usedColumns) {

        // clean up rows
        final int rowsCount = layout.getChildCount();
        if (rowsCount > usedRows) {
            layout.removeComponents(usedRows, (rowsCount - usedRows));
        }

        // validate columns
        // here we can use usedRows as children count

        TableRow tableRow;
        int columnCount;

        for (int i = 0; i < usedRows; i++) {
            tableRow = (TableRow) layout.getComponentAt(i);
            columnCount = tableRow.getChildCount();
            if (columnCount > usedColumns) {
                tableRow.removeComponents(usedColumns, (columnCount - usedColumns));
            }
        }
    }

    @Override
    public void clear() {
        map.clear();
    }

    public static class Holder extends MarkwonAdapter.Holder {

        final TableLayout tableLayout;

        Holder(boolean isRecyclable, @IdRes int tableLayoutIdRes, @NonNull Component itemView) {
            super(itemView);

            // we must call this method only once (it's somehow _paired_ inside, so
            // any call in `onCreateViewHolder` or `onBindViewHolder` will log an error
            // `isRecyclable decremented below 0` which make little sense here)
            setIsRecyclable(isRecyclable);

            final TableLayout tableLayout;
            if (tableLayoutIdRes == 0) {
                // try to cast directly
                if (!(itemView instanceof TableLayout)) {
                    throw new IllegalStateException("Root view is not TableLayout. Please provide " +
                            "TableLayout ID explicitly");
                }
                tableLayout = (TableLayout) itemView;
            } else {
                tableLayout = requireView(tableLayoutIdRes);
            }
            this.tableLayout = tableLayout;
        }
    }

    // we will use gravity instead of textAlignment because min sdk is 16 (textAlignment starts at 17)
    @SuppressWarnings("WeakerAccess")
    @SuppressLint("RtlHardcoded")
    @VisibleForTesting
    static int textGravity(@NonNull Table.Alignment alignment, boolean cellTextCenterVertical) {

        final int gravity;

        switch (alignment) {

            case LEFT:
                gravity = LayoutAlignment.LEFT;
                break;

            case CENTER:
                gravity = LayoutAlignment.HORIZONTAL_CENTER;
                break;

            case RIGHT:
                gravity = LayoutAlignment.RIGHT;
                break;

            default:
                throw new IllegalStateException("Unknown table alignment: " + alignment);
        }

        if (cellTextCenterVertical) {
            return gravity | LayoutAlignment.VERTICAL_CENTER;
        }

        // do not center vertically
        return gravity;
    }

    static class BuilderImpl implements Builder {

        private int tableLayoutResId;
        private int tableIdRes;

        private int textLayoutResId;
        private int textIdRes;

        private boolean cellTextCenterVertical = true;

        private boolean isRecyclable = true;

        @NonNull
        @Override
        public Builder tableLayout(int tableLayoutResId, int tableIdRes) {
            this.tableLayoutResId = tableLayoutResId;
            this.tableIdRes = tableIdRes;
            return this;
        }

        @NonNull
        @Override
        public Builder tableLayoutIsRoot(int tableLayoutResId) {
            this.tableLayoutResId = tableLayoutResId;
            this.tableIdRes = 0;
            return this;
        }

        @NonNull
        @Override
        public Builder textLayout(int textLayoutResId, int textIdRes) {
            this.textLayoutResId = textLayoutResId;
            this.textIdRes = textIdRes;
            return this;
        }

        @NonNull
        @Override
        public Builder textLayoutIsRoot(int textLayoutResId) {
            this.textLayoutResId = textLayoutResId;
            this.textIdRes = 0;
            return this;
        }

        @NonNull
        @Override
        public Builder cellTextCenterVertical(boolean cellTextCenterVertical) {
            this.cellTextCenterVertical = cellTextCenterVertical;
            return this;
        }

        @NonNull
        @Override
        public Builder isRecyclable(boolean isRecyclable) {
            this.isRecyclable = isRecyclable;
            return this;
        }

        @NonNull
        @Override
        public TableEntry build() {

            if (tableLayoutResId == 0) {
                throw new IllegalStateException("`tableLayoutResId` argument is required");
            }

            if (textLayoutResId == 0) {
                throw new IllegalStateException("`textLayoutResId` argument is required");
            }

            return new TableEntry(
                    tableLayoutResId, tableIdRes,
                    textLayoutResId, textIdRes,
                    isRecyclable, cellTextCenterVertical
            );
        }
    }
}
