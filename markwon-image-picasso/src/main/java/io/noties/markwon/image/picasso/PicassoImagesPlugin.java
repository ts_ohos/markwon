package io.noties.markwon.image.picasso;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;
import com.noties.markwon.wrapper.text.Spanned;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.image.AsyncDrawable;
import io.noties.markwon.image.AsyncDrawableLoader;
import io.noties.markwon.image.AsyncDrawableScheduler;
import io.noties.markwon.image.ImageSpanFactory;
import java.util.HashMap;
import java.util.Map;
import ohos.agp.components.Text;
import ohos.app.Context;

import org.commonmark.node.Image;

/**
 * @since 4.0.0
 */
public class PicassoImagesPlugin extends AbstractMarkwonPlugin {

    // TODO YR lack com.squareup.picasso:picasso:2.71828
    interface PicassoStore {

       /* @NonNull
        RequestCreator load(@NonNull AsyncDrawable drawable);*/

        void cancel(@NonNull AsyncDrawable drawable);
    }

    @NonNull
    public static PicassoImagesPlugin create(@NonNull Context context) {
        // new Picasso.Builder(context).build()
        return create();
    }

    @NonNull
    private static PicassoImagesPlugin create() {
        // @NonNull final Picasso picasso
       /* return create(new PicassoStore() {
            @NonNull
            @Override
            public RequestCreator load(@NonNull AsyncDrawable drawable) {
                return picasso.load(drawable.getDestination())
                        .tag(drawable);
            }

            @Override
            public void cancel(@NonNull AsyncDrawable drawable) {
                picasso.cancelTag(drawable);
            }
        });*/
        return null;
    }

    @NonNull
    public static PicassoImagesPlugin create(@NonNull PicassoStore picassoStore) {
        return new PicassoImagesPlugin(picassoStore);
    }

    private final PicassoAsyncDrawableLoader picassoAsyncDrawableLoader;

    @SuppressWarnings("WeakerAccess")
    PicassoImagesPlugin(@NonNull PicassoStore picassoStore) {
        this.picassoAsyncDrawableLoader = new PicassoAsyncDrawableLoader(picassoStore);
    }

    @Override
    public void configureConfiguration(@NonNull MarkwonConfiguration.Builder builder) {
        builder.asyncDrawableLoader(picassoAsyncDrawableLoader);
    }

    @Override
    public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {
        builder.setFactory(Image.class, new ImageSpanFactory());
    }

    @Override
    public void beforeSetText(@NonNull Text textView, @NonNull Spanned markdown) {
        AsyncDrawableScheduler.unschedule(textView);
    }

    @Override
    public void afterSetText(@NonNull Text textView) {
        AsyncDrawableScheduler.schedule(textView);
    }

    private static class PicassoAsyncDrawableLoader extends AsyncDrawableLoader {

        private final PicassoStore picassoStore;
        private final Map<AsyncDrawable, AsyncDrawableTarget> cache = new HashMap<>(2);

        PicassoAsyncDrawableLoader(@NonNull PicassoStore picassoStore) {
            this.picassoStore = picassoStore;
        }

        @Override
        public void load(@NonNull AsyncDrawable drawable) {

            // we must store hard-reference to target (otherwise it will be garbage-collected
            // ad picasso internally stores a target in a weak-reference)
            final AsyncDrawableTarget target = new AsyncDrawableTarget(drawable);
            cache.put(drawable, target);

           /* picassoStore.load(drawable)
                    .into(target);*/
        }

        @Override
        public void cancel(@NonNull AsyncDrawable drawable) {

            cache.remove(drawable);

            picassoStore.cancel(drawable);
        }

        @Nullable
        @Override
        public Drawable placeholder(@NonNull AsyncDrawable drawable) {
            return null;
        }


        private class AsyncDrawableTarget {

            private final AsyncDrawable drawable;

            AsyncDrawableTarget(@NonNull AsyncDrawable drawable) {
                this.drawable = drawable;
            }
        }

      /*  private class AsyncDrawableTarget implements Target {

            private final AsyncDrawable drawable;

            AsyncDrawableTarget(@NonNull AsyncDrawable drawable) {
                this.drawable = drawable;
            }

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                if (cache.remove(drawable) != null) {
                    if (drawable.isAttached() && bitmap != null) {
                        final BitmapDrawable bitmapDrawable = new BitmapDrawable(Resources.getSystem(), bitmap);
                        DrawableUtils.applyIntrinsicBounds(bitmapDrawable);
                        drawable.setResult(bitmapDrawable);
                    }
                }
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                if (cache.remove(drawable) != null) {
                    if (errorDrawable != null
                            && drawable.isAttached()) {
                        DrawableUtils.applyIntrinsicBoundsIfEmpty(errorDrawable);
                        drawable.setResult(errorDrawable);
                    }
                }
                e.printStackTrace();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null
                        && canDeliver()) {
                    DrawableUtils.applyIntrinsicBoundsIfEmpty(placeHolderDrawable);
                    drawable.setResult(placeHolderDrawable);
                }
            }

            private boolean canDeliver() {
                return drawable.isAttached() && cache.containsKey(drawable);
            }
        }*/
    }
}
