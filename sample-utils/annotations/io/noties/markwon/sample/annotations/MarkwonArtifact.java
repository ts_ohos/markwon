package io.noties.markwon.sample.annotations;


import io.noties.markwon.sample.processor.NonNull;
import java.util.Locale;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public enum MarkwonArtifact {
    /**
     * MarkwonArtifact tag.
     */
    CORE,
    EDITOR,
    EXT_LATEX,
    EXT_STRIKETHROUGH,
    EXT_TABLES,
    EXT_TASKLIST,
    HTML,
    IMAGE,
    IMAGE_COIL,
    IMAGE_GLIDE,
    IMAGE_PICASSO,
    INLINE_PARSER,
    LINKIFY,
    RECYCLER,
    RECYCLER_TABLE,
    SIMPLE_EXT,
    SYNTAX_HIGHLIGHT;

    @NonNull
    public String artifactName() {
        return name().toLowerCase(Locale.US).replace('_', '-');
    }
}
