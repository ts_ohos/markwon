# Markwon
本项目是基于开源项目**Markwon**进行适用harmonyos的移植和开发的，可以通过项目标签以及 github地址[https://github.com/noties/Markwon](https://github.com/noties/Markwon) 追踪到原项目

移植版本：v4.6.2

## 1. 项目介绍
### 项目名称：Markwon
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
* Emphasis (*, _)
* Strong emphasis (**, __)
* Strike-through (~~)
* Headers (#{1,6})
* Links ([]() && [][])
* Images
* Thematic break (---, ***, ___)
* Quotes & nested quotes (>{1,})
* Ordered & non-ordered lists & nested ones
* Strike-through (`<s>`, `<strike>`, `<del>`)
* Link (a).  

### 项目移植状态：
项目已完成移植，完成度50%，差异如下
- 主界面点击右上角图标，进入README.md界面不能正常显示内容
- 主界面stable 版本信息栏使用固定信息及样式
- 主界面CHECK FOR UPDATE 点击检测更新版本无对应链接
- 主界面列表条目点击后进入页面不能正常显示内容
- 主界面列表条目标签4个以上不能自动换行  
添加了demo模块用于效果展示；

### 调用差异：暂无
### 原项目地址：[https://github.com/noties/Markwon](https://github.com/noties/Markwon)
### 编程语言：java

## 2. 集成指引
### 方式一
1. 下载或自行编译生成Markwon的.har文件，文件路径为：./demo/libs/Markwon-debug.har。
2. 自行编译时，需要注意要自行添加签名。
3. 导入你的harmonyos项目模块的**./libs**中。
4. 在模块下的**build.gradle**中确认依赖**./libs**下的.har包，``implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])``。
5. 在代码中使用。

### 方式二：
1. 在根目录的`build.gradle`文件中添加`mavenCentral()`：
   ```groovy
   // Top-level build file where you can add configuration options common to all sub-projects/modules.
   ...
   buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
   ```

2. 在module目录下的`build.gradle`文件中添加``

   ```groovy
   ...
   dependencies {
       ...
       implementation 'com.gitee.ts_ohos:markwon:1.0.1'
   }
   ```

## 3. 使用说明
更多用法参考本项目的**demo**，原项目**Java Doc**与**OHOS**的文档。  
主要用法如下：  
### Inline HTML

```html
<u><i>H<sup>T<sub>M</sub></sup><b><s>L</s></b></i></u>
```

<u><i>H<sup>T<sub>M</sub></sup><b><s>L</s></b></i></u>

---
```xml
<ScrollView
  android:id="@+id/scroll_view"
  android:layout_width="match_parent"
  android:layout_height="match_parent"
  android:layout_marginTop="?android:attr/actionBarSize">

  <TextView
    android:id="@+id/text"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_margin="16dip"
    android:lineSpacingExtra="2dip"
    android:textSize="16sp"
    tools:text="yo\nman" />

</ScrollView>
```

```java
    // username, repo, branch, lastPathSegment

    String RE_FILE = "^"+TextUtils.getHttps()+"github\\.com\\/([\\w-.]+?)\\/([\\w-.]+?)\\/(?:blob|raw)\\/([\\w-.]+?)\\/(.+)$";
    private static final String RE_REPOSITORY = "^"+TextUtils.getHttps()+"github.com/([\\w-.]+?)/([\\w-.]+?)/*$";

    public static Pair<String, String> parseRepository(String url) {

        Pattern pattern = Pattern.compile(RE_REPOSITORY);
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches()) {
            String user = matcher.group(1);
            String repository = matcher.group(2);
            if (TextUtils.isEmpty(user) || TextUtils.isEmpty(user)) {
                return new Pair<>(null, null);
            } else {
                return new Pair<>(user, repository);
            }
        } else {
            return new Pair<String, String>(null, null);
        }
    }
```

```java
    private static final String SAMPLE_PREFIX = "io.noties.markwon.app.";

    @NonNull
    public static List<Sample> readSamples(@NonNull Context context) {

        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/samples.json");
        try (InputStream inputStream = rawFileEntry.openRawFile()) {
            return readSamples(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
```

## 4. 效果演示
**1. 主界面展示**  
**2. 点击item中plugin标签，显示选择结果**

**\*注 : demo中的图片是点击不同标签，进行筛选显示结果**

<img src="./pic/demo_1.png" width="200"> <img src="./pic/demo_2.png" width="200">

## 5. 版本迭代
- v1.0.0 基于原项目最新版本，移植功能及提交。
- v1.0.1 更新harmonyos API至5。

## 6. 版本和许可信息
- Apache License 2.0
- [LICENSE](https://gitee.com/ts_ohos/markwon/blob/master/LICENSE)

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```