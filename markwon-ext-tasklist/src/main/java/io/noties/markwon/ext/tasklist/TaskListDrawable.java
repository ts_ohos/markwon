package io.noties.markwon.ext.tasklist;

import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.IntRange;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;
import ohos.agp.graphics.Surface;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

/**
 * @since 1.0.1
 */
@SuppressWarnings("WeakerAccess")
public class TaskListDrawable extends Drawable {

    // represent ratios (not exact coordinates)
    private static final Point POINT_0 = new Point(2.75F / 18, 8.25F / 18);
    private static final Point POINT_1 = new Point(7.F / 18, 12.5F / 18);
    private static final Point POINT_2 = new Point(15.25F / 18, 4.75F / 18);

    private final int checkedFillColor;
    private final int normalOutlineColor;

    private final Paint paint = new Paint();
    private final RectFloat rectF = new RectFloat();

    private final Paint checkMarkPaint = new Paint();
    private final Path checkMarkPath = new Path();

    private boolean isChecked;

    // unfortunately we cannot rely on TextView to be LAYER_TYPE_SOFTWARE
    // if we could we would draw our checkMarkPath with PorterDuff.CLEAR
    public TaskListDrawable(
            @ColorInt int checkedFillColor,
            @ColorInt int normalOutlineColor,
            @ColorInt int checkMarkColor) {
        this.checkedFillColor = checkedFillColor;
        this.normalOutlineColor = normalOutlineColor;

        checkMarkPaint.setColor(new Color(checkMarkColor));
        checkMarkPaint.setStyle(Paint.Style.STROKE_STYLE);
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);

        // we should exclude stroke with from final bounds (half of the strokeWidth from all sides)

        // we should have square shape
        final float min = Math.min(bounds.getWidth(), bounds.getHeight());
        final float stroke = min / 8;

        final float side = min - stroke;
        rectF.modify(0, 0, side, side);

        paint.setStrokeWidth(stroke);
        checkMarkPaint.setStrokeWidth(stroke);

        checkMarkPath.reset();

        POINT_0.moveTo(checkMarkPath, side);
        POINT_1.lineTo(checkMarkPath, side);
        POINT_2.lineTo(checkMarkPath, side);
    }

    @Override
    public void setState(int[] state) {

    }

    @Override
    public void draw(@NonNull Canvas canvas) {

        final Paint.Style style;
        final int color;

        if (isChecked) {
            style = Paint.Style.FILL_STYLE;
            color = checkedFillColor;
        } else {
            style = Paint.Style.STROKE_STYLE;
            color = normalOutlineColor;
        }
        paint.setStyle(style);
        paint.setColor(new Color(color));

        final Rect bounds = getBounds();

        final float left = (bounds.getWidth() - rectF.getWidth()) / 2;
        final float top = (bounds.getHeight() - rectF.getHeight()) / 2;

        final float radius = rectF.getWidth() / 8;

        final int save = canvas.save();
        try {

            canvas.translate(left, top);

            canvas.drawRoundRect(rectF, radius, radius, paint);

            if (isChecked) {
                canvas.drawPath(checkMarkPath, checkMarkPaint);
            }
        } finally {
            canvas.restoreToCount(save);
        }
    }

    @Override
    public void setAlpha(@IntRange(from = 0, to = 255) int alpha) {
        paint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        paint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return Surface.PixelFormat.PIXEL_FORMAT_YCRCB_420_SP.value();
    }//return PixelFormat.OPAQUE

    @Override
    public boolean isStateful() {
        return true;
    }

    @Override
    protected boolean onStateChange(int[] state) {

        final boolean checked;

        final int length = state != null
                ? state.length
                : 0;

        if (length > 0) {

            boolean inner = false;

            for (int i = 0; i < length; i++) {
                if (/*android.R.attr.state_checked*/1 == state[i]) {
                    inner = true;
                    break;
                }
            }
            checked = inner;
        } else {
            checked = false;
        }

        final boolean result = checked != isChecked;
        if (result) {
            invalidateSelf();
            isChecked = checked;
        }

        return result;
    }

    private static class Point {

        final float x;
        final float y;

        Point(float x, float y) {
            this.x = x;
            this.y = y;
        }

        void moveTo(@NonNull Path path, float side) {
            path.moveTo(side * x, side * y);
        }

        void lineTo(@NonNull Path path, float side) {
            path.lineTo(side * x, side * y);
        }
    }
}
