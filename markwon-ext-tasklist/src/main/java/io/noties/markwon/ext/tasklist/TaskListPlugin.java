package io.noties.markwon.ext.tasklist;

import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.utils.LogUtils;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.MarkwonVisitor;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;

import org.commonmark.parser.Parser;

/**
 * @since 3.0.0
 */
public class TaskListPlugin extends AbstractMarkwonPlugin {

    public static TaskListPlugin create(@NonNull PixelMapElement drawable) {
        return new TaskListPlugin(drawable);
    }

    private TaskListPlugin(@NonNull PixelMapElement drawable) {
        this.drawable = null;
    }

    /**
     * Supplied Drawable must be stateful ({@link Drawable#isStateful()} returns true). If a task
     * is marked as done, then this drawable will be updated with an {@code int[] { android.R.attr.state_checked }}
     * as the state, otherwise an empty array will be used. This library provides a ready to be
     * used Drawable: {@link TaskListDrawable}
     *
     * @see TaskListDrawable
     */
    @NonNull
    public static TaskListPlugin create(@NonNull Drawable drawable) {
        return new TaskListPlugin(drawable);
    }

    @NonNull
    public static TaskListPlugin create(@NonNull Context context) {

        // by default we will be using link color for the checkbox color
        // & window background as a checkMark color
        ohos.global.resource.ResourceManager resManager = context.getResourceManager();
         int linkColor = 0;
         int backgroundColor = 0;
        try {
            linkColor = resManager.getElement(ResourceTable.Color_link_text_light).getColor();
            //resolve(context, android.R.attr.textColorLink);
            backgroundColor = resManager.getElement(ResourceTable.Color_black).getColor();
            //resolve(context, android.R.attr.colorBackground);
        } catch (Exception e) {
            LogUtils.d(LogUtils.HI_LOG_LABEL, "TaskListPlugin e:" + e);
        }

        return new TaskListPlugin(new TaskListDrawable(linkColor, linkColor, backgroundColor));
    }

    @NonNull
    public static TaskListPlugin create(
            @ColorInt int checkedFillColor,
            @ColorInt int normalOutlineColor,
            @ColorInt int checkMarkColor) {
        return new TaskListPlugin(new TaskListDrawable(
                checkedFillColor,
                normalOutlineColor,
                checkMarkColor));
    }

    private final Drawable drawable;

    private TaskListPlugin(@NonNull Drawable drawable) {
        this.drawable = drawable;
    }

    @Override
    public void configureParser(@NonNull Parser.Builder builder) {
        builder.postProcessor(new TaskListPostProcessor());
    }

    @Override
    public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {
        builder.setFactory(TaskListItem.class, new TaskListSpanFactory(drawable));
    }

    @Override
    public void configureVisitor(@NonNull MarkwonVisitor.Builder builder) {
        builder.on(TaskListItem.class, (visitor, taskListItem) -> {

            final int length = visitor.length();

            visitor.visitChildren(taskListItem);

            TaskListProps.DONE.set(visitor.renderProps(), taskListItem.isDone());

            visitor.setSpansForNode(taskListItem, length);

            if (visitor.hasNext(taskListItem)) {
                visitor.ensureNewLine();
            }
        });
    }

//    private static int resolve(@NonNull Context context, @AttrRes int attr) {
//        final TypedValue typedValue = new TypedValue();
//        final int[] attrs = new int[]{attr};
//        final TypedArray typedArray = context.obtainStyledAttributes(typedValue.data, attrs);
//        try {
//            return typedArray.getColor(0, 0);
//        } finally {
//            typedArray.recycle();
//        }
//    }
}
