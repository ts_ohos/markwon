package io.noties.markwon.html.tag;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.utils.TextUtils;

import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.RenderProps;
import io.noties.markwon.SpanFactory;
import io.noties.markwon.core.CoreProps;
import io.noties.markwon.html.HtmlTag;
import java.util.Collection;
import java.util.Collections;

import org.commonmark.node.Link;

public class LinkHandler extends SimpleTagHandler {
    @Nullable
    @Override
    public Object getSpans(@NonNull MarkwonConfiguration configuration, @NonNull RenderProps renderProps, @NonNull
        HtmlTag tag) {
        final String destination = tag.attributes().get("href");
        if (!TextUtils.isEmpty(destination)) {
            final SpanFactory spanFactory = configuration.spansFactory().get(Link.class);
            if (spanFactory != null) {

                CoreProps.LINK_DESTINATION.set(
                        renderProps,
                        destination
                );

                return spanFactory.getSpans(configuration, renderProps);
            }
        }
        return null;
    }

    @NonNull
    @Override
    public Collection<String> supportedTags() {
        return Collections.singleton("a");
    }
}
