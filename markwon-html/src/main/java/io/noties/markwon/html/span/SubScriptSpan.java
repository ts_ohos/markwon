package io.noties.markwon.html.span;

import io.noties.markwon.html.HtmlPlugin;
import ohos.agp.text.TextForm;

/**
 * 文字偏下并缩小字体0.75F
 * @author yangrui
 */
public class SubScriptSpan extends TextForm {

    /**
     * 构建特殊的 SubScriptSpan
     */
    public SubScriptSpan() {
        setSubscript(true);
        setTextSize((int) (getTextSize()* HtmlPlugin.SCRIPT_DEF_TEXT_SIZE_RATIO));
    }
}
