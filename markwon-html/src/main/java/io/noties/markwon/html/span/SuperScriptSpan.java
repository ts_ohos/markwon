package io.noties.markwon.html.span;

import io.noties.markwon.html.HtmlPlugin;
import ohos.agp.text.TextForm;

/**
 * 文字偏上并缩小字体0.75F
 * @author yangrui
 */
public class SuperScriptSpan extends TextForm {

    /**
     * 构建特殊的 SuperScriptSpan
     */
    public SuperScriptSpan() {
        setSuperscript(true);
        setTextSize((int) (getTextSize()* HtmlPlugin.SCRIPT_DEF_TEXT_SIZE_RATIO));
    }
}
