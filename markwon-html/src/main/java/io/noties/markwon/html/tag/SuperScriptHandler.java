package io.noties.markwon.html.tag;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;

import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.RenderProps;
import io.noties.markwon.html.HtmlTag;
import io.noties.markwon.html.span.SuperScriptSpan;
import java.util.Collection;
import java.util.Collections;

public class SuperScriptHandler extends SimpleTagHandler {
    @Nullable
    @Override
    public Object getSpans(@NonNull MarkwonConfiguration configuration, @NonNull RenderProps renderProps, @NonNull
        HtmlTag tag) {
        return new SuperScriptSpan();
    }

    @NonNull
    @Override
    public Collection<String> supportedTags() {
        return Collections.singleton("sup");
    }
}
