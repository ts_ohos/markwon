package io.noties.markwon.syntax;

import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.SpannableStringBuilder;

import io.noties.prism4j.Prism4j;

public interface Prism4jTheme {

    @ColorInt
    int background();

    @ColorInt
    int textColor();

    void apply(
            @NonNull String language,
            @NonNull Prism4j.Syntax syntax,
            @NonNull SpannableStringBuilder builder,
            int start,
            int end
    );
}
