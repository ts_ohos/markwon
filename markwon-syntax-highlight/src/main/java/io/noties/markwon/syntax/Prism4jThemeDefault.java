package io.noties.markwon.syntax;


import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.text.SpannableStringBuilder;
import com.noties.markwon.wrapper.text.Spanned;

import io.noties.markwon.core.spans.EmphasisSpan;
import io.noties.markwon.core.spans.StrongEmphasisSpan;
import ohos.agp.text.TextForm;

public class Prism4jThemeDefault extends Prism4jThemeBase {

    @NonNull
    public static Prism4jThemeDefault create() {
        return new Prism4jThemeDefault(0xFFf5f2f0);
    }

    /**
     * @since 3.0.0
     */
    @NonNull
    public static Prism4jThemeDefault create(@ColorInt int background) {
        return new Prism4jThemeDefault(background);
    }

    private final int background;

    private Prism4jThemeDefault(@ColorInt int background) {
        this.background = background;
    }

    @Override
    public int background() {
        return background;
    }

    @Override
    public int textColor() {
        return 0xdd000000;
    }

    @NonNull
    @Override
    protected ColorHashMap init() {
        return new ColorHashMap()
                .add(0xFF708090, "comment", "prolog", "doctype", "cdata")
                .add(0xFF999999, "punctuation")
                .add(0xFF990055, "property", "tag", "boolean", "number", "constant", "symbol", "deleted")
                .add(0xFF669900, "selector", "attr-name", "string", "char", "builtin", "inserted")
                .add(0xFF9a6e3a, "operator", "entity", "url")
                .add(0xFF0077aa, "atrule", "attr-value", "keyword")
                .add(0xFFDD4A68, "function", "class-name")
                .add(0xFFee9900, "regex", "important", "variable");
    }

    @Override
    protected void applyColor(
            @NonNull String language,
            @NonNull String type,
            @Nullable String alias,
            @ColorInt int color,
            @NonNull SpannableStringBuilder builder,
            int start,
            int end) {

        if ("css".equals(language) && isOfType("string", type, alias)) {
            super.applyColor(language, type, alias, 0xFF9a6e3a, builder, start, end);
            builder.mergeForm(new TextForm().setTextBackgroundColor(0x80ffffff));
            builder.revertForm();
            return;
        }

        if (isOfType("namespace", type, alias)) {
            color = applyAlpha(color);
        }

        super.applyColor(language, type, alias, color, builder, start, end);

        if (isOfType("important", type, alias)
                || isOfType("bold", type, alias)) {
            builder.setSpan(new StrongEmphasisSpan(), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        if (isOfType("italic", type, alias)) {
            builder.setSpan(new EmphasisSpan(), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }
}
