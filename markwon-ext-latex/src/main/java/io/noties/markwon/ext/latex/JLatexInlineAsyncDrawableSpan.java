package io.noties.markwon.ext.latex;

import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.IntRange;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import io.noties.markwon.core.MarkwonTheme;
import io.noties.markwon.image.AsyncDrawable;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;

/**
 * @since 4.3.0
 */
class JLatexInlineAsyncDrawableSpan extends JLatexAsyncDrawableSpan {

    private final AsyncDrawable drawable;

    JLatexInlineAsyncDrawableSpan(@NonNull MarkwonTheme theme, @NonNull JLatextAsyncDrawable drawable, @ColorInt int color) {
        super(theme, drawable, color);
        this.drawable = drawable;
    }

    @Override
    public int getSize(
            @NonNull Paint paint,
            CharSequence text,
            @IntRange(from = 0) int start,
            @IntRange(from = 0) int end,
            @Nullable Paint.FontMetrics fm) {

        // if we have no async drawable result - we will just render text

        final int size;

        if (drawable.hasResult()) {

            final Rect rect = drawable.getBounds();

            if (fm != null) {
                final int half = rect.bottom / 2;
                fm.ascent = -half;
                fm.descent = half;

                fm.top = fm.ascent;
                fm.bottom = 0;
            }

            size = rect.right;

        } else {

            // NB, no specific text handling (no new lines, etc)
            size = (int) (paint.measureText(text.toString()) + .5F);
        }

        return size;
    }
}
