package io.noties.markwon.ext.latex;

import com.noties.markwon.annotation.NonNull;

import io.noties.markwon.image.AsyncDrawable;
import io.noties.markwon.image.AsyncDrawableLoader;
import io.noties.markwon.image.ImageSizeResolver;

/**
 * @since 4.3.0
 */
class JLatextAsyncDrawable extends AsyncDrawable {

    private final boolean isBlock;

    JLatextAsyncDrawable(
        @NonNull String destination,
        @NonNull AsyncDrawableLoader loader,
        @NonNull ImageSizeResolver imageSizeResolver,
        boolean isBlock
    ) {
        super(destination, loader, imageSizeResolver, null);
        this.isBlock = isBlock;
    }

    public boolean isBlock() {
        return isBlock;
    }
}
