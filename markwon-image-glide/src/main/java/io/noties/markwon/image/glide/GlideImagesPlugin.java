package io.noties.markwon.image.glide;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;
import com.noties.markwon.wrapper.text.Spanned;
import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.image.*;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.commonmark.node.Image;


/**
 * @since 4.0.0
 */
public class GlideImagesPlugin extends AbstractMarkwonPlugin {
    // TODO YR lack com.github.bumptech.glide:glide:4.11.0
    public interface GlideStore {

       /* @NonNull
        RequestBuilder<Drawable> load(@NonNull AsyncDrawable drawable);

        void cancel(@NonNull Target<?> target);*/
    }

    @NonNull
    public static GlideImagesPlugin create(@NonNull final Context context) {
        // @since 4.5.0 cache RequestManager
        //  sometimes `cancel` would be called after activity is destroyed,
        //  so `Glide.with(context)` will throw an exception
        return create();
    }

    @NonNull
    private static GlideImagesPlugin create() { // @NonNull final RequestManager requestManager
       /* return create(new GlideStore() {
            @NonNull
            @Override
            public RequestBuilder<Drawable> load(@NonNull AsyncDrawable drawable) {
                return requestManager.load(drawable.getDestination());
            }

            @Override
            public void cancel(@NonNull Target<?> target) {
                requestManager.clear(target);
            }
        });*/
        return null;
    }

    @NonNull
    public static GlideImagesPlugin create(@NonNull GlideStore glideStore) {
        return new GlideImagesPlugin(glideStore);
    }

    private final GlideAsyncDrawableLoader glideAsyncDrawableLoader;

    @SuppressWarnings("WeakerAccess")
    GlideImagesPlugin(@NonNull GlideStore glideStore) {
        this.glideAsyncDrawableLoader = new GlideAsyncDrawableLoader(glideStore);
    }

    @Override
    public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {
        builder.setFactory(Image.class, new ImageSpanFactory());
    }

    @Override
    public void configureConfiguration(@NonNull MarkwonConfiguration.Builder builder) {
        builder.asyncDrawableLoader(glideAsyncDrawableLoader);
    }

    @Override
    public void beforeSetText(@NonNull Text textView, @NonNull Spanned markdown) {
        AsyncDrawableScheduler.unschedule(textView);
    }

    @Override
    public void afterSetText(@NonNull Text textView) {
        AsyncDrawableScheduler.schedule(textView);
    }

    private static class GlideAsyncDrawableLoader extends AsyncDrawableLoader {

        private final GlideStore glideStore;
        // private final Map<AsyncDrawable, Target<?>> cache = new HashMap<>(2);

        GlideAsyncDrawableLoader(@NonNull GlideStore glideStore) {
            this.glideStore = glideStore;
        }

        @Override
        public void load(@NonNull AsyncDrawable drawable) {
           /* final Target<Drawable> target = new AsyncDrawableTarget(drawable);
            cache.put(drawable, target);
            glideStore.load(drawable)
                    .into(target);*/
        }

        @Override
        public void cancel(@NonNull AsyncDrawable drawable) {
           /* final Target<?> target = cache.remove(drawable);
            if (target != null) {
                glideStore.cancel(target);
            }*/
        }

        @Nullable
        @Override
        public Drawable placeholder(@NonNull AsyncDrawable drawable) {
            return null;
        }

        private class AsyncDrawableTarget { // extends CustomTarget<Drawable>

            private final AsyncDrawable drawable;

            AsyncDrawableTarget(@NonNull AsyncDrawable drawable) {
                this.drawable = drawable;
            }

          /*

            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                if (cache.remove(drawable) != null) {
                    if (drawable.isAttached()) {
                        DrawableUtils.applyIntrinsicBoundsIfEmpty(resource);
                        drawable.setResult(resource);
                    }
                }
            }

            @Override
            public void onLoadStarted(@Nullable Drawable placeholder) {
                if (placeholder != null
                        && drawable.isAttached()) {
                    DrawableUtils.applyIntrinsicBoundsIfEmpty(placeholder);
                    drawable.setResult(placeholder);
                }
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                if (cache.remove(drawable) != null) {
                    if (errorDrawable != null
                            && drawable.isAttached()) {
                        DrawableUtils.applyIntrinsicBoundsIfEmpty(errorDrawable);
                        drawable.setResult(errorDrawable);
                    }
                }
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                // we won't be checking if target is still present as cancellation
                // must remove target anyway
                if (drawable.isAttached()) {
                    drawable.clearResult();
                }
            }*/
        }
    }
}
