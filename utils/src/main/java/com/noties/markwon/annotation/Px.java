package com.noties.markwon.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.PARAMETER, java.lang.annotation.ElementType.FIELD, java.lang.annotation.ElementType.LOCAL_VARIABLE})
@Dimension(unit=1)
public @interface Px {}


/* Location:              C:\Users\18871\Desktop\annotation-1.1.0.jar!\androidx\annotation\Px.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */