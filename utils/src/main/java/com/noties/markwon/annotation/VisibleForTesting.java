package com.noties.markwon.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.CLASS)
public @interface VisibleForTesting
{
    public static final int PRIVATE = 2;
    public static final int PACKAGE_PRIVATE = 3;
    public static final int PROTECTED = 4;
    public static final int NONE = 5;

    int otherwise() default 2;
}


/* Location:              C:\Users\18871\Desktop\annotation-1.1.0.jar!\androidx\annotation\VisibleForTesting.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */