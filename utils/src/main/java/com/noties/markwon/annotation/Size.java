package com.noties.markwon.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.CLASS)
@Target({java.lang.annotation.ElementType.PARAMETER, java.lang.annotation.ElementType.LOCAL_VARIABLE, java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.FIELD, java.lang.annotation.ElementType.ANNOTATION_TYPE})
public @interface Size
{
  long value() default -1L;
  
  long min() default Long.MIN_VALUE;
  
  long max() default Long.MAX_VALUE;
  
  long multiple() default 1L;
}


/* Location:              C:\Users\18871\Desktop\annotation-1.1.0.jar!\androidx\annotation\Size.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */