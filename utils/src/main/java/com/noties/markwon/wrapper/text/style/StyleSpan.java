package com.noties.markwon.wrapper.text.style;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class StyleSpan {
    private final int mStyle;

    public StyleSpan(int style) {
        mStyle = style;
    }
}
