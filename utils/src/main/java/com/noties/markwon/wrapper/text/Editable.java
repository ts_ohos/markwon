package com.noties.markwon.wrapper.text;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public interface Editable extends Spannable{
    void insert(int pendingNewLineIndex, String pendingNewLineContent);

    void replace(int clearLineStart, int clearLineEnd, String s);
}
