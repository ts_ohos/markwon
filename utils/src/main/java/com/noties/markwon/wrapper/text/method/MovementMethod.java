package com.noties.markwon.wrapper.text.method;

import com.noties.markwon.wrapper.text.Spannable;
import com.noties.markwon.wrapper.view.MotionEvent;

import ohos.agp.components.DragEvent;
import ohos.agp.components.Text;
import ohos.multimodalinput.event.KeyEvent;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public interface MovementMethod {
    public void initialize(Text widget, Spannable text);

    public boolean onKeyDown(Text widget, Spannable text, int keyCode, KeyEvent event);

    public boolean onKeyUp(Text widget, Spannable text, int keyCode, KeyEvent event);

    /**
     * If the key listener wants to other kinds of key events, return true,
     * otherwise return false and the caller (i.e. the widget host)
     * will handle the key.
     */
    public boolean onKeyOther(Text view, Spannable text, KeyEvent event);

    public void onTakeFocus(Text widget, Spannable text, int direction);

    public boolean onTrackballEvent(Text widget, Spannable text, MotionEvent event);

    public boolean onTouchEvent(Text widget, Spannable text, MotionEvent event);

    public boolean onTouchEvent(Text widget, Spannable text, DragEvent event);

    public boolean onGenericMotionEvent(Text widget, Spannable text, MotionEvent event);

    /**
     * Returns true if this movement method allows arbitrary selection
     * of any text; false if it has no selection (like a movement method
     * that only scrolls) or a constrained selection (for example
     * limited to links.  The "Select All" menu item is disabled
     * if arbitrary selection is not allowed.
     */
    public boolean canSelectArbitrarily();
}

