package com.noties.markwon.wrapper.text.style;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.TextPaint;


/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public abstract class MetricAffectingSpan {
    public abstract void updateMeasureState(@NonNull TextPaint textPaint);

    public void updateDrawState(@NonNull TextPaint tp) {

    }
}
