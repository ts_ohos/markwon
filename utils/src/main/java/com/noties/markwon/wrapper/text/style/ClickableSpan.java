package com.noties.markwon.wrapper.text.style;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.TextPaint;

import ohos.agp.components.Component;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public abstract class ClickableSpan {

    public abstract void onClick(Component widget);

    public void updateDrawState(@NonNull TextPaint ds) {
    }
}
