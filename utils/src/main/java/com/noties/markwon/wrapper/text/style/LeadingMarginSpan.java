package com.noties.markwon.wrapper.text.style;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Layout;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public interface LeadingMarginSpan {
    public int getLeadingMargin(boolean first);

    public void drawLeadingMargin(Canvas c, Paint p,
                                  int x, int dir,
                                  int top, int baseline, int bottom,
                                  CharSequence text, int start, int end,
                                  boolean first, Layout layout);
}
