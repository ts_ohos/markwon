package com.noties.markwon.wrapper.graphics.drawable;

import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;

/**
 * @author: yangrui
 * @function: open open harmony lack class
 * @date: 2021/3/23
 */
public class GifDrawable extends Drawable {

    /**
     * Creates drawable from byte array.<br>
     * It can be larger than size of the GIF data. Bytes beyond GIF terminator are not accessed.
     * @param bytes raw GIF bytes
     */
    public GifDrawable(byte[] bytes) {

    }

    @Override
    public void draw(Canvas canvas) {

    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }

    @Override
    public void setState(int[] state) {

    }

    /**
     * Equivalent of
     */

    public void pause() {

    }

}
