package com.noties.markwon.wrapper.text.style;

import ohos.agp.render.Paint;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public interface LineHeightSpan {

    /**
     * Classes that implement this should define how the height is being calculated.
     *
     * @param text       the text
     * @param start      the start of the line
     * @param end        the end of the line
     * @param spanstartv the start of the span
     * @param lineHeight the line height
     * @param fm         font metrics of the paint, in integers
     */
    void chooseHeight(CharSequence text, int start, int end,
                      int spanstartv, int lineHeight,
                      Paint.FontMetrics fm);

}
