package com.noties.markwon.wrapper.text.style;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.TextPaint;
import ohos.agp.components.Component;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class URLSpan extends ClickableSpan{
    private final String mURL;

    /**
     * Constructs a {@link URLSpan} from a url string.
     *
     * @param url the url string
     */
    public URLSpan(String url) {
        mURL = url;
    }

    @Override
    public void onClick(Component widget) {

    }

    @Override
    public void updateDrawState(@NonNull TextPaint ds) {
        // can customize appearance here as spans will be rendered as links
    }

    /**
     * Get the url string for this span.
     *
     * @return the url string.
     */
    public String getUrl() {
        return mURL;
    }
}
