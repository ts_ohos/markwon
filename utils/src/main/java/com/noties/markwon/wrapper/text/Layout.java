package com.noties.markwon.wrapper.text;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class Layout extends ohos.agp.text.Layout {
    public enum Alignment {

        /**
         * enum distinguish type
         */
        ALIGN_NORMAL,
        ALIGN_OPPOSITE,
        ALIGN_CENTER;

        Alignment() {
        }
    }

    public int getLineForOffset(int offset) {
        int high = getLineCount(), low = -1, guess;

        while (high - low > 1) {
            guess = (high + low) / 2;

            if (getLineStart(guess) > offset) {
                high = guess;
            } else {
                low = guess;
            }
        }

        return Math.max(low, 0);
    }

    /**
     * Return the vertical position of the bottom of the specified line.
     */
    public final int getLineBottom(int line) {
        return 0;
    }

    public float getPrimaryHorizontal(int offset) {
        return getPrimaryHorizontal(offset, false);
    }

    private float getPrimaryHorizontal(int offset, boolean clamped) {
        return 1f;
    }


}
