package com.noties.markwon.wrapper.text;

import ohos.agp.components.Text;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class PrecomputedTextCompat implements Spannable {

    public static PrecomputedTextCompat create(Spanned spanned, Params params) {
        return null;
    }

    /**
     * The information required for building {@link PrecomputedTextCompat}.
     * <p>
     * Contains information required for precomputing text measurement metadata, so it can be done
     * in isolation of a {@link Text} or {@link StaticLayout}, when final layout
     * constraints are not known.
     */
    public static final class Params {

        public Params(Text textView) {

        }

        /**
         * A builder for creating {@link Params}.
         */
        public static class Builder {
            /**
             * Builder constructor.
             *
             * @param textView text
             */
            public Builder(Text textView) {

            }

            public Builder setBreakStrategy(Text textView) {
                return this;
            }

            public Builder setHyphenationFrequency(Text textView) {
                return this;
            }

            public Params build() {
                return null;
            }
        }
    }

    @Override
    public void setSpan(Object what, int start, int end, int flags) {

    }

    @Override
    public void removeSpan(Object what) {

    }

    @Override
    public <T> T[] getSpans(int start, int end, Class<T> type) {
        return null;
    }

    @Override
    public int getSpanStart(Object tag) {
        return 0;
    }

    @Override
    public int getSpanEnd(Object tag) {
        return 0;
    }

    @Override
    public int getSpanFlags(Object tag) {
        return 0;
    }

    @Override
    public int nextSpanTransition(int start, int limit, Class type) {
        return 0;
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int i) {
        return 0;
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return null;
    }
}
