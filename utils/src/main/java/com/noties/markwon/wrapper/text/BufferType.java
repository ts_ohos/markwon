package com.noties.markwon.wrapper.text;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public enum BufferType {
    /**
     * enum distinguish type
     */
    NORMAL, SPANNABLE, EDITABLE
}
