package com.noties.markwon.wrapper.view;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class RecyclerView extends ListContainer {
    public RecyclerView(Context context) {
        super(context);
    }

    public RecyclerView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public RecyclerView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    public ViewHolder findContainingViewHolder(Component child) {
        return null;
    }

    public abstract static class Adapter<VH extends ViewHolder> {

        protected void setHasStableIds(boolean hasStableIds) {
        }

        public ViewHolder onCreateViewHolder(@NonNull ComponentContainer parent, int viewType) {
            return null;
        }

        public void onBindViewHolder(@NonNull VH holder, int position) {
        }

        public void onViewRecycled(@NonNull VH holder) {
        }

        public int getItemCount() {
            return 0;
        }

        public int getItemViewType(int position) {
            return 0;
        }

        public long getItemId(int position) {
            return 0;
        }
    }

    public abstract static class ViewHolder {
        @NonNull
        public final Component itemView;
        private int adapterPosition;


        protected ViewHolder(Component itemView) {
            this.itemView = itemView;
        }

        public int getItemViewType() {
            return 0;
        }

        public final void setIsRecyclable(boolean recyclable) {
        }

        public int getAdapterPosition() {
            return adapterPosition;
        }
    }

    /**
     * Set a new adapter to provide child views on demand.
     * <p>
     * When adapter is changed, all existing views are recycled back to the pool. If the pool has
     * only one adapter, it will be cleared.
     *
     * @param adapter The new adapter to set, or null to set no adapter.
     * @see #(Adapter, boolean)
     */
    public void setAdapter(@Nullable Adapter adapter) {
        // bail out if layout is frozen
       /* setLayoutFrozen(false);
        setAdapterInternal(adapter, false, true);
        processDataSetCompletelyChanged(false);
        requestLayout();*/
    }

}
