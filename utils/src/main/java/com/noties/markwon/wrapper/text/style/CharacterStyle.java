package com.noties.markwon.wrapper.text.style;

import com.noties.markwon.wrapper.text.TextPaint;

/**
 * The classes that affect character-level text formatting extend this
 * class.  Most extend its subclass {@link MetricAffectingSpan}, but simple
 * ones may just implement {@link UpdateAppearance}.
 */
public abstract class CharacterStyle{

    public void updateDrawState(TextPaint tp) {
    }
}
