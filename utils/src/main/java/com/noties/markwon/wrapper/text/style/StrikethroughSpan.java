package com.noties.markwon.wrapper.text.style;

import ohos.agp.text.TextForm;

/**
 * A span that strikes through the text it's attached to.
 * <p>
 * The span can be used like this:
 * <pre>{@code
 * SpannableString string = new SpannableString("Text with strikethrough span");
 *string.setSpan(new StrikethroughSpan(), 10, 23, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);}</pre>
 * <img src="{@docRoot}reference/android/images/text/style/strikethroughspan.png" />
 * <figcaption>Strikethrough text.</figcaption>
 */
public class StrikethroughSpan extends TextForm {

    /**
     * set StrikethroughSpan
     * @author: yr
     */
    public StrikethroughSpan() {
        setStrikethrough(true);
    }
}
