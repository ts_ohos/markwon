package com.noties.markwon.wrapper.graphics;

/**
 * @author: yangrui
 * @function: open open harmony lack class
 * @date: 2021/3/23
 */
public class Typeface {
    public static final int NORMAL = 0;
    public static final int BOLD = 1;
    public static final int ITALIC = 2;
    public static final int BOLD_ITALIC = 3;
}
