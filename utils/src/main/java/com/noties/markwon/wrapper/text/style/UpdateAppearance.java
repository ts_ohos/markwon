package com.noties.markwon.wrapper.text.style;

import com.noties.markwon.wrapper.text.TextPaint;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public interface UpdateAppearance {
    void updateDrawState(TextPaint tp);
}
