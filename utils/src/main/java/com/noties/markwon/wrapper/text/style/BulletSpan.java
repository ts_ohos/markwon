package com.noties.markwon.wrapper.text.style;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class BulletSpan {
    public BulletSpan() {
    }

    /**
     * Creates a {@link BulletSpan} based on a gap width
     *
     * @param gapWidth the distance, in pixels, between the bullet point and the paragraph.
     */
    public BulletSpan(int gapWidth) {
    }
}
