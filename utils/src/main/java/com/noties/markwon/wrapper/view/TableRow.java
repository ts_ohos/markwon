package com.noties.markwon.wrapper.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class TableRow extends DirectionalLayout {
    public TableRow(Context context) {
        super(context);
    }

    public TableRow(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public TableRow(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
