package com.noties.markwon.wrapper.jlatexmath;

import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.annotation.Px;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;

/**
 * @author: yangrui
 * @function: lack lib keep original name
 * @date: 2021/3/23
 */
public class JLatexMathDrawable extends Drawable {
    private JLatexMathDrawable(Builder builder) {
        super();
    }

    @NonNull
    public static Builder builder(@NonNull String latex) {
        return new Builder(latex);
    }

    @Override
    public void draw(Canvas canvas) {

    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }

    @Override
    public void setState(int[] state) {

    }

    public static class Builder {

        private final String latex;

        private float textSize;
        private int color = 0xFF000000;
        private int align;
        private Drawable background;

        Builder(@NonNull String latex) {
            this.latex = latex;
        }

        @NonNull
        public Builder textSize(@Px float textSize) {
            this.textSize = textSize;
            return this;
        }

        @NonNull
        public Builder color(@ColorInt int color) {
            this.color = color;
            return this;
        }

        @NonNull
        public Builder background(@Nullable Drawable background) {
            this.background = background;
            return this;
        }

        @NonNull
        public Builder background(@ColorInt int backgroundColor) {
            return this;
        }
        
        /**
         * @since 0.2.0 no longer in use
         */
        @NonNull
        @Deprecated
        public Builder fitCanvas(boolean fitCanvas) {
            return this;
        }

        @NonNull
        public JLatexMathDrawable build() {
            return new JLatexMathDrawable(this);
        }

        public Builder align(int blockHorizontalAlignment) {
            return this;
        }

        public void padding(int left, int top, int right, int bottom) {
        }
    }
}
