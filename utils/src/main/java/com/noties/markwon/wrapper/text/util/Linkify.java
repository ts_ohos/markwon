package com.noties.markwon.wrapper.text.util;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.text.Spannable;
import com.noties.markwon.wrapper.text.style.URLSpan;

import java.util.function.Function;
import ohos.app.Context;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class Linkify {
    private static final String LOG_TAG = "Linkify";

    /**
     *  Bit field indicating that web URLs should be matched in methods that
     *  take an options mask
     */
    public static final int WEB_URLS = 0x01;

    /**
     *  Bit field indicating that email addresses should be matched in methods
     *  that take an options mask
     */
    public static final int EMAIL_ADDRESSES = 0x02;

    /**
     *  Bit field indicating that phone numbers should be matched in methods that
     *  take an options mask
     */
    public static final int PHONE_NUMBERS = 0x04;

    @Deprecated
    public static final int MAP_ADDRESSES = 0x08;

    public static final int ALL = WEB_URLS | EMAIL_ADDRESSES | PHONE_NUMBERS | MAP_ADDRESSES;

    /**
     *  Scans the text of the provided Spannable and turns all occurrences
     *  of the link types indicated in the mask into clickable links.
     *  If the mask is nonzero, it also removes any existing URLSpans
     *  attached to the Spannable, to avoid problems if you call it
     *  repeatedly on the same text.
     *
     *  @param text Spannable whose text is to be marked-up with links
     *  @param mask Mask to define which kinds of links will be searched.
     *
     *  @return True if at least one link is found and applied.
     *
     * @see #addLinks(Spannable, int, Function)
     */
    public static final boolean addLinks(@NonNull Spannable text, int mask) {
        return addLinks(text, mask, null, null);
    }

    /**
     *  Scans the text of the provided Spannable and turns all occurrences
     *  of the link types indicated in the mask into clickable links.
     *  If the mask is nonzero, it also removes any existing URLSpans
     *  attached to the Spannable, to avoid problems if you call it
     *  repeatedly on the same text.
     *
     *  @param text Spannable whose text is to be marked-up with links
     *  @param mask mask to define which kinds of links will be searched
     *  @param urlSpanFactory function used to create {@link URLSpan}s
     *  @return True if at least one link is found and applied.
     */
    public static final boolean addLinks(@NonNull Spannable text, int mask,
                                         @Nullable Function<String, URLSpan> urlSpanFactory) {
        return addLinks(text, mask, null, urlSpanFactory);
    }

    private static boolean addLinks(@NonNull Spannable text, int mask,
                                    @Nullable Context context, @Nullable Function<String, URLSpan> urlSpanFactory) {
        return false;
    }
}
