package com.noties.markwon.wrapper.text;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public interface Spannable extends Spanned {

    public void setSpan(Object what, int start, int end, int flags);


    public void removeSpan(Object what);

    default void removeSpan(Object what, int flags) {
        removeSpan(what);
    }

    public static class Factory {
        private static final Spannable.Factory sInstance = new Spannable.Factory();

        /**
         * Returns the standard Spannable Factory.
         */
        public static Spannable.Factory getInstance() {
            return sInstance;
        }

        /**
         * Returns a new SpannableString from the specified CharSequence.
         * You can override this to provide a different kind of Spannable.
         */
        public Spannable newSpannable(CharSequence source) {
            // TODO
//            return new SpannableString(source);
            return null;
        }
    }
}
