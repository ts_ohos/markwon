package com.noties.markwon.wrapper.text;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
abstract class SpannableStringInternal {


    /* package */ SpannableStringInternal(CharSequence source,
                                          int end, boolean ignoreNoCopySpan) {
    }
}
