package com.noties.markwon.wrapper.text;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class SpannableString extends SpannableStringInternal implements Spannable {

    /**
     * For the backward compatibility reasons, this constructor copies all spans including {@link
     * @param source source text
     */
    public SpannableString(CharSequence source) {
        this(source, false );
        // preserve existing NoCopySpan behavior
    }

    /**
     * @param source source object to copy from
     * @param ignoreNoCopySpan whether to copy NoCopySpans in the {@code source}
     * @hide
     */
    private SpannableString(CharSequence source, boolean ignoreNoCopySpan) {
        super(source, source.length(), ignoreNoCopySpan);
    }


    @Override
    public void setSpan(Object what, int start, int end, int flags) {

    }

    @Override
    public void removeSpan(Object what) {

    }

    @Override
    public <T> T[] getSpans(int start, int end, Class<T> type) {
        return null;
    }

    @Override
    public int getSpanStart(Object tag) {
        return 0;
    }

    @Override
    public int getSpanEnd(Object tag) {
        return 0;
    }

    @Override
    public int getSpanFlags(Object tag) {
        return 0;
    }

    @Override
    public int nextSpanTransition(int start, int limit, Class type) {
        return 0;
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int i) {
        return 0;
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return null;
    }
}
