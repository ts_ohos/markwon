package com.noties.markwon.wrapper.text.style;

import com.noties.markwon.annotation.IntRange;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public abstract class ReplacementSpan {
    /**
     * Returns the width of the span. Extending classes can set the height of the span by updating
     * attributes of {@link Paint.FontMetrics}. If the span covers the whole
     * text, and the height is not set,
     * {@link #draw(Canvas, CharSequence, int, int, float, int, int, int, Paint)} will not be
     * called for the span.
     *
     * @param paint Paint instance.
     * @param text Current text.
     * @param start Start character index for span.
     * @param end End character index for span.
     * @param fm Font metrics, can be null.
     * @return Width of the span.
     */
    public abstract int getSize(@NonNull Paint paint, CharSequence text,
                                @IntRange(from = 0) int start, @IntRange(from = 0) int end,
                                @Nullable Paint.FontMetrics fm);

    /**
     * Draws the span into the canvas.
     *
     * @param canvas Canvas into which the span should be rendered.
     * @param text Current text.
     * @param start Start character index for span.
     * @param end End character index for span.
     * @param x Edge of the replacement closest to the leading margin.
     * @param top Top of the line.
     * @param y Baseline.
     * @param bottom Bottom of the line.
     * @param paint Paint instance.
     */
    public abstract void draw(@NonNull Canvas canvas, CharSequence text,
                              @IntRange(from = 0) int start, @IntRange(from = 0) int end, float x,
                              int top, int y, int bottom, @NonNull Paint paint);

}
