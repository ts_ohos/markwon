package com.noties.markwon.wrapper.text;

import com.noties.markwon.annotation.ColorInt;
import ohos.agp.render.Paint;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class TextPaint extends Paint {
    @ColorInt
    public int bgColor;
    public int baselineShift;

    /**
     * Set the paint's horizontal skew factor for text. The default value
     * is 0. For approximating oblique text, use values around -0.25.
     *
     * @param skewX set the paint's skew factor in X for drawing text.
     */
    public void setTextSkewX(float skewX) {

    }


    public void setUnderlineText(boolean b) {
    }
}
