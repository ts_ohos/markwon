package com.noties.markwon.wrapper.text.method;

import com.noties.markwon.wrapper.text.Spannable;
import com.noties.markwon.wrapper.view.MotionEvent;
import ohos.agp.components.DragEvent;
import ohos.agp.components.Text;
import ohos.multimodalinput.event.KeyEvent;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class ScrollingMovementMethod extends BaseMovementMethod implements MovementMethod{
    public static MovementMethod getInstance() {
        if (sInstance == null) {
            sInstance = new ScrollingMovementMethod();
        }

        return sInstance;
    }

    private static ScrollingMovementMethod sInstance;

    @Override
    public void initialize(Text widget, Spannable text) {

    }

    @Override
    public boolean onKeyDown(Text widget, Spannable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyUp(Text widget, Spannable text, int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onKeyOther(Text view, Spannable text, KeyEvent event) {
        return false;
    }

    @Override
    public void onTakeFocus(Text widget, Spannable text, int direction) {

    }

    @Override
    public boolean onTrackballEvent(Text widget, Spannable text, MotionEvent event) {
        return false;
    }

    @Override
    public boolean onTouchEvent(Text widget, Spannable text, MotionEvent event) {
        return false;
    }

    @Override
    public boolean onTouchEvent(Text widget, Spannable text, DragEvent event) {
        return false;
    }

    @Override
    public boolean onGenericMotionEvent(Text widget, Spannable text, MotionEvent event) {
        return false;
    }

    @Override
    public boolean canSelectArbitrarily() {
        return false;
    }
}
