package com.noties.markwon.wrapper.text;

import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class SpannableStringBuilder extends RichTextBuilder implements CharSequence, Spannable, Editable {

    private char[] mText;
    private int mGapLength;

    public SpannableStringBuilder() {
        this("", 0, 0);
    }

    public SpannableStringBuilder(CharSequence text) {
        this(text, 0, text.length());
    }

    /**
     * Create a new SpannableStringBuilder containing a copy of the
     * specified slice of the specified text, including its spans if any.
     */
    public SpannableStringBuilder(CharSequence text, int start, int end) {
        super();
        int srclen = end - start;

        if (srclen < 0) {
            throw new StringIndexOutOfBoundsException();
        }


        if (text instanceof Spanned) {
            Spanned sp = (Spanned) text;
            Object[] spans = sp.getSpans(start, end, Object.class);

            for (Object span : spans) {
                int st = sp.getSpanStart(span) - start;
                int en = sp.getSpanEnd(span) - start;
                int fl = sp.getSpanFlags(span);

                if (st < 0) {
                    st = 0;
                }
                if (st > end - start) {
                    st = end - start;
                }

                if (en < 0) {
                    en = 0;
                }
                if (en > end - start) {
                    en = end - start;
                }
                setSpan(false, span, st, en, fl, false);
            }


        }
    }

    private void setSpan(boolean send, Object what, int start, int end, int flags,
                         boolean enforceParagraph) {
        // open open open harmony addText must after mergeForm
        // android append must before setSpan

        if (what instanceof TextForm) {
            mergeForm((TextForm) what);
            // addText("text");
            revertForm();
        }
    }


    /**
     * Mark the specified range of text with the specified object.
     * The flags determine how the span will behave when text is
     * inserted at the start or end of the span's range.
     */
    @Override
    public void setSpan(Object what, int start, int end, int flags) {
        setSpan(true, what, start, end, flags, true);
    }

    @Override
    public void removeSpan(Object what) {

    }

    @Override
    public void removeSpan(Object what, int flags) {

    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int i) {
        return 0;
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return null;
    }

    @Override
    public <T> T[] getSpans(int start, int end, Class<T> type) {
        return null;
    }

    @Override
    public int getSpanStart(Object tag) {
        return 0;
    }

    @Override
    public int getSpanEnd(Object tag) {
        return 0;
    }

    @Override
    public int getSpanFlags(Object tag) {
        return 0;
    }

    @Override
    public int nextSpanTransition(int start, int limit, Class type) {
        return 0;
    }

    public SpannableStringBuilder append(CharSequence text) {
        addText(text.toString());
        int length = length();
        return replace(length, length, text, 0, text.length());
    }

    private SpannableStringBuilder replace(final int start, final int end,
                                           CharSequence tb, int tbstart, int tbend) {
        return new SpannableStringBuilder();
    }

    @Override
    public void insert(int pendingNewLineIndex, String pendingNewLineContent) {

    }

    @Override
    public void replace(int clearLineStart, int clearLineEnd, String s) {

    }
}
