package com.noties.markwon.wrapper.text.style;

import com.noties.markwon.annotation.ColorInt;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class ForegroundColorSpan {
    private final int mColor;

    public ForegroundColorSpan(@ColorInt int color) {
        mColor = color;
    }
}
