package com.noties.markwon.wrapper.graphics.drawable;

import com.noties.markwon.utils.Resources;
import ohos.agp.render.Canvas;
import ohos.agp.render.ColorFilter;
import ohos.media.image.PixelMap;

/**
 * @author: yangrui
 * @function: open open harmony lack class
 * @date: 2021/3/23
 */
public class BitmapDrawable extends Drawable {

    public BitmapDrawable() {

    }


    public BitmapDrawable(Resources resources, PixelMap bitmap) {

    }

    @Override
    public void draw(Canvas canvas) {

    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }

    @Override
    public void setState(int[] state) {

    }
}
