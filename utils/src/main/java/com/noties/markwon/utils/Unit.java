package com.noties.markwon.utils;


/**
 * The type with only one value: the `Unit` object. This type corresponds to the `void` type in Java.
 *
 * @author: yangrui
 */
public interface Unit {

    /**
     * callback function
     */
    void action();
}
