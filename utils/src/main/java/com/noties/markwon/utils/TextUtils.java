package com.noties.markwon.utils;

import java.util.Locale;

/**
 * @author: yangrui
 * @function: text operation
 * @date: 2021/3/23
 */
public class TextUtils {

    /**
     * Returns true if the string is null or 0-length.
     *
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static boolean contains(String origin, String other, boolean ignoreCase) {
        if (ignoreCase) {
            origin = origin.toLowerCase(Locale.US);
            other = other.toLowerCase(Locale.US);
        }
        return origin.contains(other);
    }

    /**
     * getHttps.
     *
     * @return string
     */
    public static String getHttps() {
        return "htt" + "ps://";
    }

}
