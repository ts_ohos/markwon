package com.noties.markwon.utils;

import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.utils.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * @author: yangrui
 * @function: MainAbility entry
 * @date: 2021/3/23
 */
public class MainAbility extends Ability {
    @Override
    @Nullable
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
}
