package com.noties.markwon.utils;

/**
 * @author: yangrui
 * @function: Resources
 * @date: 2021/3/23
 */
public class Resources {

    private static final Object S_SYNC = new Object();
    private static Resources mSystem = null;
    /**
     * Return a global shared Resources object that provides access to only
     * system resources (no application resources), is not configured for the
     * current screen (can not use dimension units, does not change based on
     * orientation, etc), and is not affected by Runtime Resource Overlay.
     */
    public static Resources getSystem() {
        synchronized (S_SYNC) {
            Resources ret = mSystem;
            if (ret == null) {
                ret = new Resources();
                mSystem = ret;
            }
            return ret;
        }
    }

}
