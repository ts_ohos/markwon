package com.noties.markwon.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * @author: yangrui
 * @function: replace io.noties.debug lib of sample log
 * @date: 2021/3/23
 */
public class LogUtils {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "markwonlog");

    public static final String HI_LOG_LABEL ="Log";

    public static void i(String tag, String msg) {
        HiLog.info(LABEL, "" + tag + ":" + msg);
    }

    public static void d(String tag, String msg) {
        HiLog.debug(LABEL, "" + tag + ":" + msg);
    }

    public static void e(String tag, String msg) {
        HiLog.error(LABEL, "" + tag + ":" + msg);
    }

    public static void w(String tag, String msg) {
        HiLog.warn(LABEL, "" + tag + ":" + msg);
    }

    public static void i( Object msg) {
        HiLog.info(LABEL, "" + ":" + msg);
    }

    public static void i() {
        HiLog.info(LABEL, "information" );
    }
}
