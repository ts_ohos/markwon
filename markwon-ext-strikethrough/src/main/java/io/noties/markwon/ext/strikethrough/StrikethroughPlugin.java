package io.noties.markwon.ext.strikethrough;

import com.noties.markwon.annotation.NonNull;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.MarkwonVisitor;
import java.util.Collections;
import ohos.agp.text.TextForm;

import org.commonmark.ext.gfm.strikethrough.Strikethrough;
import org.commonmark.ext.gfm.strikethrough.StrikethroughExtension;
import org.commonmark.parser.Parser;

/**
 * Plugin to add strikethrough markdown feature. This plugin will extend commonmark-java.Parser
 * with strikethrough extension, add SpanFactory and register commonmark-java.Strikethrough node
 * visitor
 *
 * @see #create()
 * @since 3.0.0
 */
public class StrikethroughPlugin extends AbstractMarkwonPlugin {

    @NonNull
    public static StrikethroughPlugin create() {
        return new StrikethroughPlugin();
    }

    @Override
    public void configureParser(@NonNull Parser.Builder builder) {
        builder.extensions(Collections.singleton(StrikethroughExtension.create()));
    }

    @Override
    public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {
        builder.setFactory(Strikethrough.class, (configuration, props) -> new TextForm().setStrikethrough(true));
    }

    @Override
    public void configureVisitor(@NonNull MarkwonVisitor.Builder builder) {
        builder.on(Strikethrough.class, (visitor, strikethrough) -> {
            final int length = visitor.length();
            visitor.visitChildren(strikethrough);
            visitor.setSpansForNodeOptional(strikethrough, length);
        });
    }
}
