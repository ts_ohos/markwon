package io.noties.markwon.image.svg;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;
import com.noties.markwon.wrapper.graphics.drawable.PictureDrawable;

import io.noties.markwon.image.MediaDecoder;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;

/**
 * @since 4.2.0
 */
public class SvgPictureMediaDecoder extends MediaDecoder {

    private static final String CONTENT_TYPE = "image/svg+xml";

    @NonNull
    public static SvgPictureMediaDecoder create() {
        return new SvgPictureMediaDecoder();
    }

    @NonNull
    @Override
    public Drawable decode(@Nullable String contentType, @NonNull InputStream inputStream) {

        // TODO YR SVG
      /*  final SVG svg;
        try {
            svg = SVG.getFromInputStream(inputStream);
        } catch (SVGParseException e) {
            throw new IllegalStateException("Exception decoding SVG", e);
        }

        final Picture picture = svg.renderToPicture();*/
        // TODO YR PictureDrawable
        return new PictureDrawable();
    }

    @NonNull
    @Override
    public Collection<String> supportedTypes() {
        return Collections.singleton(CONTENT_TYPE);
    }
}
