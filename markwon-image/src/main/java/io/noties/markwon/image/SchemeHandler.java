package io.noties.markwon.image;

import com.noties.markwon.annotation.NonNull;

import java.util.Collection;
import ohos.utils.net.Uri;

/**
 * @since 3.0.0
 */
public abstract class SchemeHandler {

    /**
     * Changes since 4.0.0:
     * <ul>
     * <li>Returns `non-null` image-item</li>
     * </ul>
     *
     * @see ImageItem#withDecodingNeeded(String, java.io.InputStream)
     */
    @NonNull
    public abstract ImageItem handle(@NonNull String raw, @NonNull Uri uri);

    /**
     * @since 4.0.0
     */
    @NonNull
    public abstract Collection<String> supportedSchemes();
}
