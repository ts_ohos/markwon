package io.noties.markwon.image;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;

import java.io.InputStream;
import java.util.Collection;

/**
 * @since 3.0.0
 */
public abstract class MediaDecoder {

    /**
     * Changes since 4.0.0:
     * <ul>
     * <li>Returns `non-null` drawable</li>
     * <li>Added `contentType` method parameter</li>
     * </ul>
     */
    @NonNull
    public abstract Drawable decode(
            @Nullable String contentType,
            @NonNull InputStream inputStream
    );

    /**
     * @since 4.0.0
     */
    @NonNull
    public abstract Collection<String> supportedTypes();
}
