package io.noties.markwon.image.data;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.utils.TextUtils;
import java.util.Base64;

public abstract class DataUriDecoder {

    @Nullable
    public abstract byte[] decode(@NonNull DataUri dataUri) throws Throwable;

    @NonNull
    public static DataUriDecoder create() {
        return new Impl();
    }



    static class Impl extends DataUriDecoder {

        private static final String CHARSET = "UTF-8";

        @Nullable
        @Override
        public byte[] decode(@NonNull DataUri dataUri) throws Throwable {

            final String data = dataUri.data();

            if (!TextUtils.isEmpty(data)) {
                if (dataUri.base64()) {
                    return Base64.getEncoder().encode(data.getBytes(CHARSET));
                } else {
                    return data.getBytes(CHARSET);
                }
            } else {
                return null;
            }
        }
    }
}
