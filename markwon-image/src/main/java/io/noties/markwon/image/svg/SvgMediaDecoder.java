package io.noties.markwon.image.svg;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.utils.PixelMapUtils;
import com.noties.markwon.utils.Resources;
import com.noties.markwon.wrapper.graphics.drawable.BitmapDrawable;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;

import io.noties.markwon.image.MediaDecoder;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;

/**
 * @since 1.1.0
 */
public class SvgMediaDecoder extends MediaDecoder {

    private static final String CONTENT_TYPE = "image/svg+xml";

    /**
     * @see #create(Resources)
     * @since 4.0.0
     */
    @NonNull
    public static SvgMediaDecoder create() {
        return create(Resources.getSystem());
    }

    @NonNull
    private static SvgMediaDecoder create(@NonNull Resources resources) {
        return new SvgMediaDecoder(resources);
    }

    private final Resources resources;

    @SuppressWarnings("WeakerAccess")
    SvgMediaDecoder(Resources resources) {
        this.resources = resources;

        // @since 4.0.0
        validate();
    }

    @NonNull
    @Override
    public Drawable decode(@Nullable String contentType, @NonNull InputStream inputStream) {


        // TODO YR SVG
       /*
         final SVG svg;
       try {
            svg = SVG.getFromInputStream(inputStream);
        } catch (SVGParseException e) {
            throw new IllegalStateException("Exception decoding SVG", e);
        }

        final float w = svg.getDocumentWidth();
        final float h = svg.getDocumentHeight();
        final float density = resources.getDisplayMetrics().density;

        final int width = (int) (w * density + .5F);
        final int height = (int) (h * density + .5F);

        final PixelMap bitmap = PixelMapUtils.createBitmap(width, height, PixelFormat.RGB_565);
        final Canvas canvas = new Canvas(bitmap);
        canvas.scale(density, density);
        svg.renderToCanvas(canvas);

        */
        // TODO YR BitmapDrawable
        return new BitmapDrawable(resources, PixelMapUtils.decodeStream(inputStream));
    }

    @NonNull
    @Override
    public Collection<String> supportedTypes() {
        return Collections.singleton(CONTENT_TYPE);
    }

    private static void validate() {
        if (!SvgSupport.hasSvgSupport()) {
            throw new IllegalStateException(SvgSupport.missingMessage());
        }
    }
}
