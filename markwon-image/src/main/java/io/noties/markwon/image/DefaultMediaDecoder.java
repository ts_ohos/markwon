package io.noties.markwon.image;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.utils.PixelMapUtils;
import com.noties.markwon.utils.Resources;
import com.noties.markwon.wrapper.graphics.drawable.BitmapDrawable;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;

import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import ohos.media.image.PixelMap;

/**
 * This class can be used as the last {@link MediaDecoder} to _try_ to handle all rest cases.
 * Here we just assume that supplied InputStream is of image type and try to decode it.
 *
 * <strong>NB</strong> if you are dealing with big images that require down scaling see {@link DefaultDownScalingMediaDecoder}
 * which additionally down scales displayed images.
 *
 * @see DefaultDownScalingMediaDecoder
 * @since 1.1.0
 */
public class DefaultMediaDecoder extends MediaDecoder {

    @NonNull
    public static DefaultMediaDecoder create() {
        return new DefaultMediaDecoder(Resources.getSystem());
    }

    @NonNull
    public static DefaultMediaDecoder create(@NonNull Resources resources) {
        return new DefaultMediaDecoder(resources);
    }

    private final Resources resources;

    @SuppressWarnings("WeakerAccess")
    DefaultMediaDecoder(Resources resources) {
        this.resources = resources;
    }

    @NonNull
    @Override
    public Drawable decode(@Nullable String contentType, @NonNull InputStream inputStream) {

        final PixelMap bitmap;
        try {
            // absolutely not optimal... thing
            bitmap = PixelMapUtils.decodeStream(inputStream);
        } catch (Throwable t) {
            throw new IllegalStateException("Exception decoding input-stream", t);
        }
        // TODO YR BitmapDrawable

        return new BitmapDrawable(resources, bitmap);
    }

    @NonNull
    @Override
    public Collection<String> supportedTypes() {
        return Collections.emptySet();
    }
}
