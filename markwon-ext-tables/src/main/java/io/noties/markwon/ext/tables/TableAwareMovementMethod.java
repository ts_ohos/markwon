package io.noties.markwon.ext.tables;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.Spannable;
import com.noties.markwon.wrapper.text.Spanned;
import com.noties.markwon.wrapper.text.method.LinkMovementMethod;
import com.noties.markwon.wrapper.text.method.MovementMethod;
import com.noties.markwon.wrapper.text.style.ClickableSpan;
import com.noties.markwon.wrapper.view.MotionEvent;

import ohos.agp.components.Component;
import ohos.agp.components.DragEvent;
import ohos.agp.components.Text;
import ohos.agp.render.Paint;
import ohos.agp.text.Layout;
import ohos.agp.text.SimpleTextLayout;
import ohos.agp.utils.Rect;
import ohos.multimodalinput.event.KeyEvent;

/**
 * @since 4.6.0
 */
public class TableAwareMovementMethod implements MovementMethod {

    @NonNull
    public static TableAwareMovementMethod wrap(@NonNull MovementMethod movementMethod) {
        return new TableAwareMovementMethod(movementMethod);
    }

    /**
     * Wraps LinkMovementMethod
     */
    @NonNull
    public static TableAwareMovementMethod create() {
        return new TableAwareMovementMethod(LinkMovementMethod.getInstance());
    }

    private static boolean handleTableRowTouchEvent(
        @NonNull Text widget,
        @NonNull Spannable buffer,
        @NonNull DragEvent event) {
        // handle only action up (originally action down is used in order to handle selection,
        //  which tables do no have)
        if (event.getAction() != DragEvent.DRAG_FINISH) {
            return false;
        }
        int x = (int) event.getX();
        int y = (int) event.getY();
        x -= widget.getLeft();
        y -= widget.getTop();
        x += widget.getScrollValue(Component.AXIS_X);
        y += widget.getScrollValue(Component.AXIS_Y);

        final Layout layout = new SimpleTextLayout(widget.getText(), new Paint(), new Rect(widget.getLeft(),
                widget.getTop(), widget.getRight(), widget.getBottom()), widget.getWidth());
        final int line = (int) layout.getCoordinateForVertical(y);
        final int off = layout.getCharIndexByCoordinate(line, x);

        final TableRowSpan[] spans = buffer.getSpans(off, off, TableRowSpan.class);
        if (spans.length == 0) {
            return false;
        }

        final TableRowSpan span = spans[0];

        // okay, we can calculate the x to obtain span, but what about y?
        final Layout rowLayout = span.findLayoutForHorizontalOffset(x);
        if (rowLayout != null) {
            // line top as basis
            final int rowY = layout.getTop(line);
            final int rowLine = (int) rowLayout.getCoordinateForVertical(y - rowY);
            final int rowOffset = rowLayout.getCharIndexByCoordinate(rowLine, x % span.cellWidth());
            final ClickableSpan[] rowClickableSpans = ((Spanned) widget.getText().chars())
                    .getSpans(rowOffset, rowOffset, ClickableSpan.class);
            if (rowClickableSpans.length > 0) {
                rowClickableSpans[0].onClick(widget);
                return true;
            }
        }

        return false;
    }

    private final MovementMethod wrapped;

    private TableAwareMovementMethod(@NonNull MovementMethod wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public void initialize(Text widget, Spannable text) {
        wrapped.initialize(widget, text);
    }

    @Override
    public boolean onKeyDown(Text widget, Spannable text, int keyCode, KeyEvent event) {
        return wrapped.onKeyDown(widget, text, keyCode, event);
    }

    @Override
    public boolean onKeyUp(Text widget, Spannable text, int keyCode, KeyEvent event) {
        return wrapped.onKeyUp(widget, text, keyCode, event);
    }

    @Override
    public boolean onKeyOther(Text view, Spannable text, KeyEvent event) {
        return wrapped.onKeyOther(view, text, event);
    }

    @Override
    public void onTakeFocus(Text widget, Spannable text, int direction) {
        wrapped.onTakeFocus(widget, text, direction);
    }

    @Override
    public boolean onTrackballEvent(Text widget, Spannable text, MotionEvent event) {
        return wrapped.onTrackballEvent(widget, text, event);
    }

    @Override
    public boolean onTouchEvent(Text widget, Spannable text, MotionEvent event) {
        return false;
    }

    @Override
    public boolean onTouchEvent(Text widget, Spannable buffer, DragEvent event) {
        // let wrapped handle first, then if super handles nothing, search for table row spans
        return wrapped.onTouchEvent(widget, buffer, event)
                || handleTableRowTouchEvent(widget, buffer, event);
    }

    @Override
    public boolean onGenericMotionEvent(Text widget, Spannable text, MotionEvent event) {
        return wrapped.onGenericMotionEvent(widget, text, event);
    }

    @Override
    public boolean canSelectArbitrarily() {
        return wrapped.canSelectArbitrarily();
    }
}
