package io.noties.markwon.ext.tables;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.utils.TextUtils;
import com.noties.markwon.wrapper.text.Spanned;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.utils.PacMap;

abstract class TableRowsScheduler {
    private static final PacMap mTag = new PacMap();
    static void schedule(@NonNull final Text view) {
        final Object[] spans = extract(view);
        if (spans != null
                && spans.length > 0) {

            if (view.getTag() == null) {
                final Component.BindStateChangedListener listener = new Component.BindStateChangedListener() {
                    @Override
                    public void onComponentBoundToWindow(Component component) {

                    }

                    @Override
                    public void onComponentUnboundFromWindow(Component component) {
                        unschedule(view);
                        view.removeBindStateChangedListener(this);
                        mTag.putObjectValue("markwon_tables_scheduler", null);
                        view.setTag(mTag);
                    }
                };
                view.setBindStateChangedListener(listener);
                mTag.putObjectValue("markwon_tables_scheduler", listener);
                view.setTag(mTag);
            }

            final TableRowSpan.Invalidator invalidator = new TableRowSpan.Invalidator() {

                // @since 4.1.0
                // let's stack-up invalidation calls (so invalidation happens,
                // but not with each table-row-span draw call)
                final Runnable runnable = () -> view.setText(view.getText());

                @Override
                public void invalidate() {
                    // @since 4.1.0 post invalidation (combine multiple calls)
                    view.getContext().getUITaskDispatcher().syncDispatch(runnable);
                }
            };

            for (Object span : spans) {
                ((TableRowSpan) span).invalidator(invalidator);
            }
        }
    }

    static void unschedule(@NonNull Text view) {
        final Object[] spans = extract(view);
        if (spans != null
                && spans.length > 0) {
            for (Object span : spans) {
                ((TableRowSpan) span).invalidator(null);
            }
        }
    }

    @Nullable
    private static Object[] extract(@NonNull Text view) {
        final Object[] out;
        final CharSequence text = view.getText();
        if (!TextUtils.isEmpty(text) && text instanceof Spanned) {
            out = ((Spanned) text).getSpans(0, text.length(), TableRowSpan.class);
        } else {
            out = null;
        }
        return out;
    }

    private TableRowsScheduler() {
    }
}
