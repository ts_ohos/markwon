package io.noties.markwon.ext.tables;

import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Px;
import io.noties.markwon.utils.ColorUtils;
import io.noties.markwon.utils.Dip;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

@SuppressWarnings("WeakerAccess")
public class TableTheme {

    @NonNull
    public static TableTheme create(@NonNull Context context) {
        return buildWithDefaults(context).build();
    }

    @NonNull
    public static Builder buildWithDefaults(@NonNull Context context) {
        final Dip dip = Dip.create(context);
        return emptyBuilder()
                .tableCellPadding(dip.toPx(4))
                .tableBorderWidth(dip.toPx(1));
    }

    @NonNull
    public static Builder emptyBuilder() {
        return new Builder();
    }


    protected static final int TABLE_BORDER_DEF_ALPHA = 75;

    protected static final int TABLE_ODD_ROW_DEF_ALPHA = 22;

    // by default 0
    protected final int tableCellPadding;

    // by default paint.color * TABLE_BORDER_DEF_ALPHA
    protected final int tableBorderColor;

    protected final int tableBorderWidth;

    // by default paint.color * TABLE_ODD_ROW_DEF_ALPHA
    protected final int tableOddRowBackgroundColor;

    // @since 1.1.1
    // by default no background
    protected final int tableEvenRowBackgroundColor;

    // @since 1.1.1
    // by default no background
    protected final int tableHeaderRowBackgroundColor;

    protected TableTheme(@NonNull Builder builder) {
        this.tableCellPadding = builder.tableCellPadding;
        this.tableBorderColor = builder.tableBorderColor;
        this.tableBorderWidth = builder.tableBorderWidth;
        this.tableOddRowBackgroundColor = builder.tableOddRowBackgroundColor;
        this.tableEvenRowBackgroundColor = builder.tableEvenRowBackgroundColor;
        this.tableHeaderRowBackgroundColor = builder.tableHeaderRowBackgroundColor;
    }

    /**
     * @since 3.0.0
     */
    @NonNull
    public Builder asBuilder() {
        return new Builder()
                .tableCellPadding(tableCellPadding)
                .tableBorderColor(tableBorderColor)
                .tableBorderWidth(tableBorderWidth)
                .tableOddRowBackgroundColor(tableOddRowBackgroundColor)
                .tableEvenRowBackgroundColor(tableEvenRowBackgroundColor)
                .tableHeaderRowBackgroundColor(tableHeaderRowBackgroundColor);
    }

    public int tableCellPadding() {
        return tableCellPadding;
    }

    public int tableBorderWidth(@NonNull Paint paint) {
        final int out;
        if (tableBorderWidth == -1) {
            out = (int) (paint.getStrokeWidth() + .5F);
        } else {
            out = tableBorderWidth;
        }
        return out;
    }

    public void applyTableBorderStyle(@NonNull Paint paint) {

        final int color;
        if (tableBorderColor == 0) {
            color = ColorUtils.applyAlpha(paint.getColor().getValue(), TABLE_BORDER_DEF_ALPHA);
        } else {
            color = tableBorderColor;
        }

        paint.setColor(new Color(color));
        // @since 4.3.1 before it was STROKE... change to FILL as we draw border differently
        paint.setStyle(Paint.Style.FILL_STYLE);
    }

    public void applyTableOddRowStyle(@NonNull Paint paint) {
        final int color;
        if (tableOddRowBackgroundColor == 0) {
            color = ColorUtils.applyAlpha(paint.getColor().getValue(), TABLE_ODD_ROW_DEF_ALPHA);
        } else {
            color = tableOddRowBackgroundColor;
        }
        paint.setColor(new Color(color));
        paint.setStyle(Paint.Style.FILL_STYLE);
    }

    /**
     * @since 1.1.1
     */
    public void applyTableEvenRowStyle(@NonNull Paint paint) {
        // by default to background to even row
        paint.setColor(new Color(tableEvenRowBackgroundColor));
        paint.setStyle(Paint.Style.FILL_STYLE);
    }

    /**
     * @since 1.1.1
     */
    public void applyTableHeaderRowStyle(@NonNull Paint paint) {
        paint.setColor(new Color(tableHeaderRowBackgroundColor));
        paint.setStyle(Paint.Style.FILL_STYLE);
    }

    public static class Builder {

        private int tableCellPadding;
        private int tableBorderColor;
        private int tableBorderWidth = -1;
        private int tableOddRowBackgroundColor;
        private int tableEvenRowBackgroundColor; // @since 1.1.1
        private int tableHeaderRowBackgroundColor; // @since 1.1.1

        @NonNull
        public Builder tableCellPadding(@Px int tableCellPadding) {
            this.tableCellPadding = tableCellPadding;
            return this;
        }

        @NonNull
        public Builder tableBorderColor(@ColorInt int tableBorderColor) {
            this.tableBorderColor = tableBorderColor;
            return this;
        }

        @NonNull
        public Builder tableBorderWidth(@Px int tableBorderWidth) {
            this.tableBorderWidth = tableBorderWidth;
            return this;
        }

        @NonNull
        public Builder tableOddRowBackgroundColor(@ColorInt int tableOddRowBackgroundColor) {
            this.tableOddRowBackgroundColor = tableOddRowBackgroundColor;
            return this;
        }

        @NonNull
        public Builder tableEvenRowBackgroundColor(@ColorInt int tableEvenRowBackgroundColor) {
            this.tableEvenRowBackgroundColor = tableEvenRowBackgroundColor;
            return this;
        }

        @NonNull
        public Builder tableHeaderRowBackgroundColor(@ColorInt int tableHeaderRowBackgroundColor) {
            this.tableHeaderRowBackgroundColor = tableHeaderRowBackgroundColor;
            return this;
        }

        @NonNull
        public TableTheme build() {
            return new TableTheme(this);
        }
    }
}
