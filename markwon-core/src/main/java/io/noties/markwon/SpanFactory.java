package io.noties.markwon;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;

/**
 * @since 3.0.0
 */
public interface SpanFactory {

    @Nullable
    Object getSpans(
            @NonNull MarkwonConfiguration configuration,
            @NonNull RenderProps props);
}
