package io.noties.markwon.syntax;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;

public class SyntaxHighlightNoOp implements SyntaxHighlight {
    @NonNull
    @Override
    public CharSequence highlight(@Nullable String info, @NonNull String code) {
        return code;
    }
}
