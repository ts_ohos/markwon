package io.noties.markwon.syntax;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;

@SuppressWarnings("WeakerAccess")
public interface SyntaxHighlight {

    @NonNull
    CharSequence highlight(@Nullable String info, @NonNull String code);
}
