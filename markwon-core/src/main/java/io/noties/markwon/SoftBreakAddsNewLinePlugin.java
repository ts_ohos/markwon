package io.noties.markwon;


import com.noties.markwon.annotation.NonNull;
import org.commonmark.node.SoftLineBreak;

/**
 * @since 4.3.0
 */
public class SoftBreakAddsNewLinePlugin extends AbstractMarkwonPlugin {

    @NonNull
    public static SoftBreakAddsNewLinePlugin create() {
        return new SoftBreakAddsNewLinePlugin();
    }

    @Override
    public void configureVisitor(@NonNull MarkwonVisitor.Builder builder) {
        builder.on(SoftLineBreak.class, (visitor, softLineBreak) -> visitor.ensureNewLine());
    }
}
