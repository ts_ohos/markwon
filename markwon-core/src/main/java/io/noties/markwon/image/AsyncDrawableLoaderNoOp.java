package io.noties.markwon.image;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;

class AsyncDrawableLoaderNoOp extends AsyncDrawableLoader {
    @Override
    public void load(@NonNull AsyncDrawable drawable) {

    }

    @Override
    public void cancel(@NonNull AsyncDrawable drawable) {

    }

    @Nullable
    @Override
    public Drawable placeholder(@NonNull AsyncDrawable drawable) {
        return null;
    }
}
