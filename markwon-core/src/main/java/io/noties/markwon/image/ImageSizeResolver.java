package io.noties.markwon.image;


import com.noties.markwon.annotation.NonNull;
import ohos.agp.utils.Rect;

/**
 * @see ImageSizeResolverDef
 * @see io.noties.markwon.MarkwonConfiguration.Builder#imageSizeResolver(ImageSizeResolver)
 * @since 1.0.1
 */
public abstract class ImageSizeResolver {

    /**
     * @since 4.0.0
     */
    @NonNull
    public abstract Rect resolveImageSize(@NonNull AsyncDrawable drawable);
}
