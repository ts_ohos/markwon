package io.noties.markwon.image;


import com.noties.markwon.annotation.CheckResult;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;
import ohos.agp.utils.Rect;

/**
 * @since 3.0.1
 */
public abstract class DrawableUtils {

    @NonNull
    @CheckResult
    public static Rect intrinsicBounds(@NonNull Drawable drawable) {
        return new Rect(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    }

    public static void applyIntrinsicBounds(@NonNull Drawable drawable) {
        drawable.setBounds(intrinsicBounds(drawable));
    }

    public static void applyIntrinsicBoundsIfEmpty(@NonNull Drawable drawable) {
        if (drawable.getBounds().isEmpty()) {
            drawable.setBounds(intrinsicBounds(drawable));
        }
    }

    private DrawableUtils() {
    }
}
