package io.noties.markwon;

import com.noties.markwon.annotation.NonNull;
import ohos.agp.components.Component;

/**
 * @see LinkResolverDef
 * @see MarkwonConfiguration.Builder#linkResolver(LinkResolver)
 * @since 4.0.0
 */
public interface LinkResolver {
    void resolve(@NonNull Component view, @NonNull String link);
}
