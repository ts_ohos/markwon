package io.noties.markwon.utils;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.Spanned;
import io.noties.markwon.core.spans.TextLayoutSpan;
import io.noties.markwon.core.spans.TextViewSpan;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.text.Layout;

/**
 * @since 4.4.0
 */
public abstract class SpanUtils {

    public static int width(@NonNull Canvas canvas, @NonNull CharSequence cs) {
        // Layout
        // TextView
        // canvas

        if (cs instanceof Spanned) {
            final Spanned spanned = (Spanned) cs;

            // if we are displayed with layout information -> use it
            final Layout layout = TextLayoutSpan.layoutOf(spanned);
            if (layout != null) {
                return layout.getWidth();
            }

            // if we have TextView -> obtain width from it (exclude padding)
            final Text textView = TextViewSpan.textViewOf(spanned);
            if (textView != null) {
                return textView.getWidth() - textView.getPaddingLeft() - textView.getPaddingRight();
            }
        }

        // else just use canvas width
        return canvas.getLocalClipBounds().getWidth();
    }
}
