package io.noties.markwon.utils;


import com.noties.markwon.annotation.NonNull;
import ohos.app.Context;

public class Dip {

    @NonNull
    public static Dip create(@NonNull Context context) {
        //context.getResources().getDisplayMetrics().density
        return new Dip(0);
    }

    @NonNull
    public static Dip create(float density) {
        return new Dip(density);
    }

    private final float density;

    @SuppressWarnings("WeakerAccess")
    public Dip(float density) {
        this.density = density;
    }

    public int toPx(int dp) {
        return (int) (dp * density + .5F);
    }
}
