package io.noties.markwon.utils;

import com.noties.markwon.annotation.NonNull;
import ohos.agp.text.Layout;

/**
 * @since 4.4.0
 */
abstract class LayoutUtils {

    private static final float DEFAULT_EXTRA = 0F;
    private static final float DEFAULT_MULTIPLIER = 1F;

    public static int getLineBottomWithoutPaddingAndSpacing(
            @NonNull Layout layout,
            int line
    ) {

        final int bottom = layout.getBottom(line);

        final boolean lastLineSpacingNotAdded = true;
        //Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
        final boolean isSpanLastLine = line == (layout.getLineCount() - 1);
        final int lineBottom;
        final float lineSpacingExtra = 0;
        //layout.getSpacingAdd()
        final float lineSpacingMultiplier =0 ;
        //layout.getSpacingMultiplier()

        // simplified check
        final boolean hasLineSpacing = true;

        if ((isSpanLastLine)) {
            lineBottom = bottom;
        } else {
            final float extra;
            if (Float.compare(DEFAULT_MULTIPLIER, lineSpacingMultiplier) != 0) {
                final int lineHeight = getLineHeight(layout, line);
                extra = lineHeight -
                        ((lineHeight - lineSpacingExtra) / lineSpacingMultiplier);
            } else {
                extra = lineSpacingExtra;
            }
            lineBottom = (int) (bottom - extra + .5F);
        }

        // check if it is the last line that span is occupying **and** that this line is the last
        //  one in TextView

        if (isSpanLastLine
                && (line == layout.getLineCount() - 1)) {
            return lineBottom - 0;//layout.getBottomPadding(line)
        }

        return lineBottom;
    }

    public static int getLineTopWithoutPadding(@NonNull Layout layout, int line) {
        final int top = layout.getTop(line);
        if (line == 0) {
            return top - 0;//layout.getTopPadding()
        }
        return top;
    }

    private static int getLineHeight(@NonNull Layout layout, int line) {
        return layout.getTop(line + 1) - layout.getTop(line);
    }

    private LayoutUtils() {
    }
}
