package io.noties.markwon.utils;


import com.noties.markwon.annotation.ColorInt;
import com.noties.markwon.annotation.FloatRange;
import com.noties.markwon.annotation.IntRange;
import ohos.agp.colors.RgbColor;
import ohos.agp.utils.Color;

public abstract class ColorUtils {

    @ColorInt
    public static int applyAlpha(
            @ColorInt int color,
            @IntRange(from = 0, to = 255) int alpha) {
        return (color & 0x00FFFFFF) | (alpha << 24);
    }

    // blend two colors w/ specified ratio, resulting color won't have alpha channel
    @ColorInt
    public static int blend(
            @ColorInt int foreground,
            @ColorInt int background,
            @FloatRange(from = 0.0F, to = 1.0F) float ratio) {
        RgbColor foregroundColor = new RgbColor(foreground);
        RgbColor backgroundColor = new RgbColor(background);
        return Color.rgb(
                (int) (((1F - ratio) *foregroundColor.getRed()) + (ratio * backgroundColor.getRed())),
                (int) (((1F - ratio) *foregroundColor.getGreen()) + (ratio * backgroundColor.getGreen())),
                (int) (((1F - ratio) *foregroundColor.getBlue()) + (ratio * backgroundColor.getBlue()))
        );
    }

    private ColorUtils() {
    }
}
