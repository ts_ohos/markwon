package io.noties.markwon.utils;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;

/**
 * @deprecated Please use {@link io.noties.markwon.image}
 */
@Deprecated
public abstract class DrawableUtils {
    public static void intrinsicBounds(@NonNull Drawable drawable) {//@NonNull Drawable drawable
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    }

    private DrawableUtils() {
    }

}
