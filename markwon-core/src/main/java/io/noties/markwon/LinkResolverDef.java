package io.noties.markwon;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.utils.TextUtils;

import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.utils.net.Uri;

public class LinkResolverDef implements LinkResolver {
    private static final String TAG = "LinkResolverDef";
    // @since 4.3.0
    private static final String DEFAULT_SCHEME = "https";
    private static final String EXTRA_APPLICATION_ID = "com.android.browser.application_id";
    @Override
    public void resolve(@NonNull Component view, @NonNull String link) {
        final Uri uri = parseLink(link);
        final Context context = view.getContext();
        // final Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        // intent.putExtra(Browser.EXTRA_APPLICATION_ID, context.getPackageName());
        // TODO YR Browser.EXTRA_APPLICATION_ID

    }

    /**
     * @since 4.3.0
     */
    @NonNull
    private static Uri parseLink(@NonNull String link) {
        final Uri uri = Uri.parse(link);
        String scheme = uri.getScheme();
        if (TextUtils.isEmpty(scheme)) {
            return Uri.parse(link.replace(scheme,DEFAULT_SCHEME));
        }
        return uri;
    }
}
