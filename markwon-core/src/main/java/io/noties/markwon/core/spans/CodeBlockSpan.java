package io.noties.markwon.core.spans;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.TextPaint;
import com.noties.markwon.wrapper.text.style.LeadingMarginSpan;
import com.noties.markwon.wrapper.text.style.MetricAffectingSpan;
import io.noties.markwon.core.MarkwonTheme;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Layout;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

/**
 * @since 3.0.0 split inline and block spans
 */
public class CodeBlockSpan extends MetricAffectingSpan implements LeadingMarginSpan {

    private final MarkwonTheme theme;
    private final Rect rect = ObjectsPool.rect();
    private final Paint paint = ObjectsPool.paint();

    public CodeBlockSpan(@NonNull MarkwonTheme theme) {
        this.theme = theme;
    }

    @Override
    public void updateMeasureState(TextPaint p) {
        apply(p);
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        apply(ds);
    }

    private void apply(TextPaint p) {
        theme.applyCodeBlockTextStyle(p);
    }

    @Override
    public int getLeadingMargin(boolean first) {
        return theme.getCodeBlockMargin();
    }

    @Override
    public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom, CharSequence text, int start, int end, boolean first, Layout layout) {

        paint.setStyle(Paint.Style.FILL_STYLE);
        int codeBlockBackgroundColor = theme.getCodeBlockBackgroundColor(p);
        paint.setColor(new Color(codeBlockBackgroundColor));

        final int left;
        final int right;
        Rect localClipBounds = c.getLocalClipBounds();
        int cWidth = localClipBounds.right - localClipBounds.left;

        if (dir > 0) {
            left = x;
            right = cWidth;
        } else {
            left = x - cWidth;
            right = x;
        }

        rect.set(left, top, right, bottom);
        c.drawRect(new RectFloat(rect), paint);
    }
}
