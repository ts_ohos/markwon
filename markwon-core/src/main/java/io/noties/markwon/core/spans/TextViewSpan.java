package io.noties.markwon.core.spans;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.text.Spannable;
import com.noties.markwon.wrapper.text.Spanned;

import java.lang.ref.WeakReference;

import ohos.agp.components.Text;

/**
 * A special span that allows to obtain {@code TextView} in which spans are displayed
 *
 * @since 4.4.0
 */
public class TextViewSpan {

    @Nullable
    public static Text textViewOf(@NonNull CharSequence cs) {
        if (cs instanceof Spanned) {
            return textViewOf((Spanned) cs);
        }
        return null;
    }

    @Nullable
    public static Text textViewOf(@NonNull Spanned spanned) {
        final TextViewSpan[] spans = spanned.getSpans(0, spanned.length(), TextViewSpan.class);
        return spans != null && spans.length > 0
                ? spans[0].textView()
                : null;
    }

    public static void applyTo(@NonNull Spannable spannable, @NonNull Text textView) {

        final TextViewSpan[] spans = spannable.getSpans(0, spannable.length(), TextViewSpan.class);
        if (spans != null) {
            for (TextViewSpan span : spans) {
                spannable.removeSpan(span);
            }
        }

        final TextViewSpan span = new TextViewSpan(textView);
        // `SPAN_INCLUSIVE_INCLUSIVE` to persist in case of possible text change (deletion, etc)
        spannable.setSpan(
                span,
                0,
                spannable.length(),
                Spanned.SPAN_INCLUSIVE_INCLUSIVE
        );
    }

    private final WeakReference<Text> reference;

    private TextViewSpan(@NonNull Text textView) {
        this.reference = new WeakReference<>(textView);
    }

    @Nullable
    private Text textView() {
        return reference.get();
    }
}
