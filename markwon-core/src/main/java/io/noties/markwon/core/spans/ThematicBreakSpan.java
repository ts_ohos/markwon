package io.noties.markwon.core.spans;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.style.LeadingMarginSpan;
import io.noties.markwon.core.MarkwonTheme;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Layout;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

public class ThematicBreakSpan implements LeadingMarginSpan {

    private final MarkwonTheme theme;
    private final Rect rect = ObjectsPool.rect();
    private final Paint paint = ObjectsPool.paint();

    public ThematicBreakSpan(@NonNull MarkwonTheme theme) {
        this.theme = theme;
    }

    @Override
    public int getLeadingMargin(boolean first) {
        return 0;
    }

    @Override
    public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom, CharSequence text, int start, int end, boolean first, Layout layout) {

        final int middle = top + ((bottom - top) / 2);

        paint.set(p);
        theme.applyThematicBreakStyle(paint);

        final int height = (int) (paint.getStrokeWidth() + .5F);
        final int halfHeight = (int) (height / 2.F + .5F);

        final int left;
        final int right;
        Rect localClipBounds = c.getLocalClipBounds();
        int cWidth = localClipBounds.right - localClipBounds.left;

        if (dir > 0) {
            left = x;
            right = cWidth;
        } else {
            left = x - cWidth;
            right = x;
        }

        rect.set(left, middle - halfHeight, right, middle + halfHeight);
        c.drawRect(new RectFloat(rect), paint);
    }
}
