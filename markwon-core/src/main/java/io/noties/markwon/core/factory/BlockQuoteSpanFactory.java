package io.noties.markwon.core.factory;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.RenderProps;
import io.noties.markwon.SpanFactory;
import io.noties.markwon.core.spans.BlockQuoteSpan;

public class BlockQuoteSpanFactory implements SpanFactory {
    @Nullable
    @Override
    public Object getSpans(@NonNull MarkwonConfiguration configuration, @NonNull RenderProps props) {
        return new BlockQuoteSpan(configuration.theme());
    }
}
