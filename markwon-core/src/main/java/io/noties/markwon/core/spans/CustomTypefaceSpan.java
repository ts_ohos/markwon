package io.noties.markwon.core.spans;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.TextPaint;
import com.noties.markwon.wrapper.text.style.MetricAffectingSpan;
import ohos.agp.text.Font;

/**
 * A span implementation that allow applying custom Font. Although it is
 * not used directly by the library, it\'s helpful for customizations.
 * <p>
 * Please note that this implementation does not validate current paint state
 * and won\'t be updating/modifying supplied Font unless {@code mergeStyles} is specified
 *
 * @since 3.0.0
 */
public class CustomTypefaceSpan extends MetricAffectingSpan {

    @NonNull
    public static CustomTypefaceSpan create(@NonNull Font typeface) {
        return create(typeface, false);
    }

    /**
     * <strong>NB!</strong> in order to <em>merge</em> typeface styles, supplied typeface must be
     * able to be created via {@code Font.create(Font, int)} method. This would mean that bundled fonts
     * inside {@code assets} folder would be able to display styles properly.
     *
     * @param mergeStyles control if typeface styles must be merged, for example, if
     *                    this span (bold) is contained by other span (italic),
     *                    {@code mergeStyles=true} would result in bold-italic
     * @since 4.6.1
     */
    @NonNull
    private static CustomTypefaceSpan create(@NonNull Font typeface, boolean mergeStyles) {
        return new CustomTypefaceSpan(typeface, mergeStyles);
    }

    private final Font typeface;

    private final boolean mergeStyles;

    /**
     * @deprecated 4.6.1 use {{@link #create(Font)}}
     * or {@link #create(Font, boolean)} factory method
     */
    @Deprecated
    public CustomTypefaceSpan(@NonNull Font typeface) {
        this(typeface, false);
    }

    // @since 4.6.1
    private CustomTypefaceSpan(@NonNull Font typeface, boolean mergeStyles) {
        this.typeface = typeface;
        this.mergeStyles = mergeStyles;
    }


    @Override
    public void updateMeasureState(@NonNull TextPaint paint) {
        updatePaint(paint);
    }

    @Override
    public void updateDrawState(@NonNull TextPaint paint) {
        updatePaint(paint);
    }

    private void updatePaint(@NonNull TextPaint paint) {
        final Font oldTypeface = paint.getFont();
        if (!mergeStyles ||
                oldTypeface == null ||
                oldTypeface == Font.DEFAULT) {
            paint.setFont(typeface);
        } else {
            //0|0=0；  0|1=1；  1|0=1；   1|1=1；
            Font want;
            want=oldTypeface;
            if (typeface != Font.DEFAULT) {
                want=typeface;
            }
            paint.setFont(want);
        }
    }
}
