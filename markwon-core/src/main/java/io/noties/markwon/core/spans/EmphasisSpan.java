package io.noties.markwon.core.spans;


import com.noties.markwon.wrapper.text.TextPaint;
import com.noties.markwon.wrapper.text.style.MetricAffectingSpan;

public class EmphasisSpan extends MetricAffectingSpan {

    @Override
    public void updateMeasureState(TextPaint p) {
        p.setTextSkewX(-0.25F);
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setTextSkewX(-0.25F);
    }
}
