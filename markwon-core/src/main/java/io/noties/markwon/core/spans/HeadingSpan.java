package io.noties.markwon.core.spans;


import com.noties.markwon.annotation.IntRange;
import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.TextPaint;
import com.noties.markwon.wrapper.text.style.LeadingMarginSpan;
import com.noties.markwon.wrapper.text.style.MetricAffectingSpan;
import io.noties.markwon.core.MarkwonTheme;
import io.noties.markwon.utils.LeadingMarginUtils;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Layout;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

public class HeadingSpan extends MetricAffectingSpan implements LeadingMarginSpan {

    private final MarkwonTheme theme;
    private final Rect rect = ObjectsPool.rect();
    private final Paint paint = ObjectsPool.paint();
    private final int level;

    public HeadingSpan(@NonNull MarkwonTheme theme, @IntRange(from = 1, to = 6) int level) {
        this.theme = theme;
        this.level = level;
    }

    @Override
    public void updateMeasureState(TextPaint p) {
        apply(p);
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        apply(tp);
    }

    private void apply(TextPaint paint) {
        theme.applyHeadingTextStyle(paint, level);
    }

    @Override
    public int getLeadingMargin(boolean first) {
        // no margin actually, but we need to access Canvas to draw break
        return 0;
    }

    @Override
    public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline,
                                  int bottom, CharSequence text, int start, int end, boolean first, Layout layout) {

        if ((level == 1 || level == 2)
                && LeadingMarginUtils.selfEnd(end, text, this)) {

            paint.set(p);

            theme.applyHeadingBreakStyle(paint);

            final float height = paint.getStrokeWidth();

            if (height > .0F) {

                final int b = (int) (bottom - height + .5F);

                final int left;
                final int right;
                Rect localClipBounds = c.getLocalClipBounds();
                int cWidth = localClipBounds.right - localClipBounds.left;
                if (dir > 0) {
                    left = x;
                    right = cWidth;
                } else {
                    left = x - cWidth;
                    right = x;
                }

                rect.set(left, b, right, bottom);
                c.drawRect(new RectFloat(rect), paint);
            }
        }
    }

    /**
     * @since 4.2.0
     */
    public int getLevel() {
        return level;
    }
}
