package io.noties.markwon.core.spans;


import com.noties.markwon.wrapper.text.TextPaint;
import com.noties.markwon.wrapper.text.style.MetricAffectingSpan;

public class StrongEmphasisSpan extends MetricAffectingSpan {

    @Override
    public void updateMeasureState(TextPaint p) {
        p.setFakeBoldText(true);
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        tp.setFakeBoldText(true);
    }
}
