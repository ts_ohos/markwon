package io.noties.markwon.core.spans;



import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.TextPaint;
import com.noties.markwon.wrapper.text.style.URLSpan;
import io.noties.markwon.LinkResolver;
import io.noties.markwon.core.MarkwonTheme;
import ohos.agp.components.Component;

public class LinkSpan extends URLSpan {

    private final MarkwonTheme theme;
    private final String link;
    private final LinkResolver resolver;

    public LinkSpan(
            @NonNull MarkwonTheme theme,
            @NonNull String link,
            @NonNull LinkResolver resolver) {
        super(link);
        this.theme = theme;
        this.link = link;
        this.resolver = resolver;
    }

    @Override
    public void onClick(Component widget) {
        resolver.resolve(widget, link);
    }

    @Override
    public void updateDrawState(@NonNull TextPaint ds) {
        theme.applyLinkStyle(ds);
    }

    /**
     * @since 4.2.0
     */
    @NonNull
    public String getLink() {
        return link;
    }
}
