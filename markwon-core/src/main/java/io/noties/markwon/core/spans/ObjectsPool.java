package io.noties.markwon.core.spans;


import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

abstract class ObjectsPool {

    // maybe it's premature optimization, but as all the drawing is done in one thread
    // and we apply needed values before actual drawing it's (I assume) safe to reuse some frequently used objects

    // if one of the spans need some really specific handling for Paint object (like colorFilters, masks, etc)
    // it should instantiate own instance of it

    private static final Rect RECT = new Rect();
    private static final RectFloat RECT_F = new RectFloat();
    // set AntiAlias int method paint
    private static final Paint PAINT = new Paint();

    static Rect rect() {
        return RECT;
    }

    static RectFloat rectF() {
        return RECT_F;
    }

    static Paint paint() {
        PAINT.setAntiAlias(true);
        return PAINT;
    }

    private ObjectsPool() {
    }
}
