package io.noties.markwon.app.sample.ui.adapt;

import com.noties.markwon.utils.TextUtils;
import com.noties.markwon.wrapper.text.Spanned;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.Sample;
import io.noties.markwon.app.sample.ui.SampleListFragment;
import io.noties.markwon.app.utils.SampleUtilsKt;
import io.noties.markwon.app.widget.FlowLayout;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import java.util.ArrayList;
import java.util.List;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * @author: yangrui
 * @function: item
 * @date: 2021/3/23
 */
public class SampleItem {

    private final Markwon markwon;
    private final Sample sample;
    public final SampleListFragment.SampleItemInterface sampleItemInterface;

    public Sample getSample() {
        return sample;
    }

    public SampleItem(Markwon markwon, Sample sample, SampleListFragment.SampleItemInterface sampleItemInterface) {
        this.markwon = markwon;
        this.sample = sample;
        this.sampleItemInterface = sampleItemInterface;
    }

    private Spanned text;
    //markwon.toMarkdown(sample.getDescription())

    private Holder createHolder(LayoutScatter inflater, ComponentContainer parent) {
        return new Holder(inflater.parse(ResourceTable.Layout_adapt_sample, parent, false));
    }

    public void render(Holder holder) {
        holder.title.setText(sample.getTitle());
        if (TextUtils.isEmpty(text)) {
            holder.description.setText("");
            holder.description.setVisibility(Component.HIDE);
        } else {
            markwon.setParsedMarkdown(holder.description, text);
            holder.description.setVisibility(Component.VISIBLE);
        }

        List<MarkwonArtifact> artifacts = sample.getArtifacts();
        List<Component> ensure = ensure(sample.getArtifacts().size(), ResourceTable.Layout_view_artifact, holder.artifacts);
        for (int i = 0; i < ensure.size(); i++) {
            Component component = ensure.get(i);
            if (component instanceof Text) {
                MarkwonArtifact markwonArtifact = artifacts.get(i);
                ((Text) component).setText(SampleUtilsKt.getDisplayName(markwonArtifact));
            }
            int finalI = i;
            component.setClickedListener((view) -> {
                sampleItemInterface.openArtifact(artifacts.get(finalI));
            });
        }

        List<String> tags = sample.getTags();
        List<Component> tagsComponent = ensure(sample.getTags().size(), ResourceTable.Layout_view_tag, holder.tags);

        for (int i = 0; i < tags.size(); i++) {
            Component component = tagsComponent.get(i);
            if (component instanceof Text) {
                ((Text) component).setText(SampleUtilsKt.getTagDisplayName(tags.get(i)));
            }
            int finalI = i;
            component.setClickedListener((view) -> {
                sampleItemInterface.openTag(tags.get(finalI));
            });
        }

        ((Component) holder.title.getComponentParent()).setClickedListener((view) -> {
            sampleItemInterface.openSample(sample);
        });

    }

    class Holder {
        final Text title;
        final Text description;
        final FlowLayout artifacts;
        final FlowLayout tags;

        Holder(Component view) {
            title = (Text) view.findComponentById(ResourceTable.Id_title);
            description = (Text) view.findComponentById(ResourceTable.Id_description);
            artifacts = (FlowLayout) view.findComponentById(ResourceTable.Id_artifacts);
            tags = (FlowLayout) view.findComponentById(ResourceTable.Id_tags);
        }
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof SampleItem;
    }

    @Override
    public int hashCode() {
        return sample.hashCode();
    }

    private static List<Component> ensure(int viewsCount, int layoutResId, FlowLayout flowLayout) {
        int childCount = flowLayout.getChildCount();
        if (viewsCount > childCount) {
            LayoutScatter inflater = LayoutScatter.getInstance(flowLayout.getContext());
            for (int i = 0; i < (viewsCount - childCount); i++) {
                Component component = inflater.parse(layoutResId, flowLayout, false);
                flowLayout.addComponent(component);
            }
        } else {
            for (int i = 0; i < childCount; i++) {
                flowLayout.getComponentAt(i).setVisibility(Component.HIDE);
            }
        }

        List<Component> viewsCountList = new ArrayList<>();
        for (int i = 0; i < viewsCount; i++) {
            viewsCountList.add(flowLayout.getComponentAt(i));
        }
        return viewsCountList;
    }

}
