package io.noties.markwon.app.samples.basics;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;

@MarkwonSampleInfo(
        id = "20200626152255",
        title = "Simple",
        description = "The most primitive and simple way to apply markdown to a `TextView`",
        artifacts = {MarkwonArtifact.CORE},
        tags = {Tags.basics}
)
public class Simple extends MarkwonTextViewSample {

    @Override
    public void render() {
        // markdown input
        String md = "# Heading\n" +
                "> A quote\n" +
                "**bold _italic_ bold**".trim();

        // markwon instance
        Markwon markwon = Markwon.create(context);

        // apply raw markdown (internally parsed and rendered)
        markwon.setMarkdown(textView, md);
    }
}
