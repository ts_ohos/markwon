package io.noties.markwon.app.utils;

import com.noties.markwon.utils.TextUtils;

import io.noties.markwon.app.App;
import java.io.IOException;
import java.util.concurrent.Future;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author: yangrui
 * @function: UpdateUtils
 * @date: 2021/3/23
 */
public class UpdateUtils {
    public abstract static class Result {
        public static class UpdateAvailable extends Result {
            public final String revision;
            public final String url;

            UpdateAvailable(String revision, String url) {
                super();
                this.revision = revision;
                this.url = url;
            }
        }

        public static class NoUpdate extends Result {
            NoUpdate() {
                super();
            }
        }

        public static class Error extends Result {

            public final Throwable throwable;

            Error(Throwable throwable) {
                super();
                this.throwable = throwable;
            }
        }
    }

    private static final String GIT_SHA = "50b3168";

    public static Cancellable checkForUpdate(final UpdateUtilsCallBack unit) {
        UpdateUtilsCallBack finalAction = null;
        if (unit != null) {
            finalAction = unit;
        }
        final UpdateUtilsCallBack action = finalAction;
        Future<?> future = App.executorService.submit(() -> {
            String url = String.format("%1s/raw/sample-store/version", TextUtils.getHttps()+"github.com/noties/Markwon");
            Request request = new Request.Builder()
                    .get()
                    .url(url)
                    .build();
            new OkHttpClient().newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    action.action(new Result.Error(e) {
                    });
                }

                @Override
                public void onResponse(Call call, Response response) {
                    try {
                        String revision = response.body().string().trim();
                        boolean hasUpdate = revision != null && GIT_SHA != revision;
                        if (hasUpdate) {
                            // revision is guarded by `hasUpdate` (includes null check)
                            action.action(new Result.UpdateAvailable(revision, apkUrl) {
                            });
                        } else {
                            action.action(new Result.NoUpdate() {
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        action.action(new Result.Error(e) {
                        });
                    }
                }
            });


        });

        return new Cancellable() {
            @Override
            public boolean isCancelled() {
                return future.isDone();
            }

            @Override
            public void cancel() {
                // action = null;
                future.cancel(true);
            }
        };
    }

    private static final String apkUrl = String.format("%1s/raw/sample-store/markwon-debug.apk", TextUtils.getHttps()+"github.com/noties/Markwon");

    public interface UpdateUtilsCallBack {
        /**
         * down load callback
         *
         * @param result Result
         */
        void action(Result result);
    }
}
