package io.noties.markwon.app.sample;

public class Tags {
    public static final String basics = "basics";
    public static final String toast = "toast";
    public static final String hack = "hack";
    public static final String parsing = "parsing";
    public static final String block = "block";
    public static final String movementMethod = "movement-method";
    public static final String links = "links";
    public static final String plugin = "plugin";
    public static final String recyclerView = "recycler-view";
    public static final String paragraph = "paragraph";
    public static final String rendering = "rendering";
    public static final String style = "style";
    public static final String theme = "theme";
    public static final String image = "image";
    public static final String newLine = "new-line";
    public static final String softBreak = "soft-break";
    public static final String defaults = "defaults";
    public static final String spacing = "spacing";
    public static final String padding = "padding";
    public static final String heading = "heading";
    public static final String anchor = "anchor";
    public static final String lists = "lists";
    public static final String extension = "extension";
    public static final String textAddedListener = "text-added-listener";
    public static final String editor = "editor";
    public static final String span = "span";
    public static final String svg = "SVG";
    public static final String gif = "GIF";
    public static final String inline = "inline";
    public static final String html = "HTML";
    public static final String knownBug = "known-bug";
    public static final String precomputedText = "precomputed-text";
    public static final String cache = "cache";
    public static final String spanFactory = "span-factory";
}
