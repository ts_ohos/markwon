package io.noties.markwon.app.utils;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.Editable;
import ohos.agp.components.Text;

abstract class TextWatcherAdapter implements Text.TextObserver {
//    @Override
//    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//    }

    @Override
    public void onTextUpdated(String s, int start, int before, int count) {

    }

//    @Override
//    public void afterTextChanged(Editable s) {
//
//    }

    interface AfterTextChanged {
        void afterTextChanged(Editable s);
    }

    @NonNull
    public static Text.TextObserver afterTextChanged(@NonNull AfterTextChanged afterTextChanged) {
        return new TextWatcherAdapter() {
           // @Override
            public void afterTextChanged(Editable s) {
                afterTextChanged.afterTextChanged(s);
            }
        };
    }
}
