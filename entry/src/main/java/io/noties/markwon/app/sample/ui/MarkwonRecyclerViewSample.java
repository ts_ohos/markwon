package io.noties.markwon.app.sample.ui;

import com.noties.markwon.wrapper.view.RecyclerView;
import io.noties.markwon.app.ResourceTable;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * @author: yangrui
 * @function: MarkwonRecyclerViewSample
 * @date: 2021/3/23
 */
public abstract class MarkwonRecyclerViewSample extends MarkwonSample {

    protected Context context;
    protected RecyclerView recyclerView;

    @Override
    public void onViewCreated(Component view) {
        context = view.getContext();
        recyclerView = (RecyclerView) view.findComponentById(ResourceTable.Id_recycler_view);
        render();
    }

    @Override
    protected int layoutResId() {
        return ResourceTable.Layout_sample_recycler_view;
    }

    /**
     * render item
     */
    protected abstract void render();
}
