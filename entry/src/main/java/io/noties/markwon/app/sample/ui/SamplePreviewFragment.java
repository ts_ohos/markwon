package io.noties.markwon.app.sample.ui;

import com.noties.markwon.utils.LogUtils;

import io.noties.markwon.app.sample.Sample;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * @author: yangrui
 * @function:
 * @date: 2021/3/23
 */
public class SamplePreviewFragment extends Fraction {


    private final Sample sample;
    private SamplePreviewFragment(Sample sample) {
        this.sample = sample;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return getMarkwonSample().createView(scatter, container);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        getMarkwonSample().onViewCreated(getComponent());
    }

    private MarkwonSample getMarkwonSample() {
        try {
            String javaClassName = sample.getJavaClassName();
            LogUtils.i(getClass().getSimpleName(),javaClassName);
            return (MarkwonSample) Class.forName(javaClassName).newInstance();
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static String ARG_SAMPLE = "arg.Sample";

    public static SamplePreviewFragment getInstance(Sample sample) {
        return new SamplePreviewFragment(sample);
    }

}
