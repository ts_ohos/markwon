package io.noties.markwon.app.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

public class CardView extends StackLayout {
    public CardView(Context context) {
        super(context);
    }

    public CardView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public CardView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
