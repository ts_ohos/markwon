package io.noties.markwon.app.samples;

import io.noties.markwon.Markwon;
import io.noties.markwon.PrecomputedTextSetterCompat;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@MarkwonSampleInfo(
  id = "20200702091654",
  title = "PrecomputedTextSetterCompat",
  description = "`TextSetter` to use `PrecomputedTextSetterCompat`",
  artifacts = MarkwonArtifact.CORE,
  tags = Tags.precomputedText
)
public class PrecomputedSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "" +
      "# Heading\n" +
      "**bold** some precomputed spans via `PrecomputedTextSetterCompat`";

    final Markwon markwon = Markwon.builder(context)
      .textSetter(PrecomputedTextSetterCompat.create(new ThreadPoolExecutor(1, 4, 1000,
        TimeUnit.MILLISECONDS, new SynchronousQueue<>(), Executors.defaultThreadFactory(),
        new ThreadPoolExecutor.AbortPolicy())))
      .build();

    markwon.setMarkdown(textView, md);
  }
}
