package io.noties.markwon.app.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.noties.markwon.annotation.NonNull;

import io.noties.markwon.app.sample.Sample;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;

public abstract class SampleUtils {

    private static final String SAMPLE_PREFIX = "io.noties.markwon.app.";

    @NonNull
    public static List<Sample> readSamples(@NonNull Context context) {

        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry("resources/rawfile/samples.json");
        try (InputStream inputStream = rawFileEntry.openRawFile()) {
            return readSamples(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // NB! stream is not closed by this method
    @NonNull
    private static List<Sample> readSamples(@NonNull InputStream inputStream) {
        final Gson gson = new Gson();
        return gson.fromJson(
                new InputStreamReader(inputStream),
                new TypeToken<List<Sample>>() {
                }.getType()
        );
    }

    private SampleUtils() {
    }
}
