package io.noties.markwon.app.readme;

import com.noties.markwon.annotation.NonNull;
import io.noties.markwon.LinkResolverDef;
import io.noties.markwon.app.utils.ReadMeUtils;
import ohos.agp.components.Component;
import ohos.utils.Pair;

public class ReadMeLinkResolver extends LinkResolverDef {
    @Override
    public void resolve(@NonNull Component view, @NonNull String link) {
        Pair<String, String> info = ReadMeUtils.parseRepository(link);
        String url = null;
        if (info != null) {
            url = ReadMeUtils.buildRepositoryReadMeUrl(info.f, info.s);
        } else {
            url = link;
        }
        super.resolve(view, url);
    }
}
