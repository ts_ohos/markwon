package io.noties.markwon.app.samples.movementmethod;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.movement.MovementMethodPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;

/**
 * @author: yangrui
 * @function: DisableImplicitMovementMethodPluginSample
 * @date: 2021/3/23
 */
@MarkwonSampleInfo(
  id = "20200629121803",
  title = "Disable implicit movement method via plugin",
  description = "Disable implicit movement method via `MovementMethodPlugin`",
  artifacts = {MarkwonArtifact.CORE},
  tags = {Tags.links, Tags.movementMethod, Tags.recyclerView}
)
public class DisableImplicitMovementMethodPluginSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "# Disable implicit movement method via plugin\n" +
      "We can disable implicit movement method via `MovementMethodPlugin` &mdash;\n" +
      "[link-that-is-not-clickable](https://noties.io)".trim();

    Markwon markwon = Markwon.builder(context)
      .usePlugin(MovementMethodPlugin.none())
      .build();

    markwon.setMarkdown(textView, md);
  }
}
