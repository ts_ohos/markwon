package io.noties.markwon.app.sample.ui;

import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.MainAbility;
import io.noties.markwon.app.sample.Sample;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.utils.Color;

import java.util.Optional;

/**
 * @author: yangrui
 * @function: SampleFragment
 * @date: 2021/3/23
 */
public class SampleFragment extends Fraction {

    private ComponentContainer containerSample;
    private boolean isCodeSelected = false;

    private final Sample sample;

    private SampleFragment(Sample sample) {
        this.sample = sample;
    }


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return scatter.parse(ResourceTable.Layout_fragment_sample, container, false);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        containerSample = (ComponentContainer) getComponent().findComponentById(ResourceTable.Id_container_sample);
        // TODO YR lack API
        isCodeSelected = false;
        initAppBar(getComponent());
        initTabBar(getComponent());
    }


    private void initAppBar(Component view) {
        Component appBar = view.findComponentById(ResourceTable.Id_app_bar);
        Component icon = appBar.findComponentById(ResourceTable.Id_app_bar_icon);
        Text title = (Text) appBar.findComponentById(ResourceTable.Id_app_bar_title);
        icon.setClickedListener((it) -> {
            MainAbility mainAbility = (MainAbility) getFractionAbility();
            // TODO YR different API
            if (mainAbility != null) {
                mainAbility.onBackPressedMainAbility();
            }
        });

        title.setText(sample.getTitle());

    }

    private void initTabBar(Component view) {
        Component tabBar = view.findComponentById(ResourceTable.Id_tab_bar);
        Component preview = tabBar.findComponentById(ResourceTable.Id_tab_bar_preview);
        Image previewImage = (Image) tabBar.findComponentById(ResourceTable.Id_preview_image);
        Text previewText = (Text) tabBar.findComponentById(ResourceTable.Id_preview_text);

        Component code = tabBar.findComponentById(ResourceTable.Id_tab_bar_code);
        Image codeImage = (Image) tabBar.findComponentById(ResourceTable.Id_code_image);
        Text codeText = (Text) tabBar.findComponentById(ResourceTable.Id_code_text);

        preview.setClickedListener((it) -> {
            previewImage.setImageElement(ElementScatter.getInstance(it.getContext()).parse(ResourceTable.Graphic_ic_remove_red_eye_green_24dp));
            previewText.setTextColor(new Color(0xff009900));

            codeImage.setImageElement(ElementScatter.getInstance(it.getContext()).parse(ResourceTable.Graphic_ic_code_white_24dp));
            codeText.setTextColor(new Color(0xff777777));

            showPreview();
        });

        code.setClickedListener((it) -> {
            codeImage.setImageElement(ElementScatter.getInstance(it.getContext()).parse(ResourceTable.Graphic_ic_code_green_24dp));
            codeText.setTextColor(new Color(0xff009900));

            previewImage.setImageElement(ElementScatter.getInstance(it.getContext()).parse(ResourceTable.Graphic_ic_remove_red_eye_white_24dp));
            previewText.setTextColor(new Color(0xff777777));

            showCode();
        });

    }

    private void showPreview() {
        isCodeSelected = false;
        showFragment(TAG_PREVIEW, TAG_CODE, SamplePreviewFragment.getInstance(sample));
    }

    private void showCode() {
        isCodeSelected = true;
        showFragment(TAG_CODE, TAG_PREVIEW, SampleCodeFragment.getInstance(sample));
    }

    private void showFragment(String showTag, String hideTag, Fraction provider) {

//        getFractionAbility().getFractionManager()
//            .startFractionScheduler()
//            .add(ResourceTable.Id_container, provider)
//            .pushIntoStack(provider.getClass().getSimpleName())
//            .submit();

        // SDK5 没有类似 getChildFragmentManager
        FractionManager fraction = getFractionAbility().getFractionManager();
        FractionScheduler manager = fraction.startFractionScheduler();
        Optional<Fraction> existing = fraction.getFractionByTag(showTag);

        if (existing.isPresent()) {
            if (existing.get().equals(provider)) {
                manager.show(existing.get());
            } else {
                manager.hide(existing.get());
                manager.add(ResourceTable.Id_container, provider, showTag);
            }
            manager.pushIntoStack(existing.get().toString());
            manager.pushIntoStack(provider.getClass().getSimpleName());
        } else {
            manager.add(ResourceTable.Id_container, provider, showTag);
            manager.pushIntoStack(provider.getClass().getSimpleName());
        }

        Optional<Fraction> fractionHideTag = fraction.getFractionByTag(hideTag);
        if (fractionHideTag.isPresent()) {
            manager.hide(fractionHideTag.get());
            manager.pushIntoStack(fractionHideTag.get().toString());
        }
        manager.submit();
    }


    private Sample getSample() {
        return sample;
    }

    private static String ARG_SAMPLE = "arg.Sample";
    private static final String TAG_PREVIEW = "tag.Preview";
    private static final String TAG_CODE = "tag.Code";
    private static String KEY_CODE_SELECTED = "key.Selected";

    public static SampleFragment getInstance(Sample sample) {
        return new SampleFragment(sample);
    }

}
