package io.noties.markwon.app.readme;



import com.noties.markwon.annotation.NonNull;
import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.app.utils.ReadMeUtils;
import ohos.utils.net.Uri;

public class ReadMeImageDestinationPlugin extends AbstractMarkwonPlugin {
    private final Uri data;
    public ReadMeImageDestinationPlugin(Uri data) {
        this.data = data;
    }

    @Override
    public void configureConfiguration(@NonNull MarkwonConfiguration.Builder builder) {
        super.configureConfiguration(builder);
        ReadMeUtils.GithubInfo info = ReadMeUtils.parseInfo(data);
        if (info == null) {
            builder.imageDestinationProcessor(new GithubImageDestinationProcessor());
        } else {
            builder.imageDestinationProcessor(new GithubImageDestinationProcessor(
                    info.getUsername(),
                    info.getRepository(),
                    info.getBranch()
            ));
        }
    }
}
