package io.noties.markwon.app.samples.parser;


import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.core.CorePlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import java.util.Set;
import java.util.stream.Collectors;

import org.commonmark.node.Heading;
import org.commonmark.parser.Parser;
import org.commonmark.parser.block.BlockParserFactory;
import org.commonmark.parser.block.BlockStart;
import org.commonmark.parser.block.MatchedBlockParser;
import org.commonmark.parser.block.ParserState;

/**
 * @author: yangrui
 * @function: CustomHeadingParserSample
 * @date: 2021/3/23
 */

@MarkwonSampleInfo(
  id = "20201111221207",
  title = "Custom heading parser",
  description = "Custom heading block parser. Actual parser is not implemented",
  artifacts = {MarkwonArtifact.CORE},
  tags = {Tags.parsing, Tags.heading}
)
public class CustomHeadingParserSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "#Head";
    Markwon markwon = Markwon.builder(context)
      .usePlugin(new AbstractMarkwonPlugin() {
                   @Override
                   public void configureParser(Parser.Builder builder) {

                     Set enabled = CorePlugin.enabledBlockTypes()
                       .stream()
                       .filter(it -> it != null && it.equals(Heading.class))
                       .collect(Collectors.toSet());
                     builder.enabledBlockTypes(enabled);
                     builder.customBlockParserFactory((parserState, matchedBlockParser) -> null);
                   }
                 }
      )
      .build();
    markwon.setMarkdown(textView, md);
  }
}
