package io.noties.markwon.app.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ThrowableUtils {
    public static  String stackTraceString(Throwable throwable){
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        throwable.printStackTrace(printWriter);
        return stringWriter.toString();
    }
}
