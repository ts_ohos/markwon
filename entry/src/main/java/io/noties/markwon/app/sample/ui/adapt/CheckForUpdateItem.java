package io.noties.markwon.app.sample.ui.adapt;

import com.noties.markwon.utils.Unit;
import io.noties.markwon.app.ResourceTable;
import ohos.agp.components.*;

/**
 * @author: yangrui
 * @function: lack lib noties adapt, this extend Item<H extends Item.Holder>
 * @date: 2021/3/23
 */
public class CheckForUpdateItem {

    private final Unit unit;

    public CheckForUpdateItem(Unit unit) {
        this.unit = unit;
    }

    public Holder createHolder(LayoutScatter inflater, ComponentContainer parent) {
        return new Holder(inflater.parse(ResourceTable.Layout_adapt_check_for_update, parent, false));
    }

    public void render(Holder holder) {
        holder.button.setClickedListener((it) -> {
            unit.action();
        });
    }

    static class Holder {
        final Button button;

        Holder(Component view) {
            button = (Button) view.findComponentById(ResourceTable.Id_button);
        }
    }
}
