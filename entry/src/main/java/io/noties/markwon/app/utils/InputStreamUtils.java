package io.noties.markwon.app.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

class InputStreamUtils {

    public static String readStringAndClose(InputStream inputStream) {
        try {
            Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
            if (scanner.hasNext()) {
                return scanner.next();
            }
            return "";
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                // ignored
            }
        }
    }
}
