package io.noties.markwon.app.utils;

import java.util.ArrayList;
import java.util.List;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

/**
 * @author: yangrui
 * @function: ViewGroupUtils
 * @date: 2021/3/23
 */
class ViewGroupUtils {

    private final List<Component> childrens = new ArrayList<>();

    public List get(ComponentContainer children) {
        for (int i = 0; i < children.getChildCount(); i++) {
            childrens.add(children.getComponentAt(i));
        }
        return childrens;
    }
}
