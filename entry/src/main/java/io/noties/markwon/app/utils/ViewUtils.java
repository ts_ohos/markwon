package io.noties.markwon.app.utils;

import com.noties.markwon.utils.Unit;
import com.noties.markwon.wrapper.view.RecyclerView;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

/**
 * @author: yangrui
 * @function: ViewUtils
 * @date: 2021/3/23
 */
public class ViewUtils {

    public static void hidden(Component view, Boolean hidden) {
        view.setVisibility(hidden ? Component.HIDE : Component.VISIBLE);
    }

    public static void setActive(Component view, Boolean active) {
        if (view instanceof ComponentContainer) {
            // TODO YR lack API setActivated
            // view.setActivated(active);
            for (int i = 0; i < ((ComponentContainer) view).getChildCount(); i++) {
                Component componentAt = ((ComponentContainer) view).getComponentAt(i);
                // componentAt.setActivated(active);
            }
        }
    }

    public static boolean getActive(Component view) {
        return false;
    }

    public static void onPreDraw(RecyclerView recyclerView, Unit unit) {
        // TODO YR different api
        recyclerView.getComponentTreeObserver();

    }

}

