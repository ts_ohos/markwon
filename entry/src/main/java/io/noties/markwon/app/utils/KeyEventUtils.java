package io.noties.markwon.app.utils;


import ohos.multimodalinput.event.KeyEvent;

public class KeyEventUtils {

    public static boolean isActionUp(KeyEvent event) {
        return event == null || (KeyEvent.KEY_PAGE_UP == event.getKeyCode());
    }
}
