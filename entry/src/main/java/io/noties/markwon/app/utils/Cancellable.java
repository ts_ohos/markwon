package io.noties.markwon.app.utils;

public interface Cancellable {
    /**
     * is isCancelled.
     * @return boolean
     */
    boolean isCancelled();

    /**
     * is isCancelled.
     */
    void cancel();
}
