package io.noties.markwon.app.widget;

import com.noties.markwon.annotation.Nullable;
import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.utils.KeyboardUtils;
import io.noties.markwon.app.utils.ViewUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.render.layoutboost.LayoutBoost;
import ohos.app.Context;

/**
 * @author: yangrui
 * @function: SearchBarCustom
 * @date: 2021/3/23
 */
public class SearchBarCustom extends DirectionalLayout {

    private Component focus;
    private TextFieldCustom textField;
    private Component clear;
    private Component cancel;
    private OnSearchListener onSearchListener = null;

    public void setOnSearchListener(OnSearchListener onSearchListener) {
        this.onSearchListener = onSearchListener;
    }

    public SearchBarCustom(Context context, @Nullable AttrSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {

        setOrientation(HORIZONTAL);
        /**
         * Gravity.CENTER_VERTICAL
         */
        setAlignment(16);
        Component inflate = LayoutBoost.inflate(context, ResourceTable.Layout_view_search_bar, this, true);
        focus = inflate.findComponentById(ResourceTable.Id_focus);
        textField = (TextFieldCustom) inflate.findComponentById(ResourceTable.Id_text_field);
        clear = inflate.findComponentById(ResourceTable.Id_clear);
        cancel = inflate.findComponentById(ResourceTable.Id_cancel);

        // listen for text state
        textField.addTextObserver((s, i, i1, i2) -> textFieldChanged(s));


        textField.setOnBackPressedListener(this::looseFocus);

        textField.setFocusChangedListener((component, hasFocus) -> {
            String text = textField.getText();
            boolean empty = text.length() == 0;
            ViewUtils.hidden(cancel, empty && !hasFocus);

        });
        textField.setEditorActionListener(event -> {
            // TODO YR API different
            /*if (KeyEventUtils.isActionUp(event)) {
                looseFocus();
            }*/
            return true;
        });

        clear.setClickedListener(component -> {
            textField.setText("");
            if (!textField.hasFocus()) {
                textField.requestFocus();
                // additionally ensure keyboard is showing
                KeyboardUtils.show(textField);
            }
        });


        cancel.setClickedListener(component -> {
            textField.setText("");
            looseFocus();
        });
        // TODO YR lack API setSaveEnabled
        // textField.setSaveEnabled(false);
    }


    private void looseFocus() {
        KeyboardUtils.hide(textField);
        focus.requestFocus();
    }


    public void search(String text) {
        textField.setText(text);
    }

    private void textFieldChanged(CharSequence text) {
        boolean isEmpty = text.length() == 0;
        ViewUtils.hidden(clear, isEmpty);
        ViewUtils.hidden(cancel, isEmpty && !textField.hasFocus());
        if (text != null) {
            onSearchListener.onListener(text.toString());
        }
    }

    public interface OnSearchListener {
        void onListener(String string);
    }
}
