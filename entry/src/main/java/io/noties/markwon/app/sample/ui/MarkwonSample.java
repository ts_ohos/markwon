package io.noties.markwon.app.sample.ui;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * @author: yangrui
 * @function: MarkwonSample
 * @date: 2021/3/23
 */
public abstract class MarkwonSample {

    Component createView(LayoutScatter inflater, ComponentContainer container) {
        return inflater.parse(layoutResId(), container, false);
    }

    /**
     *  created view
     * @param view Component
     */
    protected abstract void onViewCreated(Component view);

    /**
     * layout Id with int
     * @return int layout
     */
    protected abstract int layoutResId();
}
