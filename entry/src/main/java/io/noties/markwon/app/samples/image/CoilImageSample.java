package io.noties.markwon.app.samples.image;


import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.image.coil.CoilImagesPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;

/**
 * @author: yangrui
 * @function: CoilImageSample
 * @date: 2021/3/23
 */
@MarkwonSampleInfo(
  id = "20200826101209",
  title = "Coil image",
  artifacts = {MarkwonArtifact.IMAGE_COIL},
  tags = {Tags.image}
)
public class CoilImageSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    String md = " # H1\n" +
      "      ## H2\n" +
      "      ### H3\n" +
      "      #### H4\n" +
      "      ##### H5\n" +
      "      \n" +
      "      > a quote\n" +
      "      \n" +
      "      + one\n" +
      "      - two\n" +
      "      * three\n" +
      "      \n" +
      "      1. one\n" +
      "      1. two\n" +
      "      1. three\n" +
      "      \n" +
      "      ---\n" +
      "      \n" +
      "      # Images\n" +
      "      \n" +
      "      ![img](https://picsum.photos/id/237/1024/800)";

    Markwon markwon = Markwon.builder(context)
//      .usePlugin(coilPlugin1)
//      .usePlugin(coilPlugin2)
      .usePlugin(coilPlugin3)
      .build();
    markwon.setMarkdown(textView, md);
  }

  private CoilImagesPlugin coilPlugin1 = getCoilPlugin1();

  private CoilImagesPlugin getCoilPlugin1() {

    return CoilImagesPlugin.create(context);
  }

  private CoilImagesPlugin coilPlugin2 = getCoilPlugin2();

  private CoilImagesPlugin getCoilPlugin2() {

    return CoilImagesPlugin.create(context, getImageLoader());
  }

  private final CoilImagesPlugin coilPlugin3 = getCoilPlugin3();

  private CoilImagesPlugin getCoilPlugin3() {

    Object imageLoader = this.imageLoader;
    CoilImagesPlugin coilImagesPlugin = CoilImagesPlugin.create(new CoilImagesPlugin.CoilStore() {
      @Override
      public void load() {

      }

      @Override
      public void cancel() {

      }
    }, imageLoader);
    return coilImagesPlugin;
  }

  /**
   * TODO YR lack lib io.coil-kt:coil:0.13.0
   */
  private final Object imageLoader = getImageLoader();

  private Object getImageLoader() {
    return null;
  }

}
