package io.noties.markwon.app.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ScrollView;
import ohos.app.Context;

public class NestedScrollView extends ScrollView {
    public NestedScrollView(Context context) {
        super(context);
    }

    public NestedScrollView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public NestedScrollView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
