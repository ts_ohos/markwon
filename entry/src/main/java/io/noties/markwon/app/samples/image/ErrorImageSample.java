package io.noties.markwon.app.samples.image;


import com.noties.markwon.utils.TextUtils;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.image.ImagesPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;

@MarkwonSampleInfo(
        id = "20200630165828",
        title = "Image error handler",
        artifacts = MarkwonArtifact.IMAGE,
        tags = Tags.image
)
public class ErrorImageSample extends MarkwonTextViewSample {
    @Override
    public void render() {
        final String md = "" +
                "![error]("+ TextUtils.getHttps() +"github.com/dcurtis/markdown-mark/raw/master/png/______1664x1024-solid.png)";

        final Markwon markwon = Markwon.builder(context)
                // error handler additionally allows to log/inspect errors during image loading
                .usePlugin(ImagesPlugin.create(plugin -> {
                    plugin.errorHandler((url, throwable) -> {
                        Element element = ElementScatter.getInstance(context).parse(ResourceTable.Graphic_ic_home_black_36dp);
                        // TODO YR lack API
                        return null;
                    });
                }))
                .build();

        markwon.setMarkdown(textView, md);
    }
}
