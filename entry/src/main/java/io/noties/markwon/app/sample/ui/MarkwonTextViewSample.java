package io.noties.markwon.app.sample.ui;

import io.noties.markwon.app.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollView;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * @author: yangrui
 * @function: MarkwonTextViewSample
 * @date: 2021/3/23
 */
public abstract class MarkwonTextViewSample extends MarkwonSample {

    protected Context context;
    protected ScrollView scrollView;
    protected Text textView;

    @Override
    protected int layoutResId() {
        return ResourceTable.Layout_sample_text_view;
    }

    @Override
    public void onViewCreated(Component view) {
        context = view.getContext();
        scrollView = (ScrollView) view.findComponentById(ResourceTable.Id_scroll_view);
        textView = (Text) view.findComponentById(ResourceTable.Id_text_view);
        render();

    }

    /**
     * render item
     */
    protected abstract void render();

}
