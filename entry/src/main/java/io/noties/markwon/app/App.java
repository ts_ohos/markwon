package io.noties.markwon.app;

import com.noties.markwon.annotation.NonNull;

import io.noties.markwon.app.sample.SampleManager;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import ohos.aafwk.ability.AbilityPackage;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;


public class App extends AbilityPackage {
    public static final String SHORTCUT_ID = "shortcut_id";
    private ThreadFactory namedThreadFactory = runnable -> null;
    public static ThreadPoolExecutor executorService;
    @NonNull
    public static SampleManager sampleManager;

    public static App appInstance;

    @Override
    public void onInitialize() {
        super.onInitialize();

        // TODO Remove
        // Debug.init(AndroidLogDebugOutput(BuildConfig.DEBUG))
        executorService = new ThreadPoolExecutor(1, 4, 1000,
            TimeUnit.MILLISECONDS, new SynchronousQueue<>(), Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());

        sampleManager = new SampleManager(this, executorService);

        ensureReadmeShortcut();
        appInstance = this;
    }

    private void ensureReadmeShortcut() {

        // TODO Amend, HOS doesn't dynamicShortcuts
        /*IBundleManager bundleManager = getBundleManager();

        ShortcutInfo shortcutInfo = new ShortcutInfo();
        ShortcutIntent shortcutIntent = new ShortcutIntent();
        shortcutIntent.setTargetBundle("");
        shortcutIntent.setTargetClass("");
        shortcutInfo.setId(SHORTCUT_ID);
        shortcutInfo.setIntent(shortcutIntent);
        shortcutInfo.setBundleName("");
        shortcutInfo.setHostAbilityName("");
        shortcutInfo.setLabel("README");
        bundleManager.addHomeShortcut(shortcutInfo);*/
    }

    private static final class Companion {
        public final ExecutorService getExecutorService() {

            return App.executorService;
        }

        public final void setExecutorService(@NonNull ThreadPoolExecutor service) {
            App.executorService = service;
        }

        @NonNull
        public final SampleManager getSampleManager() {
            SampleManager sampleManager = App.sampleManager;
            if (sampleManager == null) {
                return new SampleManager(App.appInstance, executorService);
            }
            return sampleManager;
        }

        public final void setSampleManager(@NonNull SampleManager sampleManager) {
            App.sampleManager = sampleManager;
        }

        private Companion() {
        }

    }

    private String getResString(int resId) {
        try {
            return getContext().getResourceManager().getElement(resId).getString();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return "";
    }
}
