package io.noties.markwon.app.sample;

import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.ui.SampleListFragment;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

/**
 * @author: yangrui
 * @function: MainAbility
 * @date: 2021/3/23
 */
public class MainAbility extends FractionAbility {

    private SampleListFragment sampleListFragment;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        sampleListFragment = SampleListFragment.getInstance();
        getFractionManager()
                .startFractionScheduler()
                .replace(ResourceTable.Id_container, sampleListFragment)
                .submit();

        // process deeplink if we are not restored
        /*Deeplink deeplink = Deeplink.parse(intent.getUri());
        Fraction deepLinkFragment = null;
        if (deeplink != null) {
            if (deeplink instanceof Deeplink.Sample) {
                Sample sample = App.sampleManager.sample(((Deeplink.Sample) deeplink).getId());
                deepLinkFragment = SampleFraction.init(sample);
            } else if (deeplink instanceof Deeplink.Search) {
                deepLinkFragment = SampleListFraction.init(((Deeplink.Search) deeplink).getSearch());
            }
        }
        if (deepLinkFragment != null) {
            getFractionManager()
                    .startFractionScheduler()
                    .replace(ResourceTable.Id_container, deepLinkFragment)
                    .submit();
        }*/
    }

    /**
     * keyboard BackPressed
     */
    public void onBackPressedMainAbility() {
        onBackPressed();
    }

    /**
     * 界面绘制完成标志，然后去筛选数据.
     *
     * @param hasFocus 获得焦点.
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && sampleListFragment != null) {
            sampleListFragment.fetch();
        }
    }
}
