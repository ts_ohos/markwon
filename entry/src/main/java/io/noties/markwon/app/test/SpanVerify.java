package io.noties.markwon.app.test;

import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.html.span.SuperScriptSpan;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;

/**
 * @author: yangrui
 * @function: SpanVerify
 * @date: 2021/4/9
 */
public class SpanVerify extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_view_html_details_text_view, container, false);

        Text text = (Text) component.findComponentById(ResourceTable.Id_text_view);
        text.setMultipleLine(true);

        RichTextBuilder builder = new RichTextBuilder();

        builder.mergeForm(new TextForm().setRelativeTextSize(1.5f));
        builder.addText("你有新消息了");
        builder.revertForm();

        builder.mergeForm(new TextForm().setTextColor(Color.BLACK.getValue()));
        builder.addText("颜色字体");
        builder.revertForm();

        builder.mergeForm(new TextForm().setSuperscript(true));
        builder.addText("●");
        builder.revertForm();

        builder.mergeForm(new TextForm().setTextColor(Color.RED.getValue()));
        builder.addText("颜色字体");
        builder.revertForm();

        builder.mergeForm(new TextForm().setSubscript(true));
        builder.addText("1");
        builder.revertForm();

        builder.mergeForm(new SuperScriptSpan());
        builder.addText("你有新消息了");
        builder.revertForm();

        RichText richText = builder.build();
        text.setRichText(richText);

        return component;
    }
}
