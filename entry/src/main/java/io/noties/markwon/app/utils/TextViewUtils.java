package io.noties.markwon.app.utils;

import com.noties.markwon.utils.TextUtils;
import ohos.agp.components.Text;

public class TextViewUtils {

    public static void textOrHide(Text tt, CharSequence text) {
        if (TextUtils.isEmpty(text)) {
            tt.setVisibility(Text.HIDE);
        }else {
            tt.setText(text.toString());
        }
    }
}
