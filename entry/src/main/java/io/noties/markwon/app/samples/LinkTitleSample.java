package io.noties.markwon.app.samples;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.wrapper.text.Spanned;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.LinkResolver;
import io.noties.markwon.Markwon;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.core.CoreProps;
import io.noties.markwon.core.MarkwonTheme;
import io.noties.markwon.core.spans.LinkSpan;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import java.util.Locale;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

import org.commonmark.node.Link;

@MarkwonSampleInfo(
  id = "20200629122230",
  title = "Obtain link title",
  description = "Obtain title (text) of clicked link, `[title](#destination)`",
  artifacts = {MarkwonArtifact.CORE},
  tags = {Tags.links, Tags.span}
)
public class LinkTitleSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "" +
      "# Links\n\n" +
      "[link title](#)";

    final Markwon markwon = Markwon.builder(context)
      .usePlugin(new AbstractMarkwonPlugin() {
        @Override
        public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {
          builder.setFactory(Link.class, (configuration, props) ->
            // create a subclass of markwon LinkSpan
            new ClickSelfSpan(
              configuration.theme(),
              CoreProps.LINK_DESTINATION.require(props),
              configuration.linkResolver()
            )
          );
        }
      })
      .build();

    markwon.setMarkdown(textView, md);
  }
}

class ClickSelfSpan extends LinkSpan {

  ClickSelfSpan(
    @NonNull MarkwonTheme theme,
    @NonNull String link,
    @NonNull LinkResolver resolver) {
    super(theme, link, resolver);
  }

  @Override
  public void onClick(Component widget) {
    new ToastDialog(widget.getContext()).setText(
      String.format(Locale.ROOT, "clicked link title: '%s'", linkTitle(widget))
    ).show();
    super.onClick(widget);
  }

  @Nullable
  private CharSequence linkTitle(@NonNull Component widget) {

    if (!(widget instanceof Text)) {
      return null;
    }

    final Spanned spanned = (Spanned) (((Text) widget).getText().subSequence(0, ((Text) widget).getText().length()));
    final int start = spanned.getSpanStart(this);
    final int end = spanned.getSpanEnd(this);

    if (start < 0 || end < 0) {
      return null;
    }

    return spanned.subSequence(start, end);
  }
}
