package io.noties.markwon.app.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

public class AppCompatTextView extends Text {
    public AppCompatTextView(Context context) {
        super(context);
    }

    public AppCompatTextView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public AppCompatTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
