package io.noties.markwon.app.utils;

import ohos.accessibility.ability.AccessibleAbility;
import ohos.accessibility.ability.SoftKeyBoardController;
import ohos.agp.components.Component;

/**
 * @author: yangrui
 * @function: KeyboardUtils
 * @date: 2021/3/23
 */
public class KeyboardUtils {

    public static void show(Component view) {

        // TODO YR lack API getSystemService
        /*InputMethodManager systemService = view.getContext().getSystemService(InputMethodManager.class);
        if (systemService != null) {
            systemService.showSoftInput(view, 0);
        }*/

        SoftKeyBoardController softKeyBoardController = new SoftKeyBoardController(view.getId(), 0);
        softKeyBoardController.setShowMode(AccessibleAbility.SHOW_MODE_AUTO);
    }


    public static void hide(Component view) {
       /* InputMethodManager systemService = view.getContext().getSystemService(InputMethodManager.class);
        if (systemService != null) {
            systemService.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/

        SoftKeyBoardController softKeyBoardController = new SoftKeyBoardController(view.getId(), 0);
        softKeyBoardController.setShowMode(AccessibleAbility.SHOW_MODE_HIDE);


    }
}
