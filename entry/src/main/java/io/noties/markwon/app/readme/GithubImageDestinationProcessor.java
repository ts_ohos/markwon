package io.noties.markwon.app.readme;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.utils.TextUtils;

import io.noties.markwon.image.destination.ImageDestinationProcessor;
import io.noties.markwon.image.destination.ImageDestinationProcessorRelativeToAbsolute;
import ohos.utils.net.Uri;

public class GithubImageDestinationProcessor extends ImageDestinationProcessor {

    private String username = "noties";
    private String repository = "Markwon";
    private String branch = "master";

    private ImageDestinationProcessorRelativeToAbsolute processor;

    public GithubImageDestinationProcessor() {
    }

    public GithubImageDestinationProcessor(String username, String repository, String branch) {
        this.username = username;
        this.repository = repository;
        this.branch = branch;
        processor = new ImageDestinationProcessorRelativeToAbsolute(String.format("%sgithub.com/%s/%s/raw/%s/",
            TextUtils.getHttps(),this.username, this.repository, this.branch));
    }

    @NonNull
    @Override
    public String process(@NonNull String destination) {
        Uri uri = Uri.parse(destination);
        if (TextUtils.isEmpty(uri.getScheme())) {
            return processor.process(destination);
        } else {
            return destination;
        }
    }
}
