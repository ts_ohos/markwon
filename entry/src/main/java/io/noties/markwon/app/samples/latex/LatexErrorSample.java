package io.noties.markwon.app.samples.latex;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import com.noties.markwon.utils.LogUtils;
import com.noties.markwon.wrapper.graphics.drawable.Drawable;
import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.ext.latex.JLatexMathPlugin;
import io.noties.markwon.inlineparser.MarkwonInlineParserPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;

@MarkwonSampleInfo(
  id = "20200701122624",
  title = "LaTeX error handling",
  description = "Log error when parsing LaTeX and display error drawable",
  artifacts = MarkwonArtifact.EXT_LATEX,
  tags = Tags.rendering
)
public class LatexErrorSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "" +
      "# LaTeX with error\n" +
      "$$\n" +
      "\\sum_{i=0}^\\infty x \\cdot 0 \\rightarrow \\iMightNotExist{0}\n" +
      "$$\n\n" +
      "must **not** be rendered";

    final Markwon markwon = Markwon.builder(context)
      .usePlugin(MarkwonInlineParserPlugin.create())
      .usePlugin(JLatexMathPlugin.create(textView.getTextSize(), builder -> {
        builder.inlinesEnabled(true);
        //noinspection Convert2Lambda
        builder.errorHandler(new JLatexMathPlugin.ErrorHandler() {
          @Nullable
          @Override
          public Drawable handleError(@Nullable String latex, @NonNull Throwable error) {
            LogUtils.e("LatexErrorSample", error + latex);
            return null;
           // return Context.getDrawable(context, R.drawable.ic_android_black_24dp);TODO
          }
        });
      }))
      .build();

    markwon.setMarkdown(textView, md);
  }
}
