package io.noties.markwon.app.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

public class HorizontalScrollView extends StackLayout {
    public HorizontalScrollView(Context context) {
        super(context);
    }

    public HorizontalScrollView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public HorizontalScrollView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
