package io.noties.markwon.app.sample;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;
import io.noties.markwon.sample.annotations.MarkwonArtifact;

/**
 * @author: yangrui
 * @function: SampleSearch
 * @date: 2021/3/23
 */
public abstract class SampleSearch {
    @Nullable
    private final String text;

    @Override
    @NonNull
    public String toString() {
        return "SampleSearch(text=" + this.text + ",type=" + this.getClass().getSimpleName() + ')';
    }

    @Nullable
    public final String getText() {
        return this.text;
    }

    private SampleSearch(String text) {
        this.text = text;
    }

    public static final class Artifact extends SampleSearch {
        @NonNull
        private final MarkwonArtifact artifact;

        @NonNull
        public final MarkwonArtifact getArtifact() {
            return this.artifact;
        }

        public Artifact(@Nullable String text, @NonNull MarkwonArtifact artifact) {
            super(text);
            this.artifact = artifact;
        }
    }

    public static final class Tag extends SampleSearch {
        @NonNull
        private final String tag;

        @NonNull
        public final String getTag() {
            return this.tag;
        }

        public Tag(@Nullable String text, @NonNull String tag) {
            super(text);
            this.tag = tag;
        }
    }

    public static final class All extends SampleSearch {
        public All(@Nullable String text) {
            super(text);
        }
    }
}
