package io.noties.markwon.app.utils;

import com.noties.markwon.utils.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ohos.utils.Pair;
import ohos.utils.net.Uri;

/**
 * @author: yangrui
 * @function: ReadMeUtils
 * @date: 2021/3/23
 */
public class ReadMeUtils {
    // username, repo, branch, lastPathSegment

    String RE_FILE = "^"+TextUtils.getHttps()+"github\\.com\\/([\\w-.]+?)\\/([\\w-.]+?)\\/(?:blob|raw)\\/([\\w-.]+?)\\/(.+)$";
    private static final String RE_REPOSITORY = "^"+TextUtils.getHttps()+"github.com/([\\w-.]+?)/([\\w-.]+?)/*$";

    public static Pair<String, String> parseRepository(String url) {

        Pattern pattern = Pattern.compile(RE_REPOSITORY);
        Matcher matcher = pattern.matcher(url);
        if (matcher.matches()) {
            String user = matcher.group(1);
            String repository = matcher.group(2);
            if (TextUtils.isEmpty(user) || TextUtils.isEmpty(user)) {
                return new Pair<>(null, null);
            } else {
                return new Pair<>(user, repository);
            }
        } else {
            return new Pair<String, String>(null, null);
        }
    }


    public static GithubInfo parseInfo(Uri data) {
        if (data == null) {
            return null;
        }
        Pattern pattern = Pattern.compile(RE_REPOSITORY);
        Matcher matcher = pattern.matcher(data.toString());
        if (!matcher.matches()) {
            return null;
        }

        return new GithubInfo(
                matcher.group(1),
                matcher.group(2),
                matcher.group(3),
                matcher.group(4)
        );
    }

    public static String buildRawGithubUrl(Uri data) {
        GithubInfo info = parseInfo(data);
        if (info == null) {
            return data.toString();
        } else {
            return buildRawGithubUrl(info);
        }
    }

    private static String buildRawGithubUrl(GithubInfo info) {
        return TextUtils.getHttps()+"github.com/${info.username}/${info.repository}/raw/${info.branch}/${info.fileName}";
    }

    public static String buildRepositoryReadMeUrl(String username, String repository) {
        return buildRawGithubUrl(new GithubInfo(username, repository, "master", "README.md"));
    }

    public static class GithubInfo {
        private String username;
        private String repository;
        private String branch;
        private String fileName;

        GithubInfo(String username, String repository, String branch, String fileName) {
            this.username = username;
            this.repository = repository;
            this.branch = branch;
            this.fileName = fileName;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRepository() {
            return repository;
        }

        public void setRepository(String repository) {
            this.repository = repository;
        }

        public String getBranch() {
            return branch;
        }

        public void setBranch(String branch) {
            this.branch = branch;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }
    }
}
