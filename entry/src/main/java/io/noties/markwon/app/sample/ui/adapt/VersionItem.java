package io.noties.markwon.app.sample.ui.adapt;

import com.noties.markwon.wrapper.text.Spanned;
import com.noties.markwon.wrapper.text.TextPaint;
import io.noties.markwon.*;
import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.core.CoreProps;
import io.noties.markwon.core.MarkwonTheme;
import io.noties.markwon.core.spans.LinkSpan;
import io.noties.markwon.html.HtmlPlugin;
import io.noties.markwon.image.ImagesPlugin;
import io.noties.markwon.movement.MovementMethodPlugin;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.commonmark.node.Link;

/**
 * @author: yangrui
 * @function: VersionItem lack lib noties adapt, this extend Item<H extends Item.Holder>
 * @date: 2021/3/23
 */
public class VersionItem {

    private Context context;

    private final Markwon markwon = Markwon.builder(context)
            .usePlugin(ImagesPlugin.create())
            .usePlugin(MovementMethodPlugin.link())
            .usePlugin(HtmlPlugin.create())
            .usePlugin(new AbstractMarkwonPlugin() {
                @Override
                public void configureSpansFactory(MarkwonSpansFactory.Builder builder) {
                    super.configureSpansFactory(builder);
                    builder.setFactory(Link.class, (configuration, props) -> new LinkSpanNoUnderline(
                            configuration.theme(),
                            CoreProps.LINK_DESTINATION.require(props),
                            configuration.linkResolver()
                    ));

                }
            })
            .build();

    private final String md = "<a https://github.com/noties/Markwon/blob/master/CHANGELOG.md\n" +
            "https://img.shields.io/maven-central/v/io.noties.markwon/core.svg?label=stable\n" +
            "https://img.shields.io/nexus/s/https/oss.sonatype.org/io.noties.markwon/core.svg?label=snapshot\n" +
            "https://fonts.gstatic.com/s/i/materialicons/open_in_browser/v6/24px.svg?download=true </a>";

    private final Spanned text = markwon.toMarkdown(md);

    private Holder createHolder(LayoutScatter inflater, ComponentContainer parent) {
        context = parent.getContext();
        return new Holder(inflater.parse(ResourceTable.Layout_adapt_version, parent, false));
    }

    private void render(Holder holder) {
        markwon.setParsedMarkdown(holder.textView, text);
    }

    class Holder {
        final Text textView;

        Holder(Component view) {
            textView = (Text) view.findComponentById(ResourceTable.Id_text_view);
        }
    }

    public class LinkSpanNoUnderline extends LinkSpan {

        LinkSpanNoUnderline(MarkwonTheme theme, String require, LinkResolver linkResolver) {
            super(theme, require, linkResolver);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderLine(false);
        }
    }
}
