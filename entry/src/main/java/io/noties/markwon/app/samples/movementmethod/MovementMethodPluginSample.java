package io.noties.markwon.app.samples.movementmethod;

import com.noties.markwon.utils.TextUtils;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.movement.MovementMethodPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;

/**
 * @author: yangrui
 * @function: MovementMethodPluginSample
 * @date: 2021/3/23
 */

@MarkwonSampleInfo(
        id = "20200627081631",
        title = "MovementMethodPlugin",
        description = "Plugin to control movement method",
        artifacts = {MarkwonArtifact.CORE},
        tags = {Tags.movementMethod, Tags.links, Tags.plugin}
)
public class MovementMethodPluginSample extends MarkwonTextViewSample {
    @Override
    public void render() {
        final String md = "# MovementMethodPlugin\n" +
                "`MovementMethodPlugin` can be used to apply movement method \n" +
                "explicitly. Including specific case to disable implicit movement \n" +
                "method which is applied when `TextView.getMovementMethod()` \n" +
                "returns `null`. A [link]("+ TextUtils.getHttps() +"github.com)".trim();

        Markwon markwon = Markwon.builder(context)
                .usePlugin(MovementMethodPlugin.link())
                .build();

        markwon.setMarkdown(textView, md);
    }
}
