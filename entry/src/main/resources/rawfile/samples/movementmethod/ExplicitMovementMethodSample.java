package io.noties.markwon.app.samples.movementmethod;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;

/**
 * @author: yangrui
 * @function: ExplicitMovementMethodSample
 * @date: 2021/3/23
 */
public class ExplicitMovementMethodSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "# Explicit movement method\n" +
      "If `TextView` already has a movement method specified, then `Markwon`\n" +
      "won't be applying a default one. You can specify movement \n" +
      "method via call to `setMovementMethod`. If your movement method can\n" +
      "handle [links](${BuildConfig.GIT_REPOSITORY}) then link would be\n" +
      "_clickable_".trim();

    final Markwon markwon = Markwon.create(context);

    // own movement method that does not handle clicks would still be used
    //  (no default aka implicit method would be applied by Markwon)
    // TODO setMovementMethod
    // textView.setMovementMethod(ScrollingMovementMethod.getInstance());

    markwon.setMarkdown(textView, md);
  }
}
