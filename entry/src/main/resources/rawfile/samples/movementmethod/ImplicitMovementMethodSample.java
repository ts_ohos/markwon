package io.noties.markwon.app.samples.movementmethod;


import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;

/**
 * @author: yangrui
 * @function: ImplicitMovementMethodSample
 * @date: 2021/3/23
 */
@MarkwonSampleInfo(
  id = "20200627075524",
  title = "Implicit movement method",
  description = "By default movement method is applied for links to be clickable",
  artifacts = {MarkwonArtifact.CORE},
  tags = {Tags.movementMethod, Tags.links, Tags.defaults}
)
public class ImplicitMovementMethodSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "# Implicit movement method\n" +
      "By default `Markwon` applies `LinkMovementMethod` if it is missing,\n" +
      "so in order for [links](${BuildConfig.GIT_REPOSITORY}) to be clickable\n" +
      "nothing special should be done".trim();

    // by default Markwon will apply a `LinkMovementMethod` if
    //  it is missing. So, in order for links to be clickable
    //  nothing should be done

    Markwon markwon = Markwon.create(context);

    markwon.setMarkdown(textView, md);
  }
}
