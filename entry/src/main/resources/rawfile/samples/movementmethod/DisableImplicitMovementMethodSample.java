package io.noties.markwon.app.samples.movementmethod;


import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.core.CorePlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;

/**
 * @author: yangrui
 * @function: DisableImplicitMovementMethodSample
 * @date: 2021/3/23
 */
@MarkwonSampleInfo(
  id = "20200627081256",
  title = "Disable implicit movement method",
  description = "Configure `Markwon` to **not** apply implicit movement method, " +
    "which consumes touch events when used in a `RecyclerView` even when " +
    "markdown does not contain links",
  artifacts = {MarkwonArtifact.CORE},
  tags = {Tags.plugin, Tags.movementMethod, Tags.links, Tags.recyclerView}
)
public class DisableImplicitMovementMethodSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "# Disable implicit movement method\n" +
      "Sometimes it is required to stop `Markwon` from applying _implicit_\n" +
      "movement method (for example when used inside in a `RecyclerView`\n" +
      "in order to make the whole itemView clickable). `Markwon` inspects\n" +
      "`TextView` and applies implicit movement method if `getMovementMethod()` \n" +
      "returns `null`. No [links](https://github.com) will be clickable in this\n" +
      "markdown".trim();

    Markwon markwon = Markwon.builder(context)
      .usePlugin(new AbstractMarkwonPlugin() {
        @Override
        public void configure(Registry registry) {
          registry.require(CorePlugin.class)
            // this flag will make sure that CorePlugin won't apply any movement method
            .hasExplicitMovementMethod(true);
          super.configure(registry);
        }
      })
      .build();

    markwon.setMarkdown(textView, md);
  }
}
