package io.noties.markwon.app.samples;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import ohos.agp.window.dialog.ToastDialog;

@MarkwonSampleInfo(
  id = "20200627072642",
  title = "Markdown in Toast",
  description = "Display _static_ markdown content in a `android.widget.Toast`",
  artifacts = MarkwonArtifact.CORE,
  tags = Tags.toast
)
class ToastSample extends MarkwonTextViewSample {
  
  @Override
  public void render() {
    // NB! only _static_ content is going to be displayed,
    //  so, no images, tables or latex in a Toast
    final String md = "" +
      "# Heading is fine \n\n" +
      "> Even quote if **fine** \n" +
      " ```" +
      "finally code works;\n" +
      "``` \n" +
      "_italic_ to put an end to it";

    Markwon markwon = Markwon.create(context);

    // render raw input to styled markdown
    Markwon markdown = (Markwon) markwon.toMarkdown(md);

    // Toast accepts CharSequence and allows styling via spans
    new ToastDialog(context).setText(markdown.toString()).show();
  }
}