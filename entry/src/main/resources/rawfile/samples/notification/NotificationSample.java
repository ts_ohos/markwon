package io.noties.markwon.app.samples.notification;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.graphics.Typeface;
import com.noties.markwon.wrapper.text.style.BulletSpan;
import com.noties.markwon.wrapper.text.style.QuoteSpan;
import com.noties.markwon.wrapper.text.style.StyleSpan;
import com.noties.markwon.wrapper.text.style.TypefaceSpan;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.Markwon;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.app.samples.notification.shared.NotificationUtils;
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;

import org.commonmark.ext.gfm.strikethrough.Strikethrough;
import org.commonmark.node.BlockQuote;
import org.commonmark.node.Code;
import org.commonmark.node.Emphasis;
import org.commonmark.node.ListItem;
import org.commonmark.node.StrongEmphasis;

@MarkwonSampleInfo(
  id = "20200701130729",
  title = "Markdown in Notification",
  description = "Proof of concept of using `Markwon` with `android.app.Notification`",
  artifacts = MarkwonArtifact.CORE,
  tags = Tags.hack
)
public class NotificationSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    // supports:
    // * bold -> StyleSpan(BOLD)
    // * italic -> StyleSpan(ITALIC)
    // * quote -> QuoteSpan()
    // * strikethrough -> StrikethroughSpan()
    // * bullet list -> BulletSpan()

    // * link -> is styled but not clickable
    // * code -> typeface monospace works, background is not

    final String md = "" +
      "**bold _bold-italic_ bold** ~~strike~~ `code` [link](#)\n\n" +
      "* bullet-one\n" +
      "* * bullet-two\n" +
      "  * bullet-three\n\n" +
      "> a quote\n\n" +
      "";

    final Markwon markwon = Markwon.builder(context)
      .usePlugin(StrikethroughPlugin.create())
      .usePlugin(new AbstractMarkwonPlugin() {
        @Override
        public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {
          builder
            .setFactory(Emphasis.class, (configuration, props) -> new StyleSpan(Typeface.ITALIC))
            .setFactory(StrongEmphasis.class, (configuration, props) -> new StyleSpan(Typeface.BOLD))
            .setFactory(BlockQuote.class, (configuration, props) -> new QuoteSpan())
            .setFactory(Strikethrough.class, (configuration, props) -> new TextForm().setStrikethrough(true))
            // NB! notification does not handle background color
            .setFactory(Code.class, (configuration, props) -> new Object[]{
              new TextForm().setTextBackgroundColor(Color.GRAY.getValue()),
              new TypefaceSpan("monospace")
            })
            // NB! both ordered and bullet list items
            .setFactory(ListItem.class, (configuration, props) -> new BulletSpan());
        }
      })
      .build();

    markwon.setMarkdown(textView, md);

    NotificationUtils.display(context, markwon.toMarkdown(md));
  }
}
