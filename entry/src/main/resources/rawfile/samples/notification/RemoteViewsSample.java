package io.noties.markwon.app.samples.notification;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.graphics.Typeface;
import com.noties.markwon.wrapper.text.style.BulletSpan;
import com.noties.markwon.wrapper.text.style.QuoteSpan;
import com.noties.markwon.wrapper.text.style.StyleSpan;
import com.noties.markwon.wrapper.text.style.TypefaceSpan;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.Markwon;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.app.samples.notification.shared.NotificationUtils;
import io.noties.markwon.core.CoreProps;
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import java.util.Optional;
import ohos.agp.components.ComponentProvider;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;

import org.commonmark.ext.gfm.strikethrough.Strikethrough;
import org.commonmark.node.BlockQuote;
import org.commonmark.node.Code;
import org.commonmark.node.Emphasis;
import org.commonmark.node.Heading;
import org.commonmark.node.ListItem;
import org.commonmark.node.StrongEmphasis;

@MarkwonSampleInfo(
  id = "20200702090140",
  title = "RemoteViews in notification",
  description = "Display markdown with platform (system) spans in notification via `RemoteViews`",
  artifacts = MarkwonArtifact.CORE,
  tags = Tags.hack
)
public class RemoteViewsSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "" +
      "# Heading 1\n" +
//      "## Heading 2\n" +
//      "### Heading 3\n" +
//      "#### Heading 4\n" +
//      "##### Heading 5\n" +
//      "###### Heading 6\n" +
      "**bold _italic_ bold** `code` [link](#) ~~strike~~\n" +
      "* Bullet 1\n" +
      "* * Bullet 2\n" +
      "  * Bullet 3\n" +
      "> A quote **here**";

    final float[] headingSizes = {
      2.F, 1.5F, 1.17F, 1.F, .83F, .67F,
    };

    Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
    DisplayAttributes displayAttributes = display.get().getAttributes();
    final int bulletGapWidth = (int) (8 * displayAttributes.densityPixels + 0.5F);

    final Markwon markwon = Markwon.builder(context)
      .usePlugin(StrikethroughPlugin.create())
      .usePlugin(new AbstractMarkwonPlugin() {
        @Override
        public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {
          builder
            .setFactory(Heading.class, (configuration, props) -> new Object[]{
              new StyleSpan(Typeface.BOLD),new TextForm().setRelativeTextSize(headingSizes[CoreProps.HEADING_LEVEL.require(props) - 1])
            })
            .setFactory(StrongEmphasis.class, (configuration, props) -> new StyleSpan(Typeface.BOLD))
            .setFactory(Emphasis.class, (configuration, props) -> new StyleSpan(Typeface.ITALIC))
            .setFactory(Code.class, (configuration, props) -> new Object[]{
              new TextForm().setTextBackgroundColor(Color.GRAY.getValue()),
              new TypefaceSpan("monospace")
            })
            .setFactory(Strikethrough.class, (configuration, props) -> new TextForm().setStrikethrough(true))
            .setFactory(ListItem.class, (configuration, props) -> new BulletSpan(bulletGapWidth))
            .setFactory(BlockQuote.class, (configuration, props) -> new QuoteSpan());
        }
      })
      .build();

    final ComponentProvider remoteViews = new ComponentProvider(ResourceTable.Layout_sample_remote_view, context);
    // TODO remoteViews.setTextViewText(R.id.text_view, markwon.toMarkdown(md));
    // TODO markwon.toMarkdown(md) is charset
    remoteViews.setText(ResourceTable.Id_text_view, markwon.toMarkdown(md).toString());

    NotificationUtils.display(context, remoteViews);
  }
}
