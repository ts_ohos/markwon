package io.noties.markwon.app.samples.notification.shared;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.utils.LogUtils;
import com.noties.markwon.utils.PixelMapUtils;

import io.noties.markwon.app.ResourceTable;
import ohos.agp.components.ComponentProvider;
import ohos.app.Context;
import ohos.event.notification.NotificationHelper;
import ohos.event.notification.NotificationRequest;
import ohos.event.notification.NotificationSlot;
import ohos.rpc.RemoteException;

public abstract class NotificationUtils {
  private static final String TAG = "NotificationUtils";

  private static final int ID = 2;
  private static final String CHANNEL_ID = "2";

  public static void display(@NonNull Context context, @NonNull CharSequence cs) {

    ensureChannel(CHANNEL_ID);

    NotificationRequest request = new NotificationRequest(ID);
    request.setLittleIcon(PixelMapUtils.getPixelMapFromResource(context, ResourceTable.Graphic_ic_stat_name));
    request.setNotificationId(ID);
    request.setSlotId(CHANNEL_ID);

    NotificationRequest.NotificationMultiLineContent content = new NotificationRequest.NotificationMultiLineContent();
    content.setTitle(context.getString(ResourceTable.String_app_name))
            .setText(cs.toString());
    NotificationRequest.NotificationContent notificationContent = new NotificationRequest.NotificationContent(content);
    request.setContent(notificationContent);

    try {
      NotificationHelper.publishNotification(request);
    } catch (RemoteException ex) {
      LogUtils.e(TAG, "Exception occurred during publishNotification invocation.");
    }

  }

  public static void display(@NonNull Context context, @NonNull ComponentProvider remoteViews) {

    ensureChannel(CHANNEL_ID);

    NotificationRequest request = new NotificationRequest(ID);
    request.setLittleIcon(PixelMapUtils.getPixelMapFromResource(context, ResourceTable.Graphic_ic_stat_name));
    request.setNotificationId(ID);
    request.setSlotId(CHANNEL_ID);

    NotificationRequest.NotificationMultiLineContent content = new NotificationRequest.NotificationMultiLineContent();
    content.setTitle(context.getString(ResourceTable.String_app_name));
    NotificationRequest.NotificationContent notificationContent = new NotificationRequest.NotificationContent(content);
    request.setContent(notificationContent);
    request.setCustomBigView(remoteViews);
    request.setCustomView(remoteViews);

    try {
      NotificationHelper.publishNotification(request);
    } catch (RemoteException ex) {
      LogUtils.e(TAG, "Exception occurred during publishNotification invocation.");
    }
  }

  @SuppressWarnings("SameParameterValue")
  private static void ensureChannel(@NonNull String channelId) {
    final NotificationSlot channel = new NotificationSlot(channelId, channelId, NotificationSlot.LEVEL_DEFAULT);
    if (channel == null) {
      try {
        NotificationHelper.addNotificationSlot(channel);
      } catch (RemoteException ex) {
        LogUtils.d(TAG, "Exception occurred during addNotificationSlot invocation.");
      }
    }
  }

  private NotificationUtils() {
  }


}
