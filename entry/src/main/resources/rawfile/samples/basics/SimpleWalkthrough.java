package io.noties.markwon.app.samples.basics;

import com.noties.markwon.wrapper.text.Spanned;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.core.CorePlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;

import org.commonmark.node.Node;

@MarkwonSampleInfo(
        id = "20200626153426",
        title = "Simple with walk-through",
        description = "Walk-through for simple use case",
        artifacts = {MarkwonArtifact.CORE},
        tags = {Tags.basics}
)
public class SimpleWalkthrough extends MarkwonTextViewSample {
    @Override
    public void render() {
        final String md = "# Hello!\n" +
                "> a quote\n" +
                "```\n" +
                "code block\n" +
                "```".trim();

        // create markwon instance via builder method
        final Markwon markwon = Markwon.builder(context)
                // add required plugins
                // NB, there is no need to add CorePlugin as it is added automatically
                .usePlugin(CorePlugin.create())
                .build();

        // parse markdown into commonmark representation
        final Node node = markwon.parse(md);

        // render commonmark node
        final Spanned markdown = markwon.render(node);

        // apply it to a TextView
        markwon.setParsedMarkdown(textView, markdown);
    }
}
