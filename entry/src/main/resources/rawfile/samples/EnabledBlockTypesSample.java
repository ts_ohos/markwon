package io.noties.markwon.app.samples;

import com.noties.markwon.annotation.NonNull;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.Markwon;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.core.CorePlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import java.util.Set;

import org.commonmark.node.BlockQuote;
import org.commonmark.parser.Parser;

/**
 * @author: yangrui
 * @function: EnabledBlockTypesSample
 * @date: 2021/3/23
 */
@MarkwonSampleInfo(
  id = "20200627075012",
  title = "Enabled markdown blocks",
  description = "Modify/inspect enabled by `CorePlugin` block types. " +
    "Disable quotes or other blocks from being parsed",
  artifacts = MarkwonArtifact.CORE,
  tags = {Tags.parsing, Tags.block, Tags.plugin}
)
class EnabledBlockTypesSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    final String md = "" +
      "# Heading\n" +
      "## Second level\n" +
      "> Quote is not handled\n";
    final Markwon markwon = Markwon.builder(context)
      .usePlugin(new AbstractMarkwonPlugin() {
        @Override
        public void configureParser(@NonNull Parser.Builder builder) {

          // obtain all enabled block types
          final Set enabledBlockTypes = CorePlugin.enabledBlockTypes();
          // it is safe to modify returned collection
          // remove quotes
          enabledBlockTypes.remove(BlockQuote.class);

          builder.enabledBlockTypes(enabledBlockTypes);
        }
      })
      .build();

    markwon.setMarkdown(textView, md);
  }
}