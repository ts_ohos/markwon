package io.noties.markwon.app.samples.editor.shared;


import com.noties.markwon.wrapper.text.SpannableStringBuilder;
import com.noties.markwon.wrapper.text.style.StrikethroughSpan;

import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.ui.MarkwonSample;
import io.noties.markwon.core.spans.EmphasisSpan;
import io.noties.markwon.core.spans.StrongEmphasisSpan;
import java.util.ArrayList;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.text.TextForm;
import ohos.app.Context;

/**
 * @author: yangrui
 * @function: MarkwonEditTextSample
 * @date: 2021/3/23
 */
public abstract class MarkwonEditTextSample extends MarkwonSample {

  protected Context context;
  protected TextField editText;

  @Override
  protected int layoutResId() {
    return ResourceTable.Layout_sample_edit_text;
  }

  @Override
  protected void onViewCreated(Component view) {
    context = view.getContext();
    editText = (TextField) view.findComponentById(ResourceTable.Id_edit_text);
    initBottomBar(view);
    render();
  }

  /**
   * render component content
   */
  protected abstract void render();

  private void initBottomBar(Component view) {
    Button bold = (Button) view.findComponentById(ResourceTable.Id_bold);
    Button italic = (Button) view.findComponentById(ResourceTable.Id_italic);
    Button strike = (Button) view.findComponentById(ResourceTable.Id_strike);
    Button quote = (Button) view.findComponentById(ResourceTable.Id_quote);
    Button code = (Button) view.findComponentById(ResourceTable.Id_code);

    addSpan(bold, new StrongEmphasisSpan());
    addSpan(italic, new EmphasisSpan());
    addSpan(strike, new TextForm().setStrikethrough(true));
    bold.setClickedListener(new InsertOrWrapClickListener(editText, "**"));
    italic.setClickedListener(new InsertOrWrapClickListener(editText, "_"));
    strike.setClickedListener(new InsertOrWrapClickListener(editText, "~~"));
    code.setClickedListener(new InsertOrWrapClickListener(editText, "`"));
    quote.setClickedListener((it) -> {
      // TODO YR lack aip
      int start = 0;// =editText.selectionStart
      int end = 0;// =editText.selectionEnd
      if (start < 0) {
        return;
      }
      if (start == end) {
        // editText.text.insert(start, "> ")
      } else {
        ArrayList<Integer> newLines = new ArrayList<>();
        newLines.add(start);
        String text = editText.getText().substring(start, end);
        int index = text.indexOf('\n');
        while (index != -1) {
          newLines.add(start + index + 1);
          index = text.indexOf('\n', index + 1);
        }

                /*for (i in newLines.indices.reversed()) {
                    editText.text.insert(newLines[i], "> ")
                }*/
      }

    });

  }

  private void addSpan(Text textView, Object... spans) {
    SpannableStringBuilder builder = new SpannableStringBuilder(textView.getText());
    int end = builder.length();

    for (Object span : spans) {
      if (span instanceof StrikethroughSpan) {
        builder.mergeForm(new TextForm().setStrikethrough(true));
        builder.revertForm();
      }
      //builder.setSpan(span, 0, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
    textView.setText(builder.toString());

  }

  private class InsertOrWrapClickListener implements Component.ClickedListener {

    private final TextField editText;
    private final String text;

    InsertOrWrapClickListener(TextField editText, String text) {
      this.editText = editText;
      this.text = text;
    }

    @Override
    public void onClick(Component component) {
      // TODO YR lack aip
      int start = 0;// =editText.selectionStart
      int end = 0;// =editText.selectionEnd
      if (start < 0) {
        return;
      }
      if (start == end) {
        // insert at current position
        // editText.getText().insert(start, text);
      } else {
        // editText.text.insert(end, text);
        // editText.text.insert(start, text);
      }

    }

  }


}
