package io.noties.markwon.app.samples.image;


import io.noties.markwon.Markwon;
import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.image.ImagesPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;

@MarkwonSampleInfo(
        id = "20200630165504",
        title = "Image with placeholder",
        artifacts = MarkwonArtifact.IMAGE,
        tags = Tags.image
)
public class PlaceholderImageSample extends MarkwonTextViewSample {
    @Override
    public void render() {
        final String md = "" +
                "![image](https://github.com/dcurtis/markdown-mark/raw/master/png/1664x1024-solid.png)";

        final Markwon markwon = Markwon.builder(context)
                .usePlugin(ImagesPlugin.create(plugin -> {
                    plugin.placeholderProvider(drawable -> {
                        // by default drawable intrinsic size will be used
                        //  otherwise bounds can be applied explicitly
                      // TODO YR lack API
                        Element parse = ElementScatter.getInstance(context).parse(ResourceTable.Graphic_ic_android_black_24dp);
                        return null;
                    });
                }))
                .build();

        markwon.setMarkdown(textView, md);
    }
}
