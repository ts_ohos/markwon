package io.noties.markwon.app.samples.image;

import com.noties.markwon.utils.TextUtils;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonRecyclerViewSample;
import io.noties.markwon.image.coil.CoilImagesPlugin;
import io.noties.markwon.recycler.MarkwonAdapter;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import ohos.agp.components.LayoutManager;

/**
 * @author: yangrui
 * @function: CoilRecyclerViewSample
 * @date: 2021/3/23
 */
@MarkwonSampleInfo(
        id = "20200803132053",
        title = "Coil inside RecyclerView",
        description = "Display images via Coil plugin in `RecyclerView`",
        artifacts = {MarkwonArtifact.IMAGE_COIL, MarkwonArtifact.RECYCLER},
        tags = {Tags.rendering, Tags.recyclerView, Tags.image}
)
public class CoilRecyclerViewSample extends MarkwonRecyclerViewSample {
    @Override
    public void render() {
        String md = "# H1\n" +
                "      ## H2\n" +
                "      ### H3\n" +
                "      #### H4\n" +
                "      ##### H5\n" +
                "      \n" +
                "      > a quote\n" +
                "      \n" +
                "      + one\n" +
                "      - two\n" +
                "      * three\n" +
                "      \n" +
                "      1. one\n" +
                "      1. two\n" +
                "      1. three\n" +
                "      \n" +
                "      ---\n" +
                "      \n" +
                "      # Images\n" +
                "      \n" +
                "      ![img]("+ TextUtils.getHttps() +"picsum.photos/id/237/1024/800)";

        // TODO YR lack lib io.coil-kt:coil:0.13.0
        Markwon markwon = Markwon.builder(context)
                .usePlugin(CoilImagesPlugin.create(new CoilImagesPlugin.CoilStore() {
                    @Override
                    public void load() {

                    }

                    @Override
                    public void cancel() {

                    }
                }, "Coil.imageLoader(context)"))
                .build();

        MarkwonAdapter adapter = MarkwonAdapter.createTextViewIsRoot(ResourceTable.Layout_adapter_node);
        recyclerView.setLayoutManager(new LayoutManager() {
            @Override
            public void setOrientation(int orientation) {
                super.setOrientation(orientation);
            }
        });

        recyclerView.setAdapter(adapter);
        adapter.setMarkdown(markwon, md);
        adapter.notifyAll();
    }
}
