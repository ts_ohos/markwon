package io.noties.markwon.app.samples.image;


import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.LinkResolver;
import io.noties.markwon.Markwon;
import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.app.readme.GithubImageDestinationProcessor;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.app.utils.SampleUtilsKt;
import io.noties.markwon.core.spans.LinkSpan;
import io.noties.markwon.image.ImageProps;
import io.noties.markwon.image.ImagesPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import ohos.agp.components.Component;

import org.commonmark.node.Image;

/**
 * @author: yangrui
 * @function: ClickImageSample
 * @date: 2021/3/23
 */
@MarkwonSampleInfo(
        id = "20201221130230",
        title = "Click images",
        description = "Make _all_ images clickable (to open in a gallery, etc)",
        artifacts = {MarkwonArtifact.IMAGE},
        tags = {Tags.rendering, Tags.image}
)
public class ClickImageSample extends MarkwonTextViewSample {
    @Override
    public void render() {
        String md = SampleUtilsKt.loadReadMe(context);

        Markwon markwon = Markwon.builder(context)
                .usePlugin(ImagesPlugin.create())
                .usePlugin(new AbstractMarkwonPlugin() {

                    @Override
                    public void configureConfiguration(MarkwonConfiguration.Builder builder) {
                        super.configureConfiguration(builder);
                        builder.imageDestinationProcessor(new GithubImageDestinationProcessor());
                    }
                })
                .usePlugin(new AbstractMarkwonPlugin() {
                    @Override
                    public void configureSpansFactory(MarkwonSpansFactory.Builder builder) {
                        super.configureSpansFactory(builder);
                        builder.appendFactory(Image.class, (configuration, props) -> {
                            String url = ImageProps.DESTINATION.require(props);
                            return new LinkSpan(configuration.theme(), url, new ImageLinkResolver(configuration.linkResolver()));
                        });

                    }
                })


                .build();

        markwon.setMarkdown(textView, md);
    }

    class ImageLinkResolver implements LinkResolver {

        private final LinkResolver original;

        ImageLinkResolver(LinkResolver original) {
            this.original = original;
        }

        @Override
        public void resolve(Component view, String link) {
            // decide if you want to open gallery or anything else,
            //  here we just pass to original
            if (false) {
                // do your thing
            } else {
                // just use original
                original.resolve(view, link);
            }
        }
    }


}
