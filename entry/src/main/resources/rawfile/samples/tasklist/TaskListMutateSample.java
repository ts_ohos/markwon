package io.noties.markwon.app.samples.tasklist;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.utils.LogUtils;
import com.noties.markwon.wrapper.text.SpannableString;
import com.noties.markwon.wrapper.text.Spanned;
import com.noties.markwon.wrapper.text.TextPaint;
import com.noties.markwon.wrapper.text.style.ClickableSpan;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.Markwon;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.SpanFactory;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.app.samples.tasklist.shared.TaskListHolder;
import io.noties.markwon.ext.tasklist.TaskListItem;
import io.noties.markwon.ext.tasklist.TaskListPlugin;
import io.noties.markwon.ext.tasklist.TaskListSpan;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

@MarkwonSampleInfo(
  id = "20200702140901",
  title = "GFM task list mutate",
  artifacts = MarkwonArtifact.EXT_TASKLIST,
  tags = Tags.plugin
)
public class TaskListMutateSample extends MarkwonTextViewSample {
  private static final String TAG = "TaskListMutateSample";

  @Override
  public void render() {

    // NB! this sample works for a single level task list,
    //  if you have multiple levels, then see the `TaskListMutateNestedSample`

    final Markwon markwon = Markwon.builder(context)
      .usePlugin(TaskListPlugin.create(context))
      .usePlugin(new AbstractMarkwonPlugin() {
        @Override
        public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {
          // obtain origin task-list-factory
          final SpanFactory origin = builder.getFactory(TaskListItem.class);
          if (origin == null) {
            return;
          }

          builder.setFactory(TaskListItem.class, (configuration, props) -> {
            // maybe it's better to validate the actual type here also
            // and not force cast to task-list-span
            final TaskListSpan span = (TaskListSpan) origin.getSpans(configuration, props);
            if (span == null) {
              return null;
            }

            // NB, toggle click will intercept possible links inside task-list-item
            return new Object[]{
              span,
              new TaskListToggleSpan(span)
            };
          });
        }
      })
      .build();

    markwon.setMarkdown(textView, TaskListHolder.MD);
  }

  static class TaskListToggleSpan extends ClickableSpan {

    private final TaskListSpan span;

    TaskListToggleSpan(@NonNull TaskListSpan span) {
      this.span = span;
    }

    @Override
    public void onClick(@NonNull Component widget) {
      // toggle span (this is a mere visual change)
      span.setDone(!span.isDone());
      // request visual update
      widget.invalidate();

      // it must be a TextView
      final Text textView = (Text) widget;
      // it must be spanned
      // TODO
      // final Spanned spanned = (Spanned) textView.getText();
      final Spanned spanned = new SpannableString("");

      // actual text of the span (this can be used along with the  `span`)
      final CharSequence task = spanned.subSequence(
        spanned.getSpanStart(this),
        spanned.getSpanEnd(this)
      );

      LogUtils.i(TAG, "task done: " + span.isDone() + "," + task);
    }

    @Override
    public void updateDrawState(@NonNull TextPaint ds) {
      // no op, so text is not rendered as a link
    }
  }
}
