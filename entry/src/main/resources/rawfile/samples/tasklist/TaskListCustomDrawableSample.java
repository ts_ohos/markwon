package io.noties.markwon.app.samples.tasklist;

import io.noties.markwon.Markwon;
import io.noties.markwon.app.ResourceTable;
import io.noties.markwon.app.sample.Tags;
import io.noties.markwon.app.sample.ui.MarkwonTextViewSample;
import io.noties.markwon.app.samples.tasklist.shared.TaskListHolder;
import io.noties.markwon.ext.tasklist.TaskListPlugin;
import io.noties.markwon.sample.annotations.MarkwonArtifact;
import io.noties.markwon.sample.annotations.MarkwonSampleInfo;
import java.io.IOException;
import java.util.Objects;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.NotExistException;

@MarkwonSampleInfo(
  id = "20200702140749",
  title = "GFM task list custom drawable",
  artifacts = MarkwonArtifact.EXT_TASKLIST,
  tags = Tags.plugin
)
public class TaskListCustomDrawableSample extends MarkwonTextViewSample {
  @Override
  public void render() {
    PixelMapElement drawable = null;
    try {
      drawable = new PixelMapElement(Objects.requireNonNull(
              context.getResourceManager().getResource(ResourceTable.Graphic_custom_task_list)));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NotExistException e) {
      e.printStackTrace();
    }

    final Markwon markwon = Markwon.builder(context)
      .usePlugin(TaskListPlugin.create(drawable))
      .build();

    markwon.setMarkdown(textView, TaskListHolder.MD);
  }
}
