package io.noties.markwon.app.samples.plugins.shared;


import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.wrapper.text.Spannable;
import com.noties.markwon.wrapper.text.SpannableString;
import com.noties.markwon.wrapper.text.Spanned;

import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.LinkResolverDef;
import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.core.spans.HeadingSpan;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class AnchorHeadingPlugin extends AbstractMarkwonPlugin {

  public interface ScrollTo {
    void scrollTo(@NonNull Text view, int top);
  }

  private final ScrollTo scrollTo;

  public AnchorHeadingPlugin(@NonNull ScrollTo scrollTo) {
    this.scrollTo = scrollTo;
  }

  @Override
  public void configureConfiguration(@NonNull MarkwonConfiguration.Builder builder) {
    builder.linkResolver(new AnchorLinkResolver(scrollTo));
  }

  @Override
  public void afterSetText(@NonNull Text textView) {
    // TODO
    final Spannable spannable = new SpannableString("");
    // obtain heading spans
    final HeadingSpan[] spans = spannable.getSpans(0, spannable.length(), HeadingSpan.class);
    if (spans != null) {
      for (HeadingSpan span : spans) {
        final int start = spannable.getSpanStart(span);
        final int end = spannable.getSpanEnd(span);
        final int flags = spannable.getSpanFlags(span);
        spannable.setSpan(
          new AnchorSpan(createAnchor(spannable.subSequence(start, end))),
          start,
          end,
          flags
        );
      }
    }
  }

  private static class AnchorLinkResolver extends LinkResolverDef {

    private final ScrollTo scrollTo;

    AnchorLinkResolver(@NonNull ScrollTo scrollTo) {
      this.scrollTo = scrollTo;
    }

    @Override
    public void resolve(@NonNull Component view, @NonNull String link) {
      String prefix = "#";
      if (link.startsWith(prefix)) {
        final Text textView = (Text) view;
        // TODO
        // final Spanned spanned = (Spannable) textView.getText();
        final Spanned spanned = new SpannableString("");
        final AnchorSpan[] spans = spanned.getSpans(0, spanned.length(), AnchorSpan.class);
        if (spans != null) {
          final String anchor = link.substring(1);
          for (AnchorSpan span : spans) {
            if (anchor.equals(span.anchor)) {
              final int start = spanned.getSpanStart(span);
              // TODO
              // final int line = textView.getLayout().getLineForOffset(start);
              // final int top = textView.getLayout().getLineTop(line);
              // scrollTo.scrollTo(textView, top);
              return;
            }
          }
        }
      }
      super.resolve(view, link);
    }
  }

  private static class AnchorSpan {
    final String anchor;

    AnchorSpan(@NonNull String anchor) {
      this.anchor = anchor;
    }
  }

  @NonNull
  public static String createAnchor(@NonNull CharSequence content) {
    return String.valueOf(content)
      .replaceAll("[^\\w]", "")
      .toLowerCase();
  }
}
