# Markwon sample hap

Collection of sample snippets showing different aspects of `Markwon` library usage. Includes
source code of samples, latest stable/snapshot version of the library and search functionality.
Additionally can check for updates. Can be used to preview markdown documents from the `Github.com`.


## Distribution

Sample app is distributed via special parent-less branch [sample-store](https://url/sample-store).
Inside the app, under version badges, tap `CHECK FOR UPDATES` to check for updates. Sample app
is not attached to main libraries versions and can be _released_ independently.

Application is signed with `markwon.p12`, which fingerprints are:
* __SHA256__: `SHA256withECDSA`


[Download latest hap](https://url/sample-store/markwon-debug.hap)


## Deeplink

Sample app handles special `markwon` scheme:
* `markwon://sample/{ID}` to open specific sample given the `{ID}`
* `markwon://search?q={TEXT TO SEARCH}&a={ARTIFACT}&t={TAG}`

Please note that search deeplink can have one of type: artifact or tag (if both are specified artifact will be used).

To test locally:



## Building

When adding/removing samples _most likely_ a clean build would be required.
First, for annotation processor to create `samples.json`. And secondly,
in order for Android Gradle plugin to bundle resources referenced via
symbolic links (the `sample.json` itself and `io.noties.markwon.app.samples.*` directory)

```
./gradlew :app-s:clean :app-s:asDe
```

## Sample id

Sample `id` is generated manually when creating new sample. A `Live Template` can be used
to simplify generation part (for example `mid` shortcut with a single variable in `Java`  scopes):

```
groovyScript("new Date().format('YYYYMMddHHmmss', TimeZone.getTimeZone('UTC'))")
```

## Tests

This app uses [Robolectric](https://robolectric.org)(v3.8) for tests which is incompatible
with JDK > 1.8. In order to run tests from command line with IDEA-bundled JDK - a special argument is
required:

```
./gradlew :app-s:testDe -Dorg.gradle.java.home="{INSERT BUNDLED JDK PATH HERE}"
```

To obtain bundled JDK:
* open `Project Structure...`
* open `SDK Location`
* copy contents of the field under `JDK Location`


