package io.noties.markwon.inlineparser;

import com.noties.markwon.annotation.NonNull;
import com.noties.markwon.annotation.Nullable;

import java.util.regex.Pattern;

import org.commonmark.internal.Bracket;
import org.commonmark.internal.Delimiter;
import org.commonmark.node.Node;
import org.commonmark.node.Text;

/**
 * @see AutolinkInlineProcessor
 * @see BackslashInlineProcessor
 * @see BackticksInlineProcessor
 * @see BangInlineProcessor
 * @see CloseBracketInlineProcessor
 * @see EntityInlineProcessor
 * @see HtmlInlineProcessor
 * @see NewLineInlineProcessor
 * @see OpenBracketInlineProcessor
 * @see MarkwonInlineParser.FactoryBuilder#addInlineProcessor(InlineProcessor)
 * @see MarkwonInlineParser.FactoryBuilder#excludeInlineProcessor(Class)
 * @since 4.2.0
 */
public abstract class InlineProcessor {

    /**
     * Special character that triggers parsing attempt
     */
    public abstract char specialCharacter();

    /**
     * @return boolean indicating if parsing succeeded
     */
    @Nullable
    protected abstract Node parse();


    MarkwonInlineParserContext context;
    Node block;
    String input;
    int index;

    @Nullable
    public Node parse(@NonNull MarkwonInlineParserContext context) {
        this.context = context;
        this.block = context.block();
        this.input = context.input();
        this.index = context.index();

        final Node result = parse();

        // synchronize index
        context.setIndex(index);

        return result;
    }

    Bracket lastBracket() {
        return context.lastBracket();
    }

    Delimiter lastDelimiter() {
        return context.lastDelimiter();
    }

    void addBracket(Bracket bracket) {
        context.addBracket(bracket);
    }

    void removeLastBracket() {
        context.removeLastBracket();
    }

    void spnl() {
        context.setIndex(index);
        context.spnl();
        index = context.index();
    }

    @Nullable
    protected String match(@NonNull Pattern re) {
        // before trying to match, we must notify context about our index (which we store additionally here)
        context.setIndex(index);

        final String result = context.match(re);

        // after match we must reflect index change here
        this.index = context.index();

        return result;
    }

    @Nullable
    String parseLinkDestination() {
        context.setIndex(index);
        final String result = context.parseLinkDestination();
        this.index = context.index();
        return result;
    }

    @Nullable
    String parseLinkTitle() {
        context.setIndex(index);
        final String result = context.parseLinkTitle();
        this.index = context.index();
        return result;
    }

    int parseLinkLabel() {
        context.setIndex(index);
        final int result = context.parseLinkLabel();
        this.index = context.index();
        return result;
    }

    void processDelimiters(Delimiter stackBottom) {
        context.setIndex(index);
        context.processDelimiters(stackBottom);
        this.index = context.index();
    }

    @NonNull
    protected Text text(@NonNull String text) {
        return context.text(text);
    }

    @NonNull
    Text text(@NonNull String text, int start, int end) {
        return context.text(text, start, end);
    }

    char peek() {
        context.setIndex(index);
        return context.peek();
    }
}
