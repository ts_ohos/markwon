package io.noties.markwon.editor;


import com.noties.markwon.wrapper.text.TextPaint;
import com.noties.markwon.wrapper.text.style.CharacterStyle;
import io.noties.markwon.utils.ColorUtils;
import ohos.agp.utils.Color;

class PunctuationSpan extends CharacterStyle {

    private static final int DEF_PUNCTUATION_ALPHA = 75;

    @Override
    public void updateDrawState(TextPaint tp) {
        final int color = ColorUtils.applyAlpha(tp.getColor().getValue(), DEF_PUNCTUATION_ALPHA);
        tp.setColor(new Color(color));
    }
}
